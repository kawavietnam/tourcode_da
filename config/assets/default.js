'use strict';

/* eslint comma-dangle:[0, "only-multiline"] */

module.exports = {
    client: {
        lib: {
            css: [
                // bower:css
                'http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all',
                'public/lib/metronic/global/plugins/font-awesome/css/font-awesome.min.css',
                'public/lib/metronic/global/plugins/simple-line-icons/simple-line-icons.min.css',
                'public/lib/metronic/global/plugins/bootstrap/css/bootstrap.min.css',
                'public/lib/metronic/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css',
                'public/lib/metronic/global/css/components-md.min.css',
                'public/lib/metronic/global/css/plugins-md.min.css',
                'public/lib/metronic/layouts/layout3/css/layout.min.css',
                'public/lib/metronic/layouts/layout3/css/themes/default.min.css',
                'public/lib/angular-ui-select/dist/select.min.css',

                //login
                'public/lib/metronic/global/plugins/select2/css/select2.min.css',
                'public/lib/metronic/plugins/select2/css/select2-bootstrap.min.css',
                'public/lib/metronic/global/css/components-md.min.css',
                'public/lib/metronic/global/css/plugins-md.min.css',
                'public/lib/metronic/pages/css/login-5.min.css',

                'public/lib/metronic/global/plugins/bootstrap-daterangepicker/daterangepicker.min.css',
                'public/lib/metronic/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css',
                'public/lib/metronic/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css',

                'public/lib/ngToast/dist/ngToast.min.css',
                'public/lib/ngToast/dist/ngToast-animations.min.css',
                'public/lib/metronic/layouts/layout3/css/custom.min.css',
                'public/lib/clockpicker/jquery-clockpicker.min.css',
                'public/lib/datetimepicker/src/css/datetimepicker.css',
                'public/lib/datepicker/dist/ng-flat-datepicker.min.css',
                'public/lib/angular-bootstrap/ui-bootstrap-csp.css',
                // introjs
                'public/lib/angular-intro.js/introjs.min.css',
                'public/lib/slick/slick.css',
                'public/lib/slick/slick-theme.css',

            ],
            js: [
                // bower:js
                'public/lib/metronic/global/plugins/jquery.min.js',
                'public/lib/CheckVersion/CheckVersion.js',
                'public/lib/angular/angular.js',
                'public/lib/metronic/global/plugins/moment.min.js',
                'public/lib/angular-animate/angular-animate.js',
                'public/lib/angular-bootstrap/ui-bootstrap-tpls.js',
                'public/lib/ng-file-upload/ng-file-upload.js',
                'public/lib/ng-img-crop/compile/unminified/ng-img-crop.js',
                'public/lib/angular-messages/angular-messages.js',
                'public/lib/angular-mocks/angular-mocks.js',
                'public/lib/angular-resource/angular-resource.js',
                'public/lib/angular-ui-notification/dist/angular-ui-notification.js',
                'public/lib/angular-ui-router/release/angular-ui-router.js',
                'public/lib/owasp-password-strength-test/owasp-password-strength-test.js',
                'public/lib/angular-ui-select/dist/select.min.js',
                // add theme js.

                
                'public/lib/metronic/global/plugins/bootstrap/js/bootstrap.min.js',
                'public/lib/metronic/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js',
                'public/lib/metronic/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js',
                'public/lib/metronic/global/plugins/jquery.blockui.min.js',
                'public/lib/metronic/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js',
                'public/lib/metronic/global/scripts/app.min.js',

                'public/lib/metronic/layouts/layout3/scripts/layout.min.js',
                'public/lib/metronic/layouts/layout3/scripts/demo.min.js',
                'public/lib/metronic/layouts/global/scripts/quick-sidebar.min.js',

                // BEGIN PAGE LEVEL PLUGINS
                'public/lib/metronic/global/plugins/jquery-validation/js/jquery.validate.min.js',
                'public/lib/metronic/global/plugins/jquery-validation/js/additional-methods.min.js',
                'public/lib/metronic/global/plugins/select2/js/select2.full.min.js',
                'public/lib/metronic/global/plugins/backstretch/jquery.backstretch.min.js',

                'public/lib/metronic/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js',
                'public/lib/metronic/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js',
                'public/lib/metronic/global/plugins/bootstrap-daterangepicker/daterangepicker.min.js',
                'public/lib/metronic/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js',
                'public/lib/metronic/pages/scripts/components-date-time-pickers.min.js',
                'public/lib/datepicker/dist/ng-flat-datepicker.js',
                //BlocksIt
                'public/lib/blocksit/blocksit.min.js',
                // login
                'public/lib/metronic/global/scripts/app.min.js',
                'public/lib/metronic/pages/scripts/login-5.min.js',
                'public/lib/angular-md5/angular-md5.min.js',


                'public/lib/paging/dirPagination.js',

                // Edit content
                'public/lib/ckeditor/ckeditor.js',
                //UpLoad Image
                'public/lib/angular-base64-upload/dist/angular-base64-upload.min.js',
                // notify
                'public/lib/ngToast/dist/ngToast.min.js',
                'public/lib/slider/jssor.slider-22.0.6.mini.js',
                'public/lib/a0-angular-storage/dist/angular-storage.min.js',
                'public/lib/clockpicker/jquery-clockpicker.min.js',

                'public/lib/exportword/FileSaver.min.js',
                'public/lib/exportword/save.js',
                'public/lib/exportpdf/html2canvas.min.js',
                'public/lib/exportpdf/jspdf.min.js',
                'public/lib/exportpdf/saveHtmlToPdf.min.js',
                'public/lib/ckfinder/ckfinder.js',
                // introjs
                'public/lib/angular-intro.js/intro.min.js',
                'public/lib/angular-intro.js/angular-intro.js',
                // currency-format
                'public/lib/angular-currency-format/dist/currency-format.min.js',
                'public/lib/slick/slick.min.js',

                

                //js-md5
                'public/lib/js-md5/build/md5.min.js',

            ],
            tests: ['public/lib/angular-mocks/angular-mocks.js']
        },
        css: [
            'modules/*/client/css/*.css'
        ],
        less: [
            'modules/*/client/less/*.less'
        ],
        sass: [
            'modules/*/client/scss/*.scss'
        ],
        js: [
            'modules/core/client/app/config.js',
            'modules/core/client/app/init.js',
            'modules/*/client/*.js',
            'modules/*/client/**/*.js'
        ],
        img: [
            'modules/**/*/img/**/*.jpg',
            'modules/**/*/img/**/*.png',
            'modules/**/*/img/**/*.gif',
            'modules/**/*/img/**/*.svg'
        ],
        views: ['modules/*/client/views/**/*.html'],
        templates: ['build/templates.js']
    },
    server: {
        gulpConfig: ['gulpfile.js'],
        allJS: ['server.js', 'config/**/*.js', 'modules/*/server/**/*.js'],
        models: 'modules/*/server/models/**/*.js',
        routes: ['modules/!(core)/server/routes/**/*.js', 'modules/core/server/routes/**/*.js'],
        sockets: 'modules/*/server/sockets/**/*.js',
        config: ['modules/*/server/config/*.js'],
        policies: 'modules/*/server/policies/*.js',
        views: ['modules/*/server/views/*.html']
    }
};
// Tourview tls service used to communicate Tourview tls REST endpoints
(function () {
  'use strict';

  angular
    .module('tourview-tls')
    .factory('TourviewTlsService', TourviewTlsService);

  TourviewTlsService.$inject = ['$resource'];

  function TourviewTlsService($resource) {
    return $resource('api/tourview-tls/:tourviewTlId', {
      tourviewTlId: '@_id'
    }, {
      update: {
        method: 'PUT'
      }
    });
  }
}());

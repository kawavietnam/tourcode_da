(function () {
  'use strict';

  angular
    .module('tourview-tls')
    .config(function($mdDateLocaleProvider) {
          $mdDateLocaleProvider.formatDate = function(date) {
            return date ? moment(date).format('DD-MMM-YYYY') : '';
          };
          
          // $mdDateLocaleProvider.parseDate = function(dateString) {
          //   var m = moment(dateString, 'DD-MMM-YYYY', true);
          //   return m.isValid() ? m.toDate() : new Date(NaN);
          // };
        })
    .filter('filterMultipleService', ['$filter', function($filter) {
            return function(items, keyObj) {
                var filterObj = {
                    data: items,
                    filteredData: [],
                    applyFilter: function(obj, key) {
                        var fData = [];
                        if (this.filteredData.length == 0)
                            this.filteredData = this.data;
                        if (obj) {
                            if (key == "dateFilter") {
                                if(obj !== null){
                                    for (var i = 0; i < this.filteredData.length; i++) {
                                    var begin = new Date(this.filteredData[i].beginDate).getTime();
                                    var end = new Date(this.filteredData[i].endDate).getTime();
                                    var middle = new Date(obj).getTime();
                                    if (begin <= middle && middle <= end) {
                                        fData.push(this.filteredData[i]);
                                    }
                                }
                                this.filteredData = fData;
                                this.data = fData;
                                } 
                            } 

                            else {
                                var fObj = {};
                                if (!angular.isArray(obj)) {
                                    fObj[key] = obj;
                                    fData = fData.concat($filter('filter')(this.filteredData, fObj));
                                } else if (angular.isArray(obj)) {
                                    if (obj.length > 0) {
                                        for (var i = 0; i < obj.length; i++) {
                                            if (angular.isDefined(obj[i])) {
                                                fObj[key] = obj[i];
                                                fData = fData.concat($filter('filter')(this.filteredData, fObj));
                                            }
                                        }

                                    }
                                }
                               if (fData.length > 0 ) {
                                    this.filteredData = fData;
                                }
                                else  if (fData.length == 0 && obj.length != 0 )
                                {
                                    this.filteredData = [];
                                }
                            }
                        }
                    }
                };
                if (keyObj) {
                    angular.forEach(keyObj, function(obj, key) {
                        if (obj != undefined)
                            filterObj.applyFilter(obj, key);
                    });
                }
                return filterObj.filteredData;
            }
        }])
        .filter('groupBy', ['pmkr.filterStabilize', function(stabilize){
            return stabilize( function (datas, key) {
                if (!(datas && key)) return;
                var data = oderBydate(datas,'beginDate') 
                var result = {};
                for (var i=0;i<data.length;i++) {
                    if (!result[data[i][key]])
                        result[data[i][key]]=[];
                    result[data[i][key]].push(data[i])
                }
                return result;
            });
            function oderBydate(input, attribute) {
                if (!angular.isObject(input)) return input;

                var array = [];
                for(var objectKey in input) {
                    array.push(input[objectKey]);
                }

                array.sort(function(a, b){
                    a =new Date(a[attribute]).getTime();
                    b = new Date(b[attribute]).getTime();
                    return a - b;
                });
                return array;
            }
        }])
        .factory('pmkr.filterStabilize', [
          'pmkr.memoize',
          function(memoize) {
            function service(fn) {
              function filter() {
                var args = [].slice.call(arguments);
                // always pass a copy of the args so that the original input can't be modified
                args = angular.copy(args);
                // return the `fn` return value or input reference (makes `fn` return optional)
                var filtered = fn.apply(this, args) || args[0];
                return filtered;
              }
              var memoized = memoize(filter);
              return memoized;
            }
            return service;
          }
        ])
        .factory('pmkr.memoize', [
          function() {
            function service() {
              return memoizeFactory.apply(this, arguments);
            }
            function memoizeFactory(fn) {
              var cache = {};
              function memoized() {
                var args = [].slice.call(arguments);
                var key = JSON.stringify(args);
                var fromCache = cache[key];
                if (fromCache) {
                  return fromCache;
                }
                cache[key] = fn.apply(this, arguments);
                return cache[key];
              }
              return memoized;
            }
            return service;
          }
        ]) 
        .config(function($mdDateLocaleProvider) {
          $mdDateLocaleProvider.formatDate = function(date) {
            return date ? moment(date).format('DD-MMM-YYYY') : '';
          };
          
          // $mdDateLocaleProvider.parseDate = function(dateString) {
          //   var m = moment(dateString, 'DD-MMM-YYYY', true);
          //   return m.isValid() ? m.toDate() : new Date(NaN);
          // };
        })
    .controller('TourviewTlsListController', TourviewTlsListController);

  TourviewTlsListController.$inject = ['TourviewTlsService','Notification','$timeout', '$scope', '$http','md5', '$interval', 'currencyFormatService', 'store'];

  function TourviewTlsListController(TourviewTlsService, Notification,$timeout, $scope, $http,md5,$interval, currencyFormatService, store) {
        var vm = this;
        var vm = $scope;
        $scope.datepickerConfig = {
            allowFuture: true,
            dateFormat: 'DD/MMM/YYYY'
        };
        $scope.AgentFilter = "";
        $scope.ChangeValueEditModel = function()
        {
             switch ($scope.ValueEditModel) {
                case 'excursions':
                    $scope.lsDirectModel = [];
                    $scope.checkDirectModel = false;
                    $scope.isProposal = false;
                    $scope.ispackage = false;
                    break;
                case 'package':
                    $scope.checkDirectModel = true;
                    $scope.isProposal = false;
                    $scope.fromProposal = false;
                    $scope.ispackage = true;
                    break;
                case 'proposal':
                    $scope.checkDirectModel = true;
                    $scope.isProposal = true;
                    $scope.fromProposal = true;
                    $scope.ispackage = false;
                    break;
            }
        }
        $scope.Proposalstatus = "NEW";       
        $scope.ShowpopupGetValuePriceBand = function(number)
        {
            if(number== 2)
            {
                DataTourForPriceBand();
                $("#popupSevPriceBand").modal('hide')
                $("#popupGetValuePriceBand").modal('show');
            }
            else
            {
                $("#popupGetValuePriceBand").modal('hide')
                $("#popupSevPriceBand").modal('show');
            }
            
        }
// ===========================================================================================================================================
// ===========================================================================================================================================
// ===========================================================================================================================================
// markup for all
        $scope.MarkupAllLangding = function()
        {
            $("#popupMakupForALL").modal('show');
        };
        $scope.markupChangelsGroup = function(vl)
        {
            if(vl.temp == undefined && vl.temp == null)
                vl.temp =  vl.price

            vl.price = (vl.markup == 0 && vl.markup == null) ? vl.temp : vl.temp + (vl.temp*vl.markup)/100;
            $scope.Viewpax();
        }
        $scope.markupChangelsVehicle = function(vl)
        {
            if(vl.temp == undefined && vl.temp == null)
                vl.temp = vl.excursion ='excursion'? (vl.price/vl.min) : vl.price;              
            vl.price = ((vl.markup == 0 && vl.markup == null) ? vl.temp : vl.temp + (vl.temp*vl.markup)/100)*vl.min;
            $scope.Viewpax();
        }
        $scope.markupChangelsAccumulated = function(vl)
        {
            if(vl.temp == undefined && vl.temp == null)
               vl.temp =  vl.price
            vl.price = (vl.markup == 0 && vl.markup == null) ? vl.temp : vl.temp + (vl.temp*vl.markup)/100;
           $scope.Viewpax();
        }
// ===========================================================================================================================================

// CALCULATOR LANDING 
        $scope.ChangeLandingMarkup = function(LandingMK)
        {
            $scope.LandingMK = LandingMK;
            $scope.Viewpax();
        }
       
// ===========================================================================================================================================
        function sortJSON(data, key, way) {
            return data.sort(function(a, b) {
                var x = a[key]; var y = b[key];
                if (way === '123' ) { return ((x < y) ? -1 : ((x > y) ? 1 : 0)); }
                if (way === '321') { return ((x > y) ? -1 : ((x < y) ? 1 : 0)); }
            });
        }
        function IsInteger(n){
            return Number(n) === n && n % 1 === 0;
        }
        function ViewContentExcursion(data){
            var textview = "";
            var flag = false;
            var db = sortJSON(data,'day','123')
             angular.forEach(db, function(vl, key) {
                if(vl.content !== null && vl.content !== undefined && vl.content !=="")
                {
                    if(IsInteger(vl.day))
                    {
                         var title = '<div style="background:#e79da1;font:17px Garamond;height:1cm;line-height: 36px;><span style="font-size:18px"><strong> Day '+vl.day+': ' + vl.location + '</strong></span></div> <Br>'
                         title += '<p><span style="font-size:18px"><strong>'+vl.name+'</strong></span></p>'
                         textview += title + vl.content;
                    }
                    else{
                        // var title = '<div style="background:#c67878; width:100%"><span style="font-size:18px"><strong> Day '+vl.day+': ' + vl.location + '</strong></span></div> <Br>'
                        var title = '<p><span style="font-size:18px"><strong>'+vl.name+'</strong></span></p>'
                         textview += title + vl.content;
                    }
                    flag = true;
                }
            });
            if(flag) CKEDITOR.instances.editor1.setData(textview);
        }
        $scope.AddToListPriceBand = function(item)
        {
            var temp = {
                day:0,
                excursion:'excursion',
                name: item.excursionName,
                location: item.location,
                types: "",
                content: item.content,
                listvalue: [],
                currency: item.currency,
                code: item.code
            };
            for (var i = 0; i < item.tiers.length; i++) {
                if(item.tiers[i].pax !== 0)
                {
                    temp.listvalue.push({
                    name: "",
                    min: item.tiers[i].pax,
                        max: (i+1)< item.tiers.length ? (item.tiers[i+1].pax-1 == -1? item.tiers[i].pax:  item.tiers[i+1].pax-1) : item.tiers[i].pax,
                        price: $scope.GetPriceFromExchangeRate((item.tiers[i].price * item.tiers[i].pax) , item.currency),
                        priceservice: $scope.GetPriceFromExchangeRate((item.tiers[i].price * item.tiers[i].pax) , item.currency)
                    });
                }                
            }
            console.log(temp);
            $scope.lsVehicle.push(temp);
            if(!$scope.IsloadEdit)
            {
                ViewContentExcursion($scope.lsVehicle);
            }
            
            $("#popupGetValuePriceBand").modal('hide');
            $scope.Viewpax();
        }

        $scope.maxSizeForPriceBand = 5;
        $scope.SearchTourForPriceBand = function(){
            DataTourForPriceBand();
        }
        function DataTourForPriceBand() {
            
            $scope.LoadDataSearchForPriceBand = true;
            var param = {};
            if ($scope.locationTourFilter !== '') param.location = { $in: $scope.locationTourFilter };
            if ($scope.durationTourFilter !== '') param.category = { $in: $scope.durationTourFilter };
            if ($scope.CategoryTourFilter !==  '') param.priority = { $in: $scope.CategoryTourFilter };
            param.isActive = true;
            if($scope.IsReservation == true)
            {	
                //param.createPer =  { $in: store.get("User").username } ;
                //var agname = { '$in': $scope.AgentFilter }
                param.ValueEditModel = { $in: ['excursions',undefined, ''] }
                if($scope.FilterDateDay != "") {
                    param.beginDate = { "$lt": $scope.FilterDateDay };
                    param.endDate = { "$gt": $scope.FilterDateDay };
                }
            }
            else
            {
                if($scope.FilterDateDay != "") {
                    param.beginDate = { "$lt": $scope.FilterDateDay };
                    param.endDate = { "$gt": $scope.FilterDateDay };
                }
            }
            $http.post('/api/tourviewTls/loadalldata', {param: param,_pageNumber: $scope.currentPageForPriceBand, search: $scope.excursionTourFilter})
                .success(function(rs) {
                    $scope.ListExcursionForPriceBand = rs.showDataEdit;
                    $scope.totalPagingForPriceBand =rs.count;
                    $scope.LoadDataSearchForPriceBand = false;
                })
                .error(function(rs) {

                })            
        }
        $scope.ClearTourFilterForPriceBand = function() {
            $scope.excursionTourFilter = "";
            $scope.CategoryTourFilter = "";
            $scope.locationTourFilter = "";
            $scope.durationTourFilter = "";
            $scope.currentPageForPriceBand =1;
            $scope.FilterDateDay = "";
            DataTourForPriceBand();
        }
        // ------------------------------------------------------------------------------------------------------

        function newDateConvertTotext(date)
        {
            // "DateGroup" : "31 Dec, 2016 - 30 Oct, 2017",
            var monthShortNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
            var t = new Date(date);
            return t.getDate()+' '+monthShortNames[t.getMonth()]+', '+t.getFullYear();            
        };
        $scope.FilterDate1 = "";
        $scope.FilterDate2 = "";
        $scope.FilterDate3 = "";
        $scope.info = {Proposal: "",noteTiers: "* Price Per Person"
        };
        $scope.availab = { all: true, sun: true, mon: true, tue: true, wed: true, thu: true, fri: true, sat: true }
        $scope.CheckDirectSetup ={
            DirectSetup : true,
            CRM: false,
           HotelMkALL: 0
        }
        $scope.SelectMRM_DM = function(vl)
        {
            if(vl=="CRM"){
                $scope.CheckDirectSetup.CRM = true;
                $scope.CheckDirectSetup.DirectSetup = false;
                $scope.crmSelect = true;
                $scope.CheckDirectSetup.HotelMkALL = 0;
                $scope.LandingMK = 0;
                    angular.forEach($scope.lsDirectModel, function(vlDM, key) {
                            var temp = vlDM;
                            temp.tiers = [];
                            for (var i = 0; i < $scope.tiers.length; i++) {
                                 if ($scope.tiers[i].pax != null) {
                                        var item = { pax:$scope.tiers[i].pax, price:0 };
                                        if($scope.tiers[i].pax == 1)
                                        {
                                            var priceTemp = $scope.isProposal == true ?  vlDM.priceSGLRoom*vlDM.rooms*vlDM.nights  : (vlDM.priceSGLRoom*vlDM.nights + $scope.tiers[i].price)*vlDM.rooms;  
                                            item.price = priceTemp
                                        }
                                        else {
                                            var priceTemp = $scope.isProposal == true ?  ((vlDM.priceShareRoom *vlDM.nights) / vlDM.occupancy)*vlDM.rooms : (((vlDM.priceShareRoom *vlDM.nights) / vlDM.occupancy)+ $scope.tiers[i].price)*vlDM.rooms;  
                                            item.price = priceTemp
                                        }
                                        temp.tiers.push(item);
                                    }
                            }
                    });
                    $scope.Viewpax();
            }
            else if(vl=="DM")
            {
                $scope.CheckDirectSetup.CRM = false;
                $scope.CheckDirectSetup.DirectSetup = true;
                $scope.crmSelect = false;
                $scope.HidenMarkupline = false;
            }
            else { 
               if(vl=="ALL")
               {
	               	if( $scope.CheckDirectSetup.HotelMkALL == undefined &&  ($scope.CheckDirectSetup.HotelMkALL == null || $scope.CheckDirectSetup.HotelMkALL == 0) )
	               	{
	               	   $scope.HidenMarkupline = false;
	                   $scope.crmSelect = false;
                            $timeout(function(){
                                    angular.forEach($scope.lsDirectModel, function(vlDM, key) {
                                        vlDM.SGLSuppl = (((vlDM.priceSGLRoom * vlDM.nights ) - ((vlDM.priceShareRoom /vlDM.occupancy) * vlDM.nights))+ (((vlDM.priceSGLRoom * vlDM.nights ) - ((vlDM.priceShareRoom /vlDM.occupancy) * vlDM.nights)) * ($scope.CheckDirectSetup.HotelMkALL || 0))/100 )*vlDM.rooms;
                                        var temp = vlDM;
                                        temp.tiers = [];
                                        for (var i = 0; i < $scope.tiers.length; i++) {
                                            if ($scope.tiers[i].pax != null) {
                                                    var item = { pax:$scope.tiers[i].pax, price:0 };
                                                    if($scope.tiers[i].pax == 1)
                                                    {
                                                        var priceTemp = $scope.isProposal == true ?  vlDM.priceSGLRoom*vlDM.rooms*vlDM.nights : (vlDM.priceSGLRoom + $scope.tiers[i].price)*vlDM.rooms*vlDM.nights;  
                                                        item.price = priceTemp
                                                    }
                                                    else { 
                                                        var priceTemp = $scope.isProposal == true ?  ((vlDM.priceShareRoom *vlDM.nights) / vlDM.occupancy)*vlDM.rooms : (((vlDM.priceShareRoom *vlDM.nights) / vlDM.occupancy) + $scope.tiers[i].price)*vlDM.rooms;  
                                                        item.price = priceTemp
                                                    }
                                                    vlDM.tiers.push(item);
                                                }
                                        }
                                });
                                $scope.$apply();
                                $scope.Viewpax();
                            },50)
                       
	               	}
	               	else
	               	{
	               		$scope.HidenMarkupline = true; 
                             $timeout(function(){
                                $scope.$apply();                                
                                $scope.Viewpax();
                            },50)
                          
	               	}
                   
               }
            }
        }
        $scope.ChangeAvailability = function() {
            $scope.availab.sun = $scope.availab.all;
            $scope.availab.mon = $scope.availab.all;
            $scope.availab.tue = $scope.availab.all;
            $scope.availab.wed = $scope.availab.all;
            $scope.availab.thu = $scope.availab.all;
            $scope.availab.fri = $scope.availab.all;
            $scope.availab.sat = $scope.availab.all;
        }

        $scope.startTime = { hour: 8, minute: 0 };
        $scope.GetStartTime = function() {
            var time = $("#timevalue").val();
            var h = parseInt(time.substr(0, 2));
            var m = parseInt(time.substr(3, 2));
            $scope.startTime = { hour: h, minute: m };
            $("#iptTimeSelect").click();
        }
        $scope.SetStartTime = function() {
            if ($scope.startTime == undefined) {
                $("#timevalue").val("");
            } else {
                var h = $scope.startTime.hour || 0;
                var m = $scope.startTime.minute || 0;
                h = h < 10 ? "0" + h : h;
                m = m < 10 ? "0" + m : m;
                $("#timevalue").val(h + ":" + m);
            }
        }
        $scope.Expand = function(index) {
            $("#IdExpand" + index).slideToggle();
        }
        $scope.ShowHideProfit = function() {
            $(".divTrProfitResult").slideToggle();
        }
        $scope.ChangrCurrency = function() {
            var beforeCurrency = $scope.beforeCurrency;
            angular.forEach($scope.lsGroup, function(vl, key) {
                vl.price = $scope.GetPriceFromExchangeRate(vl.priceservice || vl.price, vl.currency || $scope.beforeCurrency);
            });
            
            angular.forEach($scope.lsAccumulated, function(vl, key) {
                vl.price = $scope.GetPriceFromExchangeRate(vl.priceservice || vl.price, vl.currency || $scope.beforeCurrency);
            });
            angular.forEach($scope.lsAccommodation, function(vl, key) {
                vl.price = $scope.GetPriceFromExchangeRate(vl.priceservice || vl.price, vl.currency || $scope.beforeCurrency);
            });
            angular.forEach($scope.lsExtra, function(vl, key) {
                vl.price = $scope.GetPriceFromExchangeRate(vl.priceservice || vl.price, vl.currency || $scope.beforeCurrency);
            });
            angular.forEach($scope.lsDirectmarkup, function(vl, key) {
                vl.byPrice = $scope.GetPriceFromExchangeRate(vl.priceservice || vl.byPrice, vl.currency || $scope.beforeCurrency);
                vl.price = $scope.GetPriceFromExchangeRate(vl.priceservice || vl.price, vl.currency || $scope.beforeCurrency);
            });
            if($scope.lsChildPolicy)
            {
                angular.forEach($scope.lsChildPolicy, function(vl, key) {
                    if(vl.discount !== 'Discount by (%)' && confirm("* Currency Convert is affected to  Child Policy / Fixed Rate. Please recheck the Child Policy!!!")){
                            vl.value =  $scope.GetPriceFromExchangeRate(vl.value,vl.currency || $scope.beforeCurrency);
                    }
                });
            }


            $timeout(function(){

                angular.forEach($scope.lsVehicle, function(value, key) {
                    console.log($scope.lsVehicle);
                    angular.forEach(value.listvalue, function(vl, key) {
                        console.log('.:::',vl.priceservice)
                        console.log(value.currency,'-----------------', $scope.beforeCurrency)
                        vl.price = $scope.GetPriceFromExchangeRate(vl.priceservice, value.currency || $scope.beforeCurrency);
                    });
                });

                $scope.$apply()
                angular.forEach($scope.lsDirectModel, function(vlDM, key) {
                if (beforeCurrency == $scope.Curency)
                {
                    vlDM.priceShareRoom =  vlDM.priceShareRoom;
                    vlDM.priceSGLRoom =  vlDM.priceSGLRoom;
                    vlDM.SGLSuppl =  vlDM.SGLSuppl;
                    vlDM.twinShare =  vlDM.twinShare;
                }
                else if ($scope.Curency == "THB") {
                    vlDM.priceShareRoom =  vlDM.priceShareRoom * $scope.VND / $scope.USD;
                    vlDM.priceSGLRoom =  vlDM.priceSGLRoom * $scope.VND / $scope.USD;
                    vlDM.SGLSuppl =  vlDM.SGLSuppl * $scope.VND / $scope.USD;
                    vlDM.twinShare =  vlDM.twinShare * $scope.VND / $scope.USD;
                }
                else{
                    vlDM.priceShareRoom =  vlDM.priceShareRoom * $scope.USD / $scope.VND;
                    vlDM.priceSGLRoom =  vlDM.priceSGLRoom * $scope.USD / $scope.VND;
                    vlDM.SGLSuppl =  vlDM.SGLSuppl * $scope.USD / $scope.VND;
                    vlDM.twinShare =  vlDM.twinShare * $scope.USD / $scope.VND;
                }
                vlDM.SGLSuppl = (((vlDM.priceSGLRoom * vlDM.nights ) - ((vlDM.priceShareRoom /vlDM.occupancy) * vlDM.nights))+ (((vlDM.priceSGLRoom * vlDM.nights ) - ((vlDM.priceShareRoom /vlDM.occupancy) * vlDM.nights)) * ($scope.CheckDirectSetup.HotelMkALL || 0))/100 )*vlDM.rooms;
                vlDM.tiers = [];
                for (var i = 0; i < $scope.tiers.length; i++) {
                     if ($scope.tiers[i].pax != null) {
                            var item = { pax:$scope.tiers[i].pax, price:0 };
                            if($scope.tiers[i].pax == 1)
                            {  
                                  var priceTemp = $scope.isProposal == true ?  vlDM.priceSGLRoom*vlDM.nights*vlDM.rooms : (vlDM.priceSGLRoom + $scope.tiers[i].price)*vlDM.nights*vlDM.rooms;
                                  item.price=  priceTemp + (($scope.CheckDirectSetup.HotelMkALL || 0) * priceTemp)/100;
                            }
                            else {
                                 var priceTemp = $scope.isProposal == true ?  ((vlDM.priceShareRoom *vlDM.nights) / vlDM.occupancy)*vlDM.rooms : (((vlDM.priceShareRoom *vlDM.nights) / vlDM.occupancy) + $scope.tiers[i].price)*vlDM.rooms;
                                 item.price=  priceTemp + (priceTemp *($scope.CheckDirectSetup.HotelMkALL || 0)/100);
                            }
                            vlDM.tiers.push(item);
                        }
                    }   
                });
                if($scope.isProposal == true)
                {
                    langdingTotal();
                }
                $scope.$apply()
                $scope.beforeCurrency = $scope.Curency;
                 $scope.Viewpax();
            },300)
           
        }         
        $scope.maxSize = 5;
        $scope.SearchTour = function(){
            LoadDataSearchEdit();
        }
        function LoadDataSearchEdit() {
            $scope.LoadDataSearch = true;
            var param = {};
            if ($scope.locationTourFilter !== '') param.location = { $in: $scope.locationTourFilter };
            if ($scope.durationTourFilter !== '') param.category = { $in: $scope.durationTourFilter };
            if ($scope.CategoryTourFilter !==  '') param.priority = { $in: $scope.CategoryTourFilter };
            if($scope.IsReservation == true)
            {
                
                param.createPer = { $in: store.get("User").username } ;
                //var agname = { '$in': $scope.AgentFilter }
                param.ValueEditModel = { $in: ['proposal'] }
                if ($scope.AgentFilter !==  '') param.agname = { '$in': $scope.AgentFilter } ;
            }
            else
            {
                 if($scope.FilterDateDay != "") {
                    param.beginDate = { "$lt": $scope.FilterDateDay };
                    param.endDate = { "$gt": $scope.FilterDateDay };
                }
            }
            $http.post('/api/tourviewTls/loadalldata', {param: param,_pageNumber: $scope.currentPage, search: $scope.excursionTourFilter})
                .success(function(rs) {
                    $scope.ListExcursion = rs.showDataEdit;
                    $scope.totalPaging =rs.count;
                    $scope.LoadDataSearch = false;
                })
                .error(function(rs) {

                })
            $("#popupSearchTour").modal('show');
        }
        $scope.Copy = function() {
            var data = {
                tiers: $scope.tiers,
                code: $scope.info.code,
                showGroupCode: $scope.info.showGroupCode,
                Groupcode: $scope.info.Groupcode,
                noteTiers: $scope.info.noteTiers,
                isActive: $scope.info.active,
                excursionName: $scope.info.excursionName,
                category: $scope.info.categoryName,
                location: $scope.info.location,
                market: $scope.listMarket,
                priority: $scope.info.priority,
                currency: $scope.Curency,
                DirectMarkup:$scope.lsDirectmarkup,
                beginDate: $scope.beginDate,
                endDate: $scope.endDate,
                lsChildPolicy: $scope.lsChildPolicy,
                lsGroups: $scope.lsGroup,
                lsVehicle: $scope.lsVehicle,
                listMarketExcept: $scope.listMarketExcept,
                listAgentExcept: $scope.listAgentExcept,
                lsAccumulated: $scope.lsAccumulated,
                lsAccommodation: $scope.lsAccommodation,
                lsExtra: $scope.lsExtra,
                lsTiming: $scope.lsTiming,
                maxTiming: $scope.TotalDuration,
                content: CKEDITOR.instances.editor1.getData(),
                note: CKEDITOR.instances.editor2.getData(),
                slideImages: $scope.ListImageChoose,
                startTime: $scope.startTime,
                availab: $scope.availab,
                lsDirectModel: $scope.lsDirectModel,
                CRM: $scope.CheckDirectSetup.CRM,
                DirectSetup: $scope.CheckDirectSetup.DirectSetup,
                noteDirectModel: $scope.noteDirectModel,
                
                ValueEditModel: $scope.ValueEditModel,
                ObjectAgent: $scope.info.Proposal,
                Proposalstatus: $scope.Proposalstatus,
                LandingMK: $scope.LandingMK,
                HotelMkALL: $scope.CheckDirectSetup.HotelMkALL
            }
            store.set("Tour", data);
            $scope.NgShowPaste = true;
            $scope.NgPaste = false;
        }
        $scope.Cancel = function() {
            store.set("Tour", null);
            $scope.NgShowPaste = false;
            $scope.NgPaste = false;
        }
        $scope.Paste = function() {
            var data = store.get("Tour");
            $scope.tiers = data.tiers;
            $scope.info.code = data.code;
            $scope.info.showGroupCode = data.showGroupCode;
            $scope.info.Groupcode= data.Groupcode;
            $scope.info.noteTiers = data.noteTiers;
            $scope.info.active = data.isActive;
            $scope.info.excursionName = data.excursionName;
            $scope.info.categoryName = data.category;
            $scope.info.Proposal = data.Proposal;
            $scope.info.location = data.location;
            $scope.listMarket = data.market;
            $scope.info.priority = data.priority;
            $scope.Curency = data.currency;
            $scope.lsDirectmarkup=data.DirectMarkup;
            $scope.beginDate = new Date(data.beginDate);
            $scope.endDate = new Date(data.endDate);
            $scope.lsChildPolicy = data.lsChildPolicy;
            $scope.lsGroup = data.lsGroups;
            $scope.lsVehicle = data.lsVehicle;

            $scope.listMarketExcept = data.listMarketExcept;
            $scope.listAgentExcept = data.listAgentExcept;

            $scope.lsAccumulated = data.lsAccumulated;
            $scope.lsAccommodation = data.lsAccommodation;
            $scope.lsExtra = data.lsExtra;
            $scope.lsTiming = data.lsTiming;
            $scope.TotalDuration = data.maxTiming;
            $scope.ListImageChoose = data.slideImages;
            CKEDITOR.instances.editor1.setData(data.content);
            CKEDITOR.instances.editor2.setData(data.note);
            $scope.startTime = data.startTime;
            $scope.availab = data.availab;
            $scope.SetStartTime();
            
            $scope.beforeCurrency = data.currency;
            $scope.ExchangeRate();
            $scope.NgPaste = true;
            $scope.numberTimeForDirectMarkup = data.DirectMarkup.length;
            $scope.ResetDuration();
            $scope.lsDirectModel = data.lsDirectModel;
            $scope.CRM = data.CRM,
            $scope.DirectSetup = data.DirectSetup,
            $scope.checkDirectModel = data.checkDirectModel;
            $scope.ValueEditModel = data.ValueEditModel;
            $scope.info.Proposal = data.ObjectAgent;
            $scope.Proposalstatus = data.Proposalstatus;
            $scope.LandingMK = data.LandingMK;
            $scope.CheckDirectSetup.HotelMkALL = data.HotelMkALL;
            $scope.noteDirectModel = data.noteDirectModel;
            
            $scope.ChangeValueEditModel();

        }
        $scope.DeleteTour = function(tour, $index) {
            if (confirm("WARNING!, this excursion is about deleted, please confirm the erase")) {
                $http.post('/api/tourviewTls/deletetour', tour)
                    .success(function(rs) {
                        $scope.ListExcursion.splice($scope.ListExcursion.indexOf(tour), 1);
                    })
                    .error(function(rs) {

                    });
                $("#popupSearchTour").modal('show');
            }
        }

        
        $scope.serviceNameFilter = "";
        $scope.locationFilter = "";
        $scope.supplierFilter = "";
        $scope.typesFilter = "";
        $scope.ClearServiceFilter = function() {
            $scope.serviceNameFilter = "";
            $scope.locationFilter = "";
            $scope.typesFilter = "";
            $scope.FilterDate1 = "";
            $scope.FilterDate2 = "";
            $scope.FilterDate3 = "";
            $scope.supplierFilter = "";
        }

        var objDate = new Date();
        $scope.FilterDateDay = objDate.getDate() + "/" + objDate.toLocaleString("en-us", { month: "short" }) + "/" + objDate.getFullYear();

        $scope.excursionTourFilter = "";
        $scope.durationTourFilter = "";
        $scope.locationTourFilter = "";
        $scope.CategoryTourFilter = "";
        $scope.ClearTourFilter = function() {
            $scope.FilterDateDay = "";
            $scope.excursionTourFilter = "";
            $scope.CategoryTourFilter = "";
            $scope.locationTourFilter = "";
            $scope.durationTourFilter = "";
            $scope.currentPage =1;
            LoadDataSearchEdit();
        }
        $scope.FilterTour = function(tour) {
            if (($scope.durationTourFilter == undefined || $scope.durationTourFilter == "" || tour.category == $scope.durationTourFilter) &&
                ($scope.locationTourFilter == undefined || $scope.locationTourFilter == "" || tour.location == $scope.locationTourFilter)
                && ($scope.CategoryTourFilter == undefined || $scope.CategoryTourFilter == "" || tour.priority == $scope.CategoryTourFilter)
                )

                return true;
            return false;
        }
        $scope.FilterService = function(service) {
            if (($scope.locationFilter == undefined || $scope.locationFilter == "" || service.location == $scope.locationFilter) &&
                ($scope.typesFilter == undefined || $scope.typesFilter == "" || service.types == $scope.typesFilter) &&
                ($scope.supplierFilter == undefined || $scope.supplierFilter == "" || service.supplier == $scope.supplierFilter))
                return true;
            return false;
        }
        $scope.boolSaveUpdate = true;
        $scope._id = "";
        $scope.ChooseTour = function(item) {

            $scope.IsloadEdit = true;
            $scope.itemDataExport = item;
            $scope._id = item._id;
            $scope.boolSaveUpdate = false;
            $scope.tiers = item.tiers;
            $scope.info.code = item.code;
            $scope.info.showGroupCode = item.showGroupCode;
            $scope.info.Groupcode= item.Groupcode;
            $scope.info.noteTiers = item.noteTiers;
            $scope.info.active = item.isActive;
            $scope.info.excursionName = item.excursionName;
            $scope.info.categoryName = item.category;
            $scope.info.Proposal = item.Proposal;
            $scope.info.location = item.location;
            $scope.info.priority = item.priority;
            $scope.info.updateTime = item.updateTime;
            $scope.info.createTime = item.createTime;
            $scope.info.updatePer = item.updatePer;
            $scope.info.createPer = item.createPer;
            $scope.lsDirectmarkup = item.DirectMarkup;
            $scope.listMarket = item.market;
            $scope.Curency = item.currency;
            $scope.beginDate =new Date(item.beginDate);
            $scope.endDate = new Date(item.endDate);
            $scope.lsChildPolicy = item.lsChildPolicy;
            $scope.lsGroup = item.lsGroups;
            $scope.lsVehicle = item.lsVehicle;

             $scope.listMarketExcept = item.listMarketExcept;
              $scope.listAgentExcept = item.listAgentExcept;

            $scope.lsAccumulated = item.lsAccumulated;
            $scope.lsAccommodation = item.lsAccommodation;
            $scope.lsExtra = item.lsExtra;
            $scope.lsTiming = item.lsTiming;
            $scope.startTime = item.startTime;
            if (item.availab != undefined)
                $scope.availab = item.availab;
            
            $scope.ListImageChoose = item.slideImages;
            $scope.SetStartTime();
            $scope.numberTimeForDirectMarkup = item.DirectMarkup.length;
            CKEDITOR.instances.editor1.setData(item.content);
            CKEDITOR.instances.editor2.setData(item.note);
            $scope.lsDirectModel = item.lsDirectModel;           
            $scope.CheckDirectSetup.CRM = item.CRM,
            $scope.CheckDirectSetup.DirectSetup = item.DirectSetup,
            $scope.checkDirectModel = item.checkDirectModel;
            $scope.noteDirectModel = item.noteDirectModel;
            $scope.ValueEditModel = item.ValueEditModel;
            $scope.info.Proposal = item.ObjectAgent;
            $scope.Proposalstatus = item.Proposalstatus;
            $scope.LandingMK = item.LandingMK;
            $scope.CheckDirectSetup.HotelMkALL = item.HotelMkALL;
            $scope.beforeCurrency = item.currency;
            $scope.ExchangeRate();
                    $("#popupSearchTour").modal('hide');
                    $scope.ResetDuration();
                    $scope.ChangeValueEditModel();
             $timeout(function(){
                    $scope.$apply();
                    $scope.Viewpax();
             },50);
            

        }

        $scope.UpdateAll = function() {
            if ($scope.info.code == "") {
                alert("Code isn't empty");
                return;
            }
            if ($scope.beginDate.length <= 10 || $scope.endDate.length <= 10) {
                alert("Begin day or end day isn't true!");
                return;
            }
            $http.post('/api/tourviewTls/checkexistcode', { code: $scope.info.code, action: 'update', _id: $scope._id })
                .success(function(rs) {

                    if (rs !== "Exist") SendDataToserverUpdate();
                    else alert("Code is existing!");

                })
            console.log($scope.info.Proposal);
            function SendDataToserverUpdate() {
                var sendData = {
                    _id: $scope._id,
                    tiers: $scope.tiers,
                    code: $scope.info.code,
                   showGroupCode:   $scope.info.showGroupCode,
                   Groupcode : $scope.info.Groupcode,
                    noteTiers: $scope.info.noteTiers,
                    isActive: $scope.info.active,
                    excursionName: $scope.info.excursionName,
                    category: $scope.info.categoryName,
                    location: $scope.info.location,
                     priority: $scope.info.priority,
                    updateTime: (new Date()).getTime(),
                    createTime: $scope.info.createTime,
                    updatePer: store.get("User").username,
                    createPer: ($scope.info.createPer || store.get("User").username) ,
                    DirectMarkup: $scope.lsDirectmarkup,
                    market: $scope.listMarket,
                    currency: $scope.Curency,
                    beginDate: $scope.beginDate,
                    endDate: $scope.endDate,
                    lsChildPolicy: $scope.lsChildPolicy,
                    lsGroups: $scope.lsGroup,
                    lsVehicle: $scope.lsVehicle,
                    listMarketExcept: $scope.listMarketExcept,
                    listAgentExcept: $scope.listAgentExcept,
                    lsAccumulated: $scope.lsAccumulated,
                    lsAccommodation: $scope.lsAccommodation,
                    lsExtra: $scope.lsExtra,
                    lsTiming: $scope.lsTiming,
                    maxTiming: $scope.TotalDuration,
                    content: CKEDITOR.instances.editor1.getData(),
                    note: CKEDITOR.instances.editor2.getData(),
                    slideImages: $scope.ListImageChoose,
                    startTime: $scope.startTime,
                    availab: $scope.availab,
                    lsDirectModel: $scope.lsDirectModel,
                    CRM : $scope.CheckDirectSetup.CRM,
                    DirectSetup : $scope.CheckDirectSetup.DirectSetup,
                    
                    agname: $scope.info.Proposal == undefined? "" : $scope.info.Proposal.agname == undefined? "" : $scope.info.Proposal.agname,
                    md5code:   $scope.info.Proposal == undefined? "" : $scope.info.Proposal.md5code == undefined? "" : $scope.info.Proposal.md5code,

                    checkDirectModel: $scope.checkDirectModel,
                    noteDirectModel: $scope.noteDirectModel,
                    ValueEditModel: $scope.ValueEditModel,
                    ObjectAgent: $scope.info.Proposal,
                    Proposalstatus: $scope.Proposalstatus,
                    LandingMK: $scope.LandingMK,
                    HotelMkALL: $scope.CheckDirectSetup.HotelMkALL
                    
                }
                
                $http.put('/api/tourviewTls/datasave', sendData)
                    .success(function(rs) {
                        $scope.saveRs = "Update Success..!";
                        $('#savePopup').modal('show');
                    });
            }


        }

        $scope.lsServiceType = [];
        $scope.lsGeoTree = [];
        $scope.lsExchangeRate = [];
        $scope.lsCategory = [];
        function convertToken() {
            var date = new Date();
            var day = date.getDate() < 10 ? "0" + date.getDate() : date.getDate();
            var month = (date.getMonth() + 1) < 10 ? "0" + (date.getMonth() + 1) : (date.getMonth() + 1);
            var Year = date.getFullYear();
            var text = day +""+ month +""+ Year;
            return md5.createHash(text + 'DAVN')
        }
        // $scope.CheckIsReservation = function(txt)
        // {
        //     if(txt)
        //         $scope.ValueEditModel = 'proposal';
        // }
        $scope.InitService = function() {              
            $scope.username = store.get("User").username;
            $('.clockpicker').clockpicker({
                placement: 'right',
                align: 'right',
                donetext: 'Done',
                afterDone: function() {
                    $scope.GetStartTime();
                    // $scope.ResetDuration();
                }
            });
            $scope.NgShowPaste = store.get("Tour") == null ? false : true;
            $http.get('/api/tourviewTls/servicetype')
                .success(function(rs) {
                    $scope.lsServiceType = rs;
                    $scope.Group.types = rs.length > 0 ? rs[0].name : "";
                    $scope.Vehicle.types = rs.length > 0 ? rs[0].name : "";
                }).error(function(rs) {});

            $http.get('http://da-tariff.com/modules.php?name=HotelView_TL&op=marketjson&token='+ convertToken())
            .success(function(data) {
                $scope.listMarketFromHotel = [];
                angular.forEach(data, function(vl, key) {
                    $scope.listMarketFromHotel.push(vl.smname);
                });
            })
            .error(function(data) {
                alert("Error!")
            });
            $http.get('http://da-tariff.com/modules.php?name=HotelView_TL&op=agentjson&token='+ convertToken())
            .success(function(data) {
                $scope.listProposalFromHotel = [];
                angular.forEach(data, function(vl, key) {
                    if(key==0)
                    {
                     var firstItem ={
                        agref:'--',
                        agname:'--',
                        agmail:undefined,
                        ccode:undefined,
                        cname:undefined,
                        cemail:undefined,
                        md5code:undefined,
                        cmarket:undefined}
                        $scope.listProposalFromHotel.push(firstItem);
                    }
                    if(vl.agname != null && vl.md5code != null)
                        $scope.listProposalFromHotel.push(vl);
                });
            })
            .error(function(data) {
                alert("Error!")
            });

            $http.get('/api/tourviewTls/geotree')
                .success(function(rs) {
                    $scope.lsGeoTree = rs;
                    // $scope.info.location = rs.length > 0 ? rs[0].name : "";
                }).error(function(rs) {});

            $http.get('/api/tourviewTls/exchangerate')
                .success(function(rs) {
                    $scope.lsExchangeRate = rs;
                    $scope.ExchangeRate();
                }).error(function(rs) {});

            $http.get('/api/tourviewTls/Category')
                .success(function(rs) {
                    $scope.lsCategory = rs;
                    // $scope.info.categoryName = rs.length > 0 ? rs[0].name : "";
                }).error(function(rs) {});

            $http.get('/api/tourviewTls/priority')
                .success(function(rs) {
                    $scope.lsPriority = rs;
                })
                .error(function(rs) {

                });
            $http.get('/api/tourviewTls/supplier')
            .success(function(rs) {
                $scope.lsSupplier = rs;
            })
            .error(function(rs) {

            });

        }
        //$scope.InitService();

        var date = new Date();
        $scope.beginDate = date;
        $scope.endDate = date;
        $scope.ExchangeRate = function() {
            var middle = new Date();
            if ($scope.beginDate != null && $scope.beginDate != undefined && $scope.beginDate != '')
                middle = new Date($scope.beginDate);
            var lable = true;
            for (var i = 0; i < $scope.lsExchangeRate.length; i++) {
                var begin = new Date($scope.lsExchangeRate[i].beginDate);
                var end = new Date($scope.lsExchangeRate[i].endDate);
                if (begin <= middle && middle <= end) {
                    $scope.VND = $scope.lsExchangeRate[i].VND;
                    $scope.USD = $scope.lsExchangeRate[i].USD;
                    lable = false;
                    break;
                }
            }
            if (lable) {
                alert("Sytem hasn't had ExchangeRate yet");
            }
        }

        $scope.GetPriceFromExchangeRate = function(priceservice, currency) {
            if (currency == $scope.Curency)
                return priceservice;
            if ($scope.Curency == "THB") {
                return priceservice * $scope.VND / $scope.USD;
            }
            return priceservice * $scope.USD / $scope.VND;
        }
        //Controlls
        $scope.SaveAll = function() {
           if ($scope.info.code == "") {
                alert("Code isn't empty");
                return;
            }
            if ($scope.beginDate == null || $scope.endDate == null || $scope.beginDate.length <= 8 || $scope.endDate.length <= 8) {
                alert("Begin day or end day isn't true!");
                return;
            }
            $http.post('/api/tourviewTls/checkexistcode', { code: $scope.info.code, action: 'add', _id: 123 })
                .success(function(rs) {
                    if (rs !== "Exist") {
                        var sendData = {
                            tiers: $scope.tiers,
                            code: $scope.info.code,
                            showGroupCode:   $scope.info.showGroupCode,
                            Groupcode : $scope.info.Groupcode,
                            noteTiers: $scope.info.noteTiers,
                            isActive: $scope.info.active,
                            excursionName: $scope.info.excursionName,
                            category: $scope.info.categoryName,
                            location: $scope.info.location,
                            priority: $scope.info.priority,
                            updateTime: 0,
                            createTime: (new Date()).getTime(),
                            updatePer: "",
                            createPer: store.get("User").username,
                            DirectMarkup:  $scope.lsDirectmarkup,
                            market: $scope.listMarket,
                            currency: $scope.Curency,
                            beginDate: $scope.beginDate,
                            endDate: $scope.endDate,
                            lsGroups: $scope.lsGroup,
                            lsVehicle: $scope.lsVehicle,
                             listMarketExcept: $scope.listMarketExcept,
                              listAgentExcept: $scope.listAgentExcept,
                            lsAccumulated: $scope.lsAccumulated,
                            lsAccommodation: $scope.lsAccommodation,
                            lsExtra: $scope.lsExtra,
                            lsTiming: $scope.lsTiming,
                            maxTiming: $scope.TotalDuration,
                            content: CKEDITOR.instances.editor1.getData(),
                            note: CKEDITOR.instances.editor2.getData(),
                            slideImages: $scope.ListImageChoose,
                            startTime: $scope.startTime,
                            availab: $scope.availab,
                            lsDirectModel: $scope.lsDirectModel,
                            CRM:$scope.CheckDirectSetup.CRM,
                            DirectSetup: $scope.CheckDirectSetup.DirectSetup,
                            checkDirectModel: $scope.checkDirectModel,
                            noteDirectModel: $scope.noteDirectModel,
                            ValueEditModel: $scope.ValueEditModel,
                            ObjectAgent: $scope.info.Proposal,
                            agname: $scope.info.Proposal == undefined? "" : $scope.info.Proposal.agname == undefined? "" : $scope.info.Proposal.agname,
                            md5code: $scope.info.Proposal == undefined? "" : $scope.info.Proposal.md5code == undefined? "" : $scope.info.Proposal.md5code,
                            
                            Proposalstatus: $scope.Proposalstatus,
                            LandingMK: $scope.LandingMK,
                            HotelMkALL: $scope.CheckDirectSetup.HotelMkALL

                        }
                        $http.post('/api/tourviewTls/datasave', sendData)
                            .success(function(rs) {
                                $scope.saveRs = "Save Success..!";
                                $('#savePopup').modal('show');
                                $scope.boolSaveUpdate = false;
                                $scope._id = rs._id;
                            });
                    } else alert("Code is existing!");

                })
        }
        $scope.beforeCurrency = "USD";
        $scope.Curency = "USD";
        $scope.Market = "WW";
        
        $scope.listMarket = [{ vlMarket: "WW" }];
        $scope.updateMarket = function() {
            var objectMarket = {
                vlMarket: $scope.Market
            }
            $scope.listMarket.push(objectMarket);
        }
        $scope.delectMarket = function($index) {
            $scope.listMarket.splice($index, 1);
        }
        $scope.MarketExcept = "SP";
        $scope.listMarketExcept = [{vlMarket:'SP'}];
        $scope.updateMarketExcept = function() {
            var objectMarket = {
                vlMarket: $scope.MarketExcept
            }
            $scope.listMarketExcept.push(objectMarket);
        }
        $scope.delectMarketExcept = function($index) {
            $scope.listMarketExcept.splice($index, 1);
        }

        $scope.listAgentExcept = [];
        $scope.AgentExcept = function() {
            $scope.listAgentExcept.push($scope.info.ProposalExcept);
        }
        $scope.delectAgentExcept = function($index) {
            $scope.listAgentExcept.splice($index, 1);
        }

        //service
        $scope.listSevPriceBand = [];
        $scope.listSevGroup = [];
        $scope.listSevAccumulated = [];
        $scope.LoadService = function(servicename) { //all service with servicename
            $scope["list" + servicename] = [];
            $("#popup" + servicename).modal('show');
            $http.post('/api/tourviewTls/LoadService', { name: servicename })
                .success(function(rs) {
                    $scope["list" + servicename] = rs;

                }).error(function(rs) {

                });

        };
        $scope.ChooseSevPriceBand = function(service, servicename) {
            var temp = {
                id: service._id,
                supplier: service.supplier,
                beginDate: service.beginDate,
                endDate: service.endDate,
                supplierTourCode: service.supplierTourCode,
                name: service.name,
                types: service.types,
                listvalue: [],
                currency: service.priceband.currencyCh
            };
            for (var i = 0; i < service.priceband.list.length; i++) {
                temp.listvalue.push({
                    name: service.priceband.list[i].name,
                    min: service.priceband.list[i].min,
                    max: service.priceband.list[i].max,
                    price: $scope.GetPriceFromExchangeRate(service.priceband.list[i].price, service.priceband.currency),
                    priceservice: service.priceband.list[i].price
                });
            }
            $scope.lsVehicle.push(temp);
            $("#popup" + servicename).modal('hide');
            $scope.Viewpax();
        }
        $scope.ChooseSevGroup = function(service, servicename) {
            $scope.Group = {
                id: service._id,
                name: service.name,
                beginDate: service.beginDate,
                endDate: service.endDate,
                supplier: service.supplier,
                supplierTourCode: service.supplierTourCode,
                types: service.types,
                price: $scope.GetPriceFromExchangeRate(service.group.price, service.group.currency),
                priceservice: service.group.price,
                groupPerson: $scope.Group.groupPerson,
                currency: service.group.currency
            };
            $scope.Group.groupPerson = "Per Group";
            $("#popup" + servicename).modal('hide');
            $scope.Viewpax();
        }
         $scope.showsupplierTourCode = function(item){
            $scope.itemPopupValue = item;
            $("#itemPopupValue").modal('show');
        }
        $scope.ChooseSevAccumulated = function(id, service, servicename) {
            console.log(service);
            var temp = {
                id: id,
                name: service.name,
                types: service.types,
                supplier: service.supplier,
                beginDate: service.beginDate,
                endDate: service.endDate,
                supplierTourCode: service.supplierTourCode,
                min: service.accumulated.min,
                max: service.accumulated.max,
                price: $scope.GetPriceFromExchangeRate(service.accumulated.price, service.accumulated.currency),
                priceservice: service.accumulated.price,
                currency: service.accumulated.currency
            };
            $scope.lsAccumulated.push(temp);
            $("#popup" + servicename).modal('hide');
            $scope.Viewpax();
        }
        
        // Group ---------
        $scope.Group = { groupPerson: "Per Group" };
        $scope.gSave = false;
        $scope.lsGroup = [];
        $scope.GroupValue = function(action, group, $index) {
            if (action === "delete" && confirm("Are you sure to delete?")) {
                $scope.lsGroup.splice($scope.lsGroup.indexOf(group), 1);
            } else if (action === "add") {
                var temp = {
                    id: $scope.Group.id || null,
                    supplier:$scope.Group.supplier,
                    supplierTourCode: $scope.Group.supplierTourCode,
                    beginDate: $scope.Group.beginDate,
                    endDate: $scope.Group.endDate,
                    day: $scope.Group.day,
                    name: $scope.Group.name,
                    types: $scope.Group.types,
                    price: $scope.Group.price,
                    groupPerson: $scope.Group.groupPerson,
                    priceservice: $scope.Group.priceservice > 0 ? $scope.Group.priceservice : $scope.Group.price,
                    currency: $scope.Group.currency || $scope.Curency
                }
                $scope.lsGroup.push(temp);
                $scope.Group = { groupPerson: "Per Group" };
            } else if (action === "edit") {
                $scope.Group = group;
                $scope.gSave = true;

            } else if (action === "save") {
                $scope.gSave = false;
                $scope.Group = { groupPerson: "Per Group" };
            }
            $scope.Viewpax();
        }


        // lsChildPolicy ---------
        $scope.childPolicy = { discount: "Discount by (%)" };
        $scope.cSave = false;
        $scope.lsChildPolicy = [];
        $scope.childPolicyValue = function(action, childPolicy, $index) {
            if (action === "delete" && confirm("Are you sure to delete?")) {
                $scope.lsChildPolicy.splice($index, 1);
            } else if (action === "add") {
                var temp = {
                    ageFrom: $scope.childPolicy.ageFrom,
                    ageTo: $scope.childPolicy.ageTo,
                    discount: $scope.childPolicy.discount,
                    value: $scope.childPolicy.value,
                    note: $scope.childPolicy.note
                }
                $scope.lsChildPolicy.push(temp);
                $scope.childPolicy = { discount: "Discount by (%)" };                
            } else if (action === "edit") {
                $scope.childPolicy = childPolicy;
                $scope.gSave = true;

            } else if (action === "save") {
                $scope.gSave = false;
                $scope.childPolicy = { discount: "Discount by (%)" };
            }
            $scope.Viewpax();
        }

        // Vehicle
        $scope.Vehicle = {};
        $scope.VehicleDetail = {};

        // list Vehicle
        $scope.lsVehicle = [];
        //list popup
        $scope.VehicleTemp = {};
        $scope.lsVehicleDetail = [];
        $scope.vSave = false;
        $scope.vSaveDetail = false;
        $scope.VehicleValue = function(action, Vehicle, $index) {
            if (action === "delete" && confirm("Are you sure to delete?")) {
                // $scope.TotalVehicle -= Vehicle.price; 
                $scope.lsVehicle.splice($scope.lsVehicle.indexOf(Vehicle), 1);
                if(!$scope.IsloadEdit) ViewContentExcursion($scope.lsVehicle);
            } else if (action === "add") {
                var temp = {
                    id: null,
                    supplier:$scope.Vehicle.supplier,
                    supplierTourCode: $scope.Vehicle.supplierTourCode,
                    day: $scope.Vehicle.day,
                    name: $scope.Vehicle.name,
                    types: $scope.Vehicle.types,
                    currency: $scope.Curency
                        // tax: $scope.Vehicle.tax
                }
                $scope.lsVehicle.push(temp);
                $scope.Vehicle = {};
            } else if (action === "edit") {
                $scope.Vehicle = Vehicle;
                $scope.vSave = true;
            } else if (action === "editDetail") {
                $scope.VehicleTemp.name = Vehicle.name;
                $scope.VehicleTemp.index = $index;
                if (Vehicle.listvalue)
                    $scope.lsVehicleDetail = Vehicle.listvalue;
                else
                    $scope.lsVehicleDetail = []
            } else {
                $scope.Vehicle = {};
                 if(!$scope.IsloadEdit) ViewContentExcursion($scope.lsVehicle);
                $scope.vSave = false;
            }
            $scope.Viewpax();
        }
        $scope.SaveVehicleDetail = function() {
            $scope.lsVehicle[$scope.VehicleTemp.index].listvalue = $scope.lsVehicleDetail;
            $scope.VehicleTemp = {};
            $scope.lsVehicleDetail = [];
            $('#responsive').modal('hide');
            $scope.Viewpax();
        }
        $scope.VehicleDetailValue = function(action, Vehicle, $index) {
                if (action === "delete" && confirm("Are you sure to delete?")) {
                    $scope.lsVehicleDetail.splice($index, 1);
                } else if (action === "add") {
                    var temp = {
                        name: $scope.VehicleDetail.name,
                        price: $scope.VehicleDetail.price,
                        min: $scope.VehicleDetail.min,
                        max: $scope.VehicleDetail.max,
                        priceservice: $scope.VehicleDetail.price
                    }
                    $scope.lsVehicleDetail.push(temp);
                    $scope.VehicleDetail = {};
                } else if (action === "edit") {
                    $scope.VehicleDetail = Vehicle;
                    $scope.vSaveDetail = true;
                } else {
                    $scope.VehicleDetail = {};
                    $scope.vSaveDetail = false;
                }
                $scope.Viewpax();
            }
            //Accumulated
        $scope.Accumulated = {};
        $scope.lsAccumulated = [];
        $scope.aSave = false;
        $scope.AccumulatedValue = function(action, Accumulated, $index) {
            if (action === "delete" && confirm("Are you sure to delete?")) {
                $scope.TotalAccumulated -= Accumulated.price;
                $scope.lsAccumulated.splice($scope.lsAccumulated.indexOf(Accumulated), 1);
            } else if (action === "add") {
                var temp = {
                    id: null,
                    supplier:$scope.Accumulated.supplier,
                    supplierTourCode: $scope.Accumulated.supplierTourCode,
                    day: $scope.Accumulated.day,
                    name: $scope.Accumulated.name,
                    types: $scope.Accumulated.types,
                     price: $scope.Accumulated.price,
                    min: $scope.Accumulated.min,
                    max: $scope.Accumulated.max,
                    priceservice: $scope.Accumulated.price,
                    currency: $scope.Curency
                }
                $scope.lsAccumulated.push(temp);
                $scope.Accumulated = {};
            } else if (action === "edit") {
                $scope.Accumulated = Accumulated;
                $scope.aSave = true;
            } else {
                $scope.Accumulated = {};
                $scope.aSave = false;
            }
            $scope.Viewpax();
        }
        $scope.Accommodation = {};
        $scope.lsAccommodation = [];
        $scope.acSave = false
        $scope.AccommodationValue = function(action, Accommodation, $index) {
            if (action === "delete" && confirm("Are you sure to delete?")) {
                $scope.TotalAccommodation -= Accommodation.price;
                $scope.lsAccommodation.splice($index, 1);
            } else if (action === "add") {
                var temp = {
                    day: $scope.Accommodation.day,
                    supplier:$scope.Accommodation.supplier,
                    supplierTourCode: $scope.Accommodation.supplierTourCode,
                    name: $scope.Accommodation.name,
                    occupancy: $scope.Accommodation.occupancy,
                    price: $scope.Accommodation.price,
                    priceservice: $scope.Accommodation.price,
                    currency: $scope.Curency
                }
                $scope.lsAccommodation.push(temp);
                $scope.Accommodation = {};
            } else if (action === "edit") {
                $scope.Accommodation = Accommodation;
                $scope.acSave = true;
            } else {
                $scope.Accommodation = {};
                $scope.acSave = false;
            }
            $scope.Viewpax();
        }

        function filltermax(list) {
            if(list)
            {
                if(list[0])
                {
                    var max = list[0].max;
                    for (var i = 0; i < list.length; i++)
                        if (list[i].max > max)
                            max = list[i].max;
                    return max;
                }
                
            }
            else return null;
        }

        $scope.showGroupCodeAction = function (txt){
            if(txt == false) $scope.info.Groupcode = undefined;
            $scope.info.showGroupCode = txt;
        }

        $scope.tiers = [
            { pax: 1, makeUp: 0, price: 0, profit: 0, ChildAplly:'Yes',priceChild:[] },
            { pax: 2, makeUp: 0, price: 0, profit: 0, ChildAplly:'Yes',priceChild:[] },
            { pax: 3, makeUp: 0, price: 0, profit: 0, ChildAplly:'Yes',priceChild:[] },
            { pax: 7, makeUp: 0, price: 0, profit: 0, ChildAplly:'Yes',priceChild:[] },
            { pax: 10, makeUp: 0, price: 0, profit: 0, ChildAplly:'Yes',priceChild:[] },
            { pax: 15, makeUp: 0, price: 0, profit: 0, ChildAplly:'Yes',priceChild:[] },
            { pax: 20, makeUp: 0, price: 0, profit: 0, ChildAplly:'Yes',priceChild:[] },
            { pax: 25, makeUp: 0, price: 0, profit: 0, ChildAplly:'Yes',priceChild:[] },
            { pax: 30, makeUp: 0, price: 0, profit: 0, ChildAplly:'Yes',priceChild:[] }
        ];
        function decimalAdjust(type, value, exp) {
            // If the exp is undefined or zero...
            if (typeof exp === 'undefined' || +exp === 0) {
            return Math[type](value);
            }
            value = +value;
            exp = +exp;
            // If the value is not a number or the exp is not an integer...
            if (isNaN(value) || !(typeof exp === 'number' && exp % 1 === 0)) {
            return NaN;
            }
            // Shift
            value = value.toString().split('e');
            value = Math[type](+(value[0] + 'e' + (value[1] ? (+value[1] - exp) : -exp)));
            // Shift back
            value = value.toString().split('e');
            return +(value[0] + 'e' + (value[1] ? (+value[1] + exp) : exp));
        }
        $scope.Viewpax = function() {
                $timeout(function(){
                    $scope.$apply();
                     for (var i = 0; i < $scope.tiers.length; i++) {
                        var PaxValue = {},
                            tem = 0;
                        var datatem = 0;

                        angular.forEach($scope.lsVehicle, function(value, key) {

                            angular.forEach(value.listvalue, function(vl, key) {
                                if ($scope.tiers[i].pax >= vl.min && $scope.tiers[i].pax <= vl.max) {
                                    tem += vl.price;

                                } else if ($scope.tiers[i].pax > filltermax(value.listvalue))
                                    datatem = Math.ceil($scope.tiers[i].pax / vl.max) * vl.price
                            });
                            if ($scope.tiers[i].pax > filltermax(value.listvalue)) {
                                tem += datatem
                            }
                        });

                        PaxValue.Vehicle = (tem / $scope.tiers[i].pax);
                         angular.forEach($scope.lsAccumulated, function(value, key) {
                            if ($scope.tiers[i].pax >= value.min && $scope.tiers[i].pax <= value.max) {
                                if (PaxValue.Accumulated) {
                                    PaxValue.Accumulated += (Math.ceil(value.price / $scope.tiers[i].pax));
                                } else {
                                    PaxValue.Accumulated = (Math.ceil(value.price / $scope.tiers[i].pax));
                                }
                            } else {
                                if (PaxValue.Accumulated) {
                                    PaxValue.Accumulated += (Math.ceil($scope.tiers[i].pax / value.max) * value.price) / $scope.tiers[i].pax;
                                } else {
                                    PaxValue.Accumulated = (Math.ceil($scope.tiers[i].pax / value.max) * value.price) / $scope.tiers[i].pax;
                                }
                            }

                        });

                        for (var index = $scope.lsAccommodation.length - 1; index >= 0; index--) {
                            if ($scope.tiers[i].pax >= $scope.lsAccommodation[index].occupancy) {
                                PaxValue.Accommodation = ($scope.lsAccommodation[index].price / $scope.lsAccommodation[index].occupancy);
                                break;
                            }
                        }

                        var TotalPerson = 0,
                            TotalGroups = 0;
                        angular.forEach($scope.lsGroup, function(value, key) {
                            if (value.groupPerson === "Per Group")
                                TotalGroups += value.price;
                            else TotalPerson += value.price;
                        });
                        // PaxValue.buyPrice = Math.round((($scope.TotalGroups || 0) / $scope.tiers[i].pax) + ($scope.TotalPerson || 0) + (PaxValue.Vehicle || 0) + (PaxValue.Accommodation || 0) + (PaxValue.Accumulated || 0));
                        PaxValue.buyPrice = ((TotalGroups || 0) / $scope.tiers[i].pax) + (TotalPerson || 0) + (PaxValue.Vehicle || 0) + (PaxValue.Accommodation || 0) + (PaxValue.Accumulated || 0);
                        
                        $scope.tiers[i].price = PaxValue.buyPrice + PaxValue.buyPrice * $scope.tiers[i].makeUp / 100;
                        $scope.tiers[i].price = ($scope.tiers[i].price * ($scope.LandingMK | 0))/100 + $scope.tiers[i].price;
                        $scope.tiers[i].profit = ($scope.tiers[i].price - PaxValue.buyPrice) * $scope.tiers[i].pax;
                        $scope.tiers[i].priceChild = [];
                        if ($scope.tiers[i].ChildAplly == 'Yes') {                    
                            angular.forEach($scope.lsChildPolicy, function(vl, key) {
                                if(vl.discount === 'Discount by (%)'){
                                var priceMarkup = $scope.tiers[i].price  - ($scope.tiers[i].price  * vl.value / 100)
                                var vlchange = {vl:priceMarkup,vlFix:false}
                                $scope.tiers[i].priceChild.push(vlchange);                            
                                } else if(vl.discount === 'Discount by (Amount)') {                             
                                    var priceMarkup = $scope.tiers[i].price - vl.value;
                                    priceMarkup = decimalAdjust('round', priceMarkup, 1);
                                    var vlchange = {vl:priceMarkup,vlFix:false}
                                    $scope.tiers[i].priceChild.push(vlchange);
                                                        
                                } else
                                {                              
                                    var vlchange = {vl:decimalAdjust('round', vl.value, 1),vlFix:true}   
                                    $scope.tiers[i].priceChild.push(vlchange);
                                }
                            });
                        }
                    }
                    $scope.totalSGlSUPPL= 0;
                    if($scope.isProposal == true)
                    {
                        angular.forEach($scope.lsDirectModel, function(vl, key) {                                   
                             $scope.totalSGlSUPPL += vl.SGLSuppl;                                      
                        })  
                        console.log($scope.lsDirectModel)
                        for (var i = 0; i < $scope.tiers.length; i++) {
                            if($scope.lsDirectModel)
                            {
                                if($scope.CheckDirectSetup.HotelMkALL)
                                {
                                    angular.forEach($scope.lsDirectModel, function(vl, key) {                                    
                                        vl.SGLSuppl = (((vl.priceSGLRoom * vl.nights ) - ((vl.priceShareRoom /vl.occupancy) * vl.nights))+ (((vl.priceSGLRoom * vl.nights ) - ((vl.priceShareRoom /vl.occupancy) * vl.nights)) * ($scope.CheckDirectSetup.HotelMkALL || 0))/100 )*vl.rooms;
                                            var temp = vl;
                                            temp.tiers = [];
                                            for (var i = 0; i < $scope.tiers.length; i++) {
                                                if ($scope.tiers[i].pax != null) {
                                                        var item = { pax:$scope.tiers[i].pax, price:0 };
                                                        if($scope.tiers[i].pax == 1)
                                                        {
                                                            var priceChange = vl.priceSGLRoom*vl.rooms*vl.nights;
                                                            var priceTemp = priceChange + (priceChange * ($scope.CheckDirectSetup.HotelMkALL||0))/100;  
                                                            item.price = priceTemp
                                                            
                                                        }
                                                        else {
                                                            var priceChange =  ((vl.priceShareRoom *vl.nights) / vl.occupancy)*vl.rooms
                                                            var priceTemp = priceChange + (priceChange * ($scope.CheckDirectSetup.HotelMkALL||0))/100;  
                                                            item.price = priceTemp;                                                       
                                                        }
                                                        vl.tiers.push(item);
                                                    }
                                            }
                                                                        
                                    });
                                }  
                                angular.forEach($scope.lsDirectModel, function(vl, key) {
                                if(vl.tiers){
                                    if(vl.tiers[i] && vl.tiers[i].price)                                
                                        $scope.tiers[i].price = $scope.tiers[i].price   + vl.tiers[i].price; 
                                    }   
                                }); 
                            }                            
                        }           
                    }
                    else if($scope.ispackage == true )
                    {
                        console.log(4);
                         angular.forEach($scope.lsDirectModel, function(vlDM, key) {
                                vlDM.tiers = [];
                                vlDM.SGLSuppl = (((vlDM.priceSGLRoom * vlDM.nights ) - ((vlDM.priceShareRoom /vlDM.occupancy) * vlDM.nights))+ (((vlDM.priceSGLRoom * vlDM.nights ) - ((vlDM.priceShareRoom /vlDM.occupancy) * vlDM.nights)) * ($scope.CheckDirectSetup.HotelMkALL || 0))/100 )*vlDM.rooms;
                                for (var i = 0; i < $scope.tiers.length; i++) {
                                    if ($scope.tiers[i].pax != null) {
                                        var item = { pax:$scope.tiers[i].pax, price:0 };
                                        if($scope.tiers[i].pax == 1)
                                        {   
                                            var priceTemp = ( ((vlDM.priceSGLRoom *vlDM.nights)+((vlDM.priceSGLRoom *vlDM.nights) * (vlDM.directMkModel ? vlDM.directMkModel : $scope.CheckDirectSetup.HotelMkALL) || 0)/100)  + $scope.tiers[i].price)*vlDM.rooms;
                                            item.price =  priceTemp;
                                        }
                                        else {
                                            var priceTemp = ( ((vlDM.priceShareRoom *vlDM.nights) / vlDM.occupancy)+(((vlDM.priceShareRoom *vlDM.nights) / vlDM.occupancy) * (vlDM.directMkModel ? vlDM.directMkModel : $scope.CheckDirectSetup.HotelMkALL) || 0)/100  + $scope.tiers[i].price)*vlDM.rooms;
                                            item.price = priceTemp;
                                        }
                                        vlDM.tiers.push(item);
                                    }
                                }     
                        });
                        angular.forEach($scope.lsDirectModel, function(vl, key) {                                   
                             $scope.totalSGlSUPPL += vl.SGLSuppl;                                      
                        })   
                    }                   
                },500);
        };
        $scope.ChangeValueFixChild = function(x,index,item)
        {
             $timeout(function(){
                var indexOfitem =  $scope.tiers.indexOf(x);                
                $scope.tiers[indexOfitem].priceChild.splice(index,1,item);
                $scope.$apply();
             },50);
        }
        //Extra
        $scope.Extra = {};
        $scope.lsExtra = [];
        $scope.exSave = false
        $scope.ExtraValue = function(action, Extra, $index) {
            if (action === "delete" && confirm("Are you sure to delete?")) {
                $scope.lsExtra.splice($index, 1);
            } else if (action === "add") {
                var temp = {
                    extra: $scope.Extra.extra,
                    charge: $scope.Extra.charge,
                    price: $scope.Extra.price,
                    note: $scope.Extra.note
                }
                $scope.lsExtra.push(temp);
                $scope.Extra = {};
            } else if (action === "edit") {
                $scope.Extra = Extra;
                $scope.exSave = true;
            } else {
                $scope.Extra = {};
                $scope.exSave = false;
            }
        }

        //DirectMarkup
        $scope.DirectMarkup = {};
        $scope.lsDirectmarkup = [];
        $scope.dmSave = false;

        $scope.DirectMarkup.byPrice =0;
        $scope.DirectMarkup.directMarkup =0;
        $scope.ChangeValueDirectMarkup = function(){
            var vldirectMarkup = $scope.DirectMarkup.directMarkup/100;
            $scope.DirectMarkup.price = $scope.DirectMarkup.byPrice + ($scope.DirectMarkup.byPrice*vldirectMarkup);
        }
       
        $scope.DirectMarkupValue = function(action, Extra, $index) {
            if (action === "delete") {
                if (confirm("WARNING!, this Direct Mark-Up is about deleted, please confirm the erase")) {
                    for(var i = $scope.lsDirectmarkup.length - 1; i >= 0; i--) {
                        if($scope.lsDirectmarkup[i]._id != undefined && $scope.lsDirectmarkup[i]._id === Extra._id &&  $scope.lsDirectmarkup[i].name ==  Extra.name 
                            &&  $scope.lsDirectmarkup[i].beginDate ==  Extra.beginDate 
                            &&  $scope.lsDirectmarkup[i].endDate ==  Extra.endDate 
                            &&  $scope.lsDirectmarkup[i].price ==  Extra.price 
                            &&  $scope.lsDirectmarkup[i].DateGroup ==  Extra.DateGroup 
                            ) {
                                $scope.lsDirectmarkup.splice(i, 1);
                              }
                    }
                }
                // $scope.lsDirectmarkup.splice($index, 1);
            } else if (action === "add") {
                if($scope.DirectMarkup.endDate !== undefined || $scope.DirectMarkup.beginDate !== undefined)
                {
                    var temp = {
                        name: $scope.DirectMarkup.name,
                        charge: $scope.DirectMarkup.charge,
                        byPrice: $scope.DirectMarkup.byPrice,
                        directMarkup: $scope.DirectMarkup.directMarkup,
                        price: $scope.DirectMarkup.price,
                        DateGroup: newDateConvertTotext($scope.DirectMarkup.beginDate) + " - " + newDateConvertTotext($scope.DirectMarkup.endDate),
                        note: $scope.DirectMarkup.note,
                        endDate: $scope.DirectMarkup.endDate,
                        beginDate: $scope.DirectMarkup.beginDate,
                        time: new Date().getTime()
                    }
                
                    $scope.lsDirectmarkup.push(temp);
                    $scope.DirectMarkup = {endDate: $scope.DirectMarkup.endDate,beginDate: $scope.DirectMarkup.beginDate,byPrice:0,directMarkup:0};
                }
                else
                {
                    alert("beginDate, endDate are required to fill");
                }
                
            } else if (action === "edit") {
                $('html, body').animate({ scrollTop: $("#idScrollDirect").offset().top - 100 }, 'fast');
                $scope.DirectMarkup =Extra;
                console.log(Extra.beginDate);
                $scope.DirectMarkup.beginDate = new Date(Extra.beginDate);
                $scope.DirectMarkup.endDate = new Date(Extra.endDate);
                $scope.dmSave = true;
            } else {
                Extra.DateGroup = newDateConvertTotext(Extra.beginDate) + " - " + newDateConvertTotext(Extra.endDate);
                $timeout(function(){
                    for(var i = $scope.lsDirectmarkup.length - 1; i >= 0; i--) {
                        if(($scope.lsDirectmarkup[i]._id != undefined && $scope.lsDirectmarkup[i]._id === Extra._id)  || $scope.lsDirectmarkup[i].time === Extra.time){
                            $scope.lsDirectmarkup.splice(i, 1 ,Extra);                        
                        }
                    }
                   $scope.$apply();
                    $scope.Viewpax();
                },400);
                $scope.DirectMarkup = {byPrice:0,directMarkup:0};
                $scope.dmSave = false;
            }
        }        
        // DirectModelValue
        
        $scope.DataSaveDirectModelTemp ={};
        $scope.DirectModel = {occupancy:2,byPrice:0,nights:0,priceShareRoom:0,priceSGLRoom:0};
        $scope.lsDirectModel = [];
        $scope.changevlDateCheckin = function()
        {
            if($scope.DirectModel.beginDate && $scope.DirectModel.endDate && $scope.isProposal)
            {
                var beginDate = new Date($scope.DirectModel.beginDate); 
                var endDate = new Date($scope.DirectModel.endDate);
                var msDiff = endDate - beginDate;
                $scope.DirectModel.nights = msDiff / 1000 / 60 / 60 / 24;
            }
            else if($scope.isProposal) $scope.DirectModel.nights = 0;
        }
        $scope.ChangeValueDirectModel = function(MK_item)
        {        
                 $timeout(function(){
                    
                        MK_item.SGLSuppl = (((MK_item.priceSGLRoom * MK_item.nights ) - ((MK_item.priceShareRoom /MK_item.occupancy) * MK_item.nights))+ (((MK_item.priceSGLRoom * MK_item.nights ) - ((MK_item.priceShareRoom /MK_item.occupancy) * MK_item.nights)) * (MK_item.directMkModel || 0))/100 )*MK_item.rooms;
                        MK_item.tiers = [];
                        for (var i = 0; i < $scope.tiers.length; i++) {
                        if ($scope.tiers[i].pax != null) {
                                var item = { pax:$scope.tiers[i].pax, price:0 };
                                if($scope.tiers[i].pax == 1)
                                {
                                    var priceTemp = $scope.isProposal == true ?  (MK_item.priceSGLRoom)*MK_item.nights*MK_item.rooms : (  (MK_item.priceSGLRoom*MK_item.nights + (((MK_item.directMkModel | 0) * MK_item.priceSGLRoom*MK_item.nights)/100))  + $scope.tiers[i].price )*MK_item.rooms;
                                    item.price = $scope.isProposal == true ? priceTemp + ((MK_item.directMkModel | 0) * priceTemp)/100 : priceTemp
                                }
                                else {
                                    var priceTemp = $scope.isProposal == true ?  ((MK_item.priceShareRoom *MK_item.nights) / MK_item.occupancy)*MK_item.rooms : ( (((MK_item.priceShareRoom *MK_item.nights) / MK_item.occupancy) + (((MK_item.priceShareRoom *MK_item.nights) / MK_item.occupancy) * (MK_item.directMkModel | 0)) /100)    + $scope.tiers[i].price )*MK_item.rooms;
                                    item.price =  $scope.isProposal == true ?  priceTemp + (priceTemp * (MK_item.directMkModel | 0)) /100 : priceTemp
                                }
                                MK_item.tiers.push(item);
                            }
                        } 
                        $timeout(function(){
                            for(var i = $scope.lsDirectModel.length - 1; i >= 0; i--) {
                                if(($scope.lsDirectModel[i]._id != undefined && $scope.lsDirectModel[i]._id === MK_item._id)  || $scope.lsDirectModel[i].time === MK_item.time){
                                        $scope.lsDirectModel.splice(i, 1,MK_item);
                                    }
                            }
                            $scope.$apply();
                            if($scope.isProposal)
                            {
                                console.log(1)
                                $scope.Viewpax();
                            }
                        },200);
                   

                },50)
                 
        }
        $scope.LoadDataHotel = function()
        {
            $http.get('http://da-tariff.com/modules.php?name=HotelView&op=hoteljson&token='+convertToken())
                .success(function(data) {                   
                    if(data)
                    {
                    $scope.ListHotelData = data;
                       $("#popupServiceHotel").modal('show');
                    }
                })
                .error(function(data) {
                    alert("Error!")
                });
        }
        $scope.ClearHotelModelFilter = function()
        {
            $scope.htnameFilter= "";
            $scope.tpnameFilter= "";
            $scope.htlocationFilter= "";
        }
        $scope.ChooseHotelModel = function (item)
        {
            $scope.DirectModel.name = item.htname + " | " + item.tpname;
            $("#popupServiceHotel").modal('hide');
        }
        $scope.calculatingTotalPrice = function()
        {
            $scope.DirectModel.price = ($scope.DirectModel.nights *$scope.DirectModel.byPrice)*$scope.DirectModel.rooms;
        }
        
        $scope.DirectModelValue = function(action, Extra, $index) {
            if (action === "delete") {
                if (confirm("WARNING!, this service is about deleted, please confirm the erase")) {

                     for(var i = $scope.lsDirectModel.length - 1; i >= 0; i--) {
                            if(($scope.lsDirectModel[i]._id != undefined && $scope.lsDirectModel[i]._id === Extra._id)  || $scope.lsDirectModel[i].time === Extra.time){
                                     $scope.lsDirectModel.splice(i, 1);
                                    $timeout(function(){
                                        $scope.$apply();
                                        $scope.Viewpax();                      
                                    },100)
                                }
                            }
                }
            } else if (action === "add") {
                if($scope.DirectModel.endDate !== undefined || $scope.DirectModel.beginDate !== undefined)
                {
                    var temp = {
                        day: $scope.DirectModel.day,
                        name: $scope.DirectModel.name,
                        charge: $scope.DirectModel.charge,
                        rooms: $scope.DirectModel.rooms,
                        directMkModel: $scope.DirectModel.directMkModel,                        
                        occupancy: $scope.DirectModel.occupancy,
                        roomcategory: $scope.DirectModel.roomcategory,
                        priceShareRoom: $scope.DirectModel.priceShareRoom,
                        priceSGLRoom: $scope.DirectModel.priceSGLRoom,
                        nights: $scope.DirectModel.nights,
                        DateGroup: newDateConvertTotext($scope.DirectModel.beginDate) + " - " + newDateConvertTotext($scope.DirectModel.endDate),
                        endDate: $scope.DirectModel.endDate,
                        beginDate: $scope.DirectModel.beginDate,
                        time: new Date().getTime(),
                        twinShare: (($scope.DirectModel.priceShareRoom /$scope.DirectModel.occupancy) * $scope.DirectModel.nights)*$scope.DirectModel.rooms,
                        SGLSuppl: (($scope.DirectModel.priceSGLRoom * $scope.DirectModel.nights ) - (($scope.DirectModel.priceShareRoom /$scope.DirectModel.occupancy) * $scope.DirectModel.nights))*$scope.DirectModel.rooms,
                        tiers:[]
                    }
                    for (var i = 0; i < $scope.tiers.length; i++) {
                        if ($scope.tiers[i].pax != null) {
                            var item = { pax:$scope.tiers[i].pax, price:0 };
                            if($scope.tiers[i].pax == 1)
                            {
                                item.price = $scope.DirectModel.nights*($scope.DirectModel.priceSGLRoom)*$scope.DirectModel.rooms 
                            }
                            else {
                                item.price = (($scope.DirectModel.priceShareRoom *$scope.DirectModel.nights) / $scope.DirectModel.occupancy)*$scope.DirectModel.rooms
                            }
                            temp.tiers.push(item);
                        }
                    }  
                   
                    $scope.lsDirectModel.push(temp);
                    $timeout(function(){
                        $scope.$apply();
                        $scope.Viewpax();                      
                    },100)
                    if($scope.CheckDirectSetup.HotelMkALL >0)
		               {
		                      $scope.HidenMarkupline = true;
                              
                                angular.forEach($scope.lsDirectModel, function(vlDM, key) {
                                        vlDM.SGLSuppl = (((vlDM.priceSGLRoom * vlDM.nights ) - ((vlDM.priceShareRoom /vlDM.occupancy) * vlDM.nights))+ (((vlDM.priceSGLRoom * vlDM.nights ) - ((vlDM.priceShareRoom /vlDM.occupancy) * vlDM.nights)) * ($scope.CheckDirectSetup.HotelMkALL || 0))/100 )*vlDM.rooms;
                                        var temp = vlDM;
                                        temp.tiers = [];
                                        for (var i = 0; i < $scope.tiers.length; i++) {
                                            if ($scope.tiers[i].pax != null) {
                                                var item = { pax:$scope.tiers[i].pax, price:0 };
                                                if($scope.tiers[i].pax == 1)
                                                {   
                                                    var priceTemp = $scope.isProposal == true ?  vlDM.nights*vlDM.priceSGLRoom*vlDM.rooms : (vlDM.priceSGLRoom + $scope.tiers[i].price)*vlDM.nights*vlDM.rooms;
                                                    item.price = priceTemp + ($scope.CheckDirectSetup.HotelMkALL * priceTemp)/100
                                                }
                                                else {
                                                    var priceTemp = $scope.isProposal == true ?  ((vlDM.priceShareRoom *vlDM.nights) / vlDM.occupancy)*vlDM.rooms : (((vlDM.priceShareRoom *vlDM.nights) / vlDM.occupancy) + $scope.tiers[i].price)*vlDM.rooms;
                                                    item.price =  priceTemp + (priceTemp *$scope.CheckDirectSetup.HotelMkALL)/100
                                                }
                                                vlDM.tiers.push(item);
                                            }
                                        }	                            
                                });
                             
                                 $timeout(function(){
                                    $scope.$apply();
                                    $scope.Viewpax();
                                },100)
		                        
		               }
                    $scope.DirectModel = {endDate: $scope.DirectModel.endDate,beginDate: $scope.DirectModel.beginDate,occupancy:2,byPrice:0,nights:0,priceShareRoom:0,priceSGLRoom:0};
                }
                else
                {
                    alert("beginDate, endDate are required to fill");
                }
                
            } else if (action === "edit") {
                $scope.DirectModel =Extra;
                $scope.DirectModel.beginDate = new Date(Extra.beginDate);
                 $scope.DirectModel.endDate = new Date(Extra.endDate);
                $scope.dmSave = true;
            } else { 
                    Extra.SGLSuppl = (((Extra.priceSGLRoom * Extra.nights ) - ((Extra.priceShareRoom /Extra.occupancy) * Extra.nights))+ (((Extra.priceSGLRoom * Extra.nights ) - ((Extra.priceShareRoom /Extra.occupancy) * Extra.nights)) * (Extra.directMkModel || 0))/100 )*Extra.rooms;
                    Extra.DateGroup = newDateConvertTotext(Extra.beginDate) + " - " + newDateConvertTotext(Extra.endDate);
                    Extra.tiers = [];
                    for (var i = 0; i < $scope.tiers.length; i++) {
                        if ($scope.tiers[i].pax != null) {
                            var item = { pax:$scope.tiers[i].pax, price:0 };
                            if($scope.tiers[i].pax == 1)
                            {
                                item.price = Extra.nights*Extra.priceSGLRoom*Extra.rooms
                            }
                            else {
                                item.price =((Extra.priceShareRoom *Extra.nights) / Extra.occupancy)*Extra.rooms
                            }
                            Extra.tiers.push(item);
                        }
                    }          
                    $timeout(function(){
                        for(var i = $scope.lsDirectModel.length - 1; i >= 0; i--) {
                            if(($scope.lsDirectModel[i]._id != undefined && $scope.lsDirectModel[i]._id === Extra._id)  || $scope.lsDirectModel[i].time === Extra.time){
                                    $scope.lsDirectModel.splice(i, 1,Extra);
                                }
                            }
                         $scope.$apply();
                        $scope.Viewpax();
                    },50);
                    if($scope.CheckDirectSetup.HotelMkALL >0)
		               {
		                    $scope.HidenMarkupline = true;
                                angular.forEach($scope.lsDirectModel, function(vlDM, key) {
                                        vlDM.SGLSuppl = (((vlDM.priceSGLRoom * vlDM.nights ) - ((vlDM.priceShareRoom /vlDM.occupancy) * vlDM.nights))+ (((vlDM.priceSGLRoom * vlDM.nights ) - ((vlDM.priceShareRoom /vlDM.occupancy) * vlDM.nights)) * ($scope.CheckDirectSetup.HotelMkALL || 0))/100 )*vlDM.rooms;
                                        var temp = vlDM;
                                        temp.tiers = [];
                                        for (var i = 0; i < $scope.tiers.length; i++) {
                                            if ($scope.tiers[i].pax != null) {
                                                var item = { pax:$scope.tiers[i].pax, price:0 };
                                                if($scope.tiers[i].pax == 1)
                                                {   
                                                    var priceTemp = $scope.isProposal == true ?  (vlDM.priceSGLRoom)*vlDM.rooms*vlDM.nights : (vlDM.priceSGLRoom + $scope.tiers[i].price)*vlDM.nights*vlDM.rooms;                                                   
                                                    item.price =priceTemp + ($scope.CheckDirectSetup.HotelMkALL * priceTemp)/100
                                                }
                                                else {
                                                    var priceTemp = $scope.isProposal == true ?   ((vlDM.priceShareRoom *vlDM.nights) / vlDM.occupancy)*vlDM.rooms :  (((vlDM.priceShareRoom *vlDM.nights) / vlDM.occupancy) + $scope.tiers[i].price)*vlDM.rooms;   
                                                    item.price = priceTemp + (priceTemp *$scope.CheckDirectSetup.HotelMkALL)/100
                                                }
                                                vlDM.tiers.push(item);
                                            }
                                    }     
                                });
                                $timeout(function(){
                                    $scope.$apply();
                                    if($scope.isProposal == true)
                                    {
                                        $scope.Viewpax();
                                    }
                                },100)
		               }

                $scope.DirectModel = {occupancy:2,byPrice:0,nights:0,priceShareRoom:0,priceSGLRoom:0};
                $scope.dmSave = false;
            }
        }

        //Timing
        $scope.Timing = {};
        $scope.lsTiming = [];
        $scope.tmSave = false;
        $scope.SetTime = function(duration) {
            var minutes = $scope.startTime.minute + duration;
            var hours = $scope.startTime.hour + parseInt(minutes / 60);
            minutes = minutes % 60;

            var vh = hours >= 10 ? hours : "0" + hours;
            var vms = minutes >= 10 ? minutes : "0" + minutes;

            return vh + ":" + vms;
        }
        $scope.TimingValue = function(action, Timing, $index) {
            if (action === "delete" && confirm("Are you sure to delete?")) {
                $scope.lsTiming.splice($index, 1);
                $scope.TotalDuration = filltermaxlsTiming($scope.lsTiming);
                $scope.timeView = timeConvert(filltermaxlsTiming($scope.lsTiming));
                $scope.ResetDuration();
            } else if (action === "add") {
                var temp = {
                    duration: $scope.lsTiming.length > 0 ? $scope.lsTiming[$scope.lsTiming.length - 1].lenghts + $scope.lsTiming[$scope.lsTiming.length - 1].duration : 0,
                    time: "",
                    lenghts: $scope.Timing.lenghts,
                    description: $scope.Timing.description
                }
                temp.time = $scope.SetTime(temp.duration);

                $scope.lsTiming.push(temp);
                $scope.Timing = {};
                $scope.TotalDuration = filltermaxlsTiming($scope.lsTiming);
                $scope.timeView = timeConvert(filltermaxlsTiming($scope.lsTiming));
            } else if (action === "edit") {
                $scope.Timing = Timing;
                $scope.tmSave = true;
                $scope.TotalDuration = filltermaxlsTiming($scope.lsTiming);
                $scope.timeView = timeConvert(filltermaxlsTiming($scope.lsTiming));
            } else {
                $scope.Timing = {};
                $scope.tmSave = false;
                $scope.ResetDuration();
                $scope.TotalDuration = filltermaxlsTiming($scope.lsTiming);
                $scope.timeView = timeConvert(filltermaxlsTiming($scope.lsTiming));
            }
        }
        $scope.ResetDuration = function() {
            if( $scope.lsTiming.length >0)
            {
                $scope.lsTiming[0].duration = 0;
                $scope.lsTiming[0].time = $scope.SetTime($scope.lsTiming[0].duration);
                for (var i = 1; i < $scope.lsTiming.length; i++) {
                    $scope.lsTiming[i].duration = $scope.lsTiming[i - 1].duration + $scope.lsTiming[i - 1].lenghts;
                    $scope.lsTiming[i].time = $scope.SetTime($scope.lsTiming[i].duration);
                }
                $scope.TotalDuration = filltermaxlsTiming($scope.lsTiming);
                $scope.timeView = timeConvert(filltermaxlsTiming($scope.lsTiming));
            }
            
        }

        function filltermaxlsTiming(list) {
            if (list.length > 0) {
                var max = 0;
                for (var i = list.length - 1; i >= 0; i--) {
                    max = list[i].duration + list[i].lenghts;
                    break;
                }
                return max;
            }
        }
        $scope.timeView = "0h00";
        $scope.TotalDuration = 0;

        function timeConvert(n) {
            if ($scope.lsTiming.length > 0) {
                var minutes = (n % 60) < 10 ? "0" + (n % 60) : (n % 60);
                var hours = (n - minutes) / 60;
                return hours + "h" + minutes;
            } else
                return "0h00";
        }
        CKEDITOR.replace('editor1', {
            "extraPlugins": 'imagebrowser,ckeditor-gwf-plugin,tab',
            "imageBrowser_listUrl": '/api/tourviewTls/LoadImageToContent',
            height: '400px',
        });
        CKEDITOR.replace('editor2', {
            toolbar: [
                { name: 'document', items: ['Source', '-', 'NewPage', 'Preview', '-', 'Templates'] }, // Defines toolbar group with name (used to create voice label) and items in 3 subgroups.
                ['Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo'], // Defines toolbar group without name.
                '/', // Line break - next group will be placed in new line.
                { name: 'basicstyles', items: ['Bold', 'Italic'] },
                { name: 'basicstyles', groups: ['basicstyles', 'cleanup'], items: ['Bold', 'Italic', 'Strike', '-', 'RemoveFormat'] },
                { name: 'paragraph', groups: ['list', 'indent', 'blocks', 'align', 'bidi'], items: ['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote'] },
                { name: 'styles', items: ['Styles', 'Format'] }
            ],
            height: '100px',
        });

        // Manager Images
        $scope.ShowPopupManagerImages = function() {
            $('#PopupManagerImages').modal('show');
            $http.get('/api/tourviewTls/loadimage')
                .success(function(data) {
                    $scope.ListImageDelete = data;
                })
                .error(function(data) {
                    alert("Error!")
                });
            $http.get('/api/tourviewTls/groupImage')
                .success(function(lsgroup) {
                    $scope.groupImageList = lsgroup;
                })
                .error(function(data) {
                    alert("Error!")
                });
        };
        $scope.addGroupImage = false;
        $scope.AddNewImage = true;
        $scope.showAddGroup = function() {
            $scope.addGroupImage = true;
        }
        $scope.showAddNewImage = function() {
            $scope.AddNewImage = false;
        }
        $scope.CancelAdd = function()
        {
            $scope.AddNewImage = true;
        }
        $scope.CreateGroupImageCancel = function() {
            $scope.addGroupImage = false;
        }
        $scope.CreateGroupImage = function(action, itemGroupImage, $index) {
                if (action === "delete") {
                    if(confirm("Are you sure to delete?")){
                        $http.post('/api/tourviewTls/deleteGroupImage', itemGroupImage)
                            .success(function(rs) {
                                for(var i=0; i<$scope.groupImageList.length; i++){
                                    if($scope.groupImageList[i]._id == itemGroupImage._id && itemGroupImage.name===$scope.groupImageList[i].name){
                                        $scope.groupImageList.splice(i, 1);
                                        break;
                                    }
                                }
                            })
                            .error(function(rs) {

                            });
                    }
                } else if (action === "add") {
                    var datasend = {
                        name: $scope.itemGroupImage.name
                    }
                    $http.post('/api/tourviewTls/groupImage', datasend)
                        .success(function(itemImage) {
                            $scope.groupImageList.push(itemImage);
                            $scope.itemGroupImage = {};
                        });

                } else if (action === "edit") {
                    $scope.itemGroupImage = itemGroupImage;
                    $scope.ImagemSave = true;
                } else {
                    $http.put('/api/tourviewTls/groupImage', $scope.itemGroupImage)
                        .success(function(rs) {
                            $scope.ImagemSave = false;
                            $scope.itemGroupImage = {};
                        })
                        .error(function(rs) {

                        });
                }

            }
        $scope.GetListImageByName = function(xGroup){
            var param ={
                group: xGroup
            }
            $http.post('/api/tourviewTls/GeTListBygroup', param)
                .success(function(rs) {
                        console.log(rs);
                        $scope.ListImageDelete = rs;
                })
                .error(function(data) {
                    alert("Error!")
                });

        }
        $scope.DeleteImage = function(vl, $index) {
            $http.post('/api/tourviewTls/deleteimage', vl)
                .success(function(rs) {
                    if (rs == true)
                        $scope.ListImageDelete.splice($index, 1);
                })
                .error(function(data) {
                    alert("Error!")
                });

        };
        $scope.ImgEdit = {};
        $scope.boolImgEdit = true;
        $scope.imgesEdit = false;
        $scope.ChooseUpdateImage = function(vl, $index) {
            $scope.boolImgEdit = false;
            $scope.ImgEdit = vl;
            $scope.imgesEdit = true;

        };
        $scope.UpdateImage = function() {
            $http.put('/api/tourviewTls/updateimagegroup', $scope.ImgEdit)
                .success(function(rs) {
                    $scope.imgesEdit = false;
                    $scope.boolImgEdit = true;
                    $scope.ImgEdit = {};
                    alert("Success")
                })
                .error(function(data) {
                    alert("Error!")
                });
        };
        $scope.CancelImage = function(vl, $index) {
            $scope.boolImgEdit = true;
            $scope.ImgEdit = {};
            $scope.imgesEdit = false;
        };
        $scope.ImageShow = null;
        $scope.AddImage = function() {
            if ($scope.ImageShow !== null) {
                var dataimage = {
                    Image: $scope.ImageShow.base64,
                    NameImage: $scope.ImageShow.filename,
                    group: $scope.Addgroup
                }
                $http.post('/api/tourviewTls/updateImage', dataimage)
                    .success(function(data) {
                        alert("Upload Image Success..!");
                        $scope.folder = "";
                        $http.get('/api/tourviewTls/loadimage')
                            .success(function(data) {
                                $scope.ListImageDelete = data;
                                alert("Success")
                            })
                            .error(function(data) {
                                alert("Error!")
                            });
                        $scope.ImageShow = null;
                    })
                    .error(function(data) {
                        alert("Error!")
                    });
            } else alert("Please select image!")
        }

        //Choose Image
        $scope.ListImageChoose = [];
        $scope.ShowPopupChooseImages = function() {
            $http.get('/api/tourviewTls/loadimage')
                .success(function(data) {
                    $scope.ListImageDelete = data;
                })
                .error(function(data) {
                    alert("Error!")
                });
            $http.get('/api/tourviewTls/groupImage')
                .success(function(lsgroup) {
                    $scope.groupImageList = lsgroup;
                })
                .error(function(data) {
                    alert("Error!")
                });
            $('#PopupChooseImages').modal('show');
        };
        $scope.selectImage = function(x, event) {
                for (var i = 0; i < $scope.ListImageChoose.length; i++) {
                    if (x._id == $scope.ListImageChoose[i]._id) {
                        return;
                    }
                }
                $scope.ListImageChoose.push(x);
                if ($scope.ListImageChoose.length > 3) {
                    $(".clsSelectImg." + $scope.ListImageChoose[0]._id).removeClass("clsSelectImg " + $scope.ListImageChoose[0]._id);
                    $scope.ListImageChoose.splice(0, 1);
                }
                $(event.currentTarget).addClass("clsSelectImg " + x._id);
            }
            // Delete Image
        $scope.DeleteImageChoose = function(vl, $index) {
            $scope.ListImageChoose.splice($index, 1);
        };
        $scope.ImageChooseShow = null;
        //Scroll header
        $scope.dir = 1;
        $scope.MIN_TOP = 200;
        $scope.MAX_TOP = 350;
        $scope.sticky_relocate = function() {
            var window_top = $(window).scrollTop();
            var div_top = $("#sticky-anchor").offset().top;
            if (window_top > div_top) {
                $('#sticky').addClass('stick');
                $('#sticky-anchor').height($('#sticky').outerHeight());
            } else {
                $('#sticky').removeClass('stick');
                $('#sticky-anchor').height(0);
            }
        }
        $(window).scroll(function() { $scope.sticky_relocate(); });
        $scope.sticky_relocate();

        // ComponentsDateTimePickers.init();
        $scope.datepickerOptions = {
            format: 'yyyy-mm-dd',
            language: 'fr',
            startDate: "2012-10-01",
            endDate: "2012-10-31",
            autoclose: true,
            weekStart: 0
        }
        function addValForItem ()
        {
            if(!$scope.itemDataExport)
            {
                $scope.itemDataExport = {
                            tiers: $scope.tiers,
                            code: $scope.info.code,
                            showGroupCode:   $scope.info.showGroupCode,
                            Groupcode : $scope.info.Groupcode,
                            noteTiers: $scope.info.noteTiers,
                            isActive: $scope.info.active,
                            excursionName: $scope.info.excursionName,
                            category: $scope.info.categoryName,
                            location: $scope.info.location,
                            priority: $scope.info.priority,
                            updateTime: 0,
                            createTime: (new Date()).getTime(),
                            updatePer: "",
                            createPer: store.get("User").username,
                            DirectMarkup:  $scope.lsDirectmarkup,
                            market: $scope.listMarket,
                            currency: $scope.Curency,
                            beginDate: $scope.beginDate,
                            endDate: $scope.endDate,
                            lsGroups: $scope.lsGroup,
                            lsVehicle: $scope.lsVehicle,

                            listMarketExcept: $scope.listMarketExcept,
                            listAgentExcept: $scope.listAgentExcept,

                            lsAccumulated: $scope.lsAccumulated,
                            lsAccommodation: $scope.lsAccommodation,
                            lsExtra: $scope.lsExtra,
                            lsTiming: $scope.lsTiming,
                            maxTiming: $scope.TotalDuration,
                            content: CKEDITOR.instances.editor1.getData(),
                            note: CKEDITOR.instances.editor2.getData(),
                            slideImages: $scope.ListImageChoose,
                            startTime: $scope.startTime,
                            availab: $scope.availab,
                            lsDirectModel: $scope.lsDirectModel,
                            CRM:$scope.CheckDirectSetup.CRM,
                            DirectSetup: $scope.CheckDirectSetup.DirectSetup,
                            checkDirectModel: $scope.checkDirectModel,
                            noteDirectModel: $scope.noteDirectModel,
                            ValueEditModel: $scope.ValueEditModel,
                            ObjectAgent: $scope.info.Proposal,
                            agname: $scope.info.Proposal == undefined? "" : $scope.info.Proposal.agname == undefined? "" : $scope.info.Proposal.agname,
                            md5code: $scope.info.Proposal == undefined? "" : $scope.info.Proposal.md5code == undefined? "" : $scope.info.Proposal.md5code,
                            
                            Proposalstatus: $scope.Proposalstatus,
                            LandingMK: $scope.LandingMK,
                            HotelMkALL: $scope.CheckDirectSetup.HotelMkALL
                        }
            }            
        }

        $scope.PrinterCheck = function() {
            $scope.isLoading = true;
            addValForItem();
             
            if($scope.info.Proposal.md5code)
            {
               var vlCode =$scope.info.Proposal.md5code+"0";
                $scope.DetailGet(vlCode);
                $timeout(function () {
                        $scope.isLoading = false;
                        var printContents = document.getElementById('content').innerHTML;
                        var myWindow = window.open('', '_blank');
                        myWindow.document.write(printContents);
                        myWindow.document.close();
                        myWindow.focus();
                        myWindow.print();
                        myWindow.close();
                }, 2000);
            }
            else{
                $scope.isLoading = false;
				alert("Select An Agent")
			}
        };
        $scope.Exportdata = function() {

            $scope.isLoading = true;
            var name = $scope.info.excursionName;
            addValForItem();
            $("#content p" ).each(function( index ) { 
               var a =  $(this).find("img");
               $(a).insertBefore(this); 
            });
            
            if($scope.info.Proposal.md5code)
            {
               var vlCode =$scope.info.Proposal.md5code+"0";
                if(navigator.userAgent.indexOf('Mac')>1){
                    if(isSafari())
                    {
                        alert("This function is worked properly on Browses of Chrome,\n Firefox and Edge. Please try it again on those platforms.\n \t\t\t Thank you for your patience. \n\n \t\t\t ▬▬▬▬▬▬▬▬▬ஜ۩۞۩ஜ▬▬▬▬▬▬▬▬▬\n\n")
                    }
                    else
                    {
                        $scope.DetailGet(vlCode);
                        $timeout(function () {
                              $("#content").wordExport(name);
                              $scope.isLoading = false;
                        }, 2000);
                    }
                }
                else{
                    $scope.DetailGet(vlCode);
                        $timeout(function () {
                              $("#content").wordExport(name);
                              $scope.isLoading = false;
                        }, 2000);
                }
            }
            
			else{
                $scope.isLoading = false;
				alert("Select An Agent")
			}
        };
        $scope.showExcept = function(){
            $('#popupshowExcept').modal('show');
        }
        $scope.DetailGet = function(codeID) {
            $scope.item =  $scope.itemDataExport;
            $http.get('/api/tourviewTls/webinfo')
                .success(function(rs) {
                    if (rs) {
                        $scope.WebInfo = rs;
                        $scope.term = $sce.trustAsHtml(rs.term);
                        $scope.WebInfo.coverpage = $sce.trustAsHtml($scope.WebInfo.coverpage);
                    }
                }).error(function(rs) {});
            $scope.markup = 0;
            $http.get('http://da-tariff.com/modules.php?name=HotelView&op=crmjson&ccode='+codeID)
                .success(function(rs) {
                    $scope.markup = rs;
                    if ($scope.markup == 'false') {
                       alert("No Find Code in CRM.")
                    }
                }).error(function(rs) {});

                    if ($scope.item.availab == undefined)
                        $scope.item.availab = { sun: true, mon: true, tue: true, wed: true, thu: true, fri: true, sat: true };
                    $http.get('/api/tourviewTls/exchangerate')
                        .success(function(lsExchangeRate) {
                            var ctourpro = $scope.markup.cpcode=="1"? 0 : parseInt($scope.markup.ctourpro);
                            $scope.lsExchangeRate = lsExchangeRate;
                            $scope.content = $sce.trustAsHtml($scope.item.content)
                            if(!angular.isUndefined($scope.item.note)){
                            	if($scope.item.note !== "")
                                	$scope.ShownoteContent = true;
                            }
                            $scope.noteContent =$sce.trustAsHtml($scope.item.note)
                            var h = $scope.item.startTime.hour;
                            var m = $scope.item.startTime.minute;

                            var VND = 1,
                                USD = 1;
                            var middle = new Date($scope.item.beginDate).getTime();
                            for (var i = 0; i < $scope.lsExchangeRate.length; i++) {
                                var begin = new Date($scope.lsExchangeRate[i].beginDate).getTime();
                                var end = new Date($scope.lsExchangeRate[i].endDate).getTime();
                                if (begin <= middle && middle <= end) {
                                    VND = $scope.lsExchangeRate[i].VND;
                                    USD = $scope.lsExchangeRate[i].USD;
                                    break;
                                }
                            }
                            // if($scope.item.tiers[0].price == null)
                            //     $scope.hidenTiers=  true;


                            $scope.totalSGlSUPPLExport = 0;
                            $scope.allHotel ="";
                            if($scope.item.lsDirectModel.length >0)
                            {
                                $scope.item.lsDirectModel.forEach(function(value,key) {
                                    var currency = $scope.item.currency;
                                    var parentCurrency = $scope.markup.ccurrency
                                    var vlSGLSuppl = changeVl(currency, parentCurrency,  value.SGLSuppl);
                                    $scope.totalSGlSUPPLExport += $scope.item.CRM == true ? vlSGLSuppl + vlSGLSuppl * parseInt(ctourpro || 0) / 100 : vlSGLSuppl;
                                    if(value.name !== "undefined")
                                        $scope.allHotel +=    $sce.trustAsHtml( "<br> &emsp;&emsp;&emsp;&emsp; - " + value.name);
                                    value.tiers.forEach(function(ti) {
                                        var priceMarkup = $scope.item.CRM == true ? ti.price + ti.price * parseInt(ctourpro || 0) / 100 : ti.price;
                                        ti.price = changeVl(currency, parentCurrency, priceMarkup );
                                    });
                                });
                            }
                            function changeVl(currency, parentCurrency, priceMarkup )
                            {
                                var vl = 0;
                                if (currency == parentCurrency)
                                    vl = priceMarkup;
                                else if (currency == "USD" && parentCurrency == "THB") {
                                    vl = priceMarkup * VND / USD;
                                } else if (currency == "THB" && parentCurrency == "USD") {
                                    vl= priceMarkup * USD / VND;
                                }
                                return vl;
                            }
                            
                            $scope.lengthTiers = 1;
                            $scope.item.tiers.forEach(function(value) {
                                if(value.pax > 0) $scope.lengthTiers++;
                            });
                            
                            if($scope.item.Groupcode)
                            {
                                $http.post('/api/tourviewTls/GettiersByGroupCode', {Groupcode: $scope.item.Groupcode })
                                .success(function(Grs){
                                        var lsGrs = [];
                                    angular.forEach(Grs, function(vl, key) {
                                        var currency = vl.currency;
                                        angular.forEach(vl.tiers, function(value, key) {
                                                var priceMarkup =0;
                                                if($scope.item.ValueEditModel == "package" && $scope.item.lsDirectModel.length == 0 )
                                                {
                                                    priceMarkup = $scope.item.CRM == true ? value.price + value.price * parseInt(ctourpro || 0) / 100: value.price;                                    
                                                }
                                                else{
                                                    priceMarkup = value.price + value.price * parseInt(ctourpro || 0) / 100;                                   
                                                }
                                                
                                                var parentCurrency = $scope.markup.ccurrency
                                                if (currency == parentCurrency)
                                                    value.price = priceMarkup;
                                                else if (currency == "USD" && parentCurrency == "THB") {
                                                    value.price = priceMarkup * VND / USD;
                                                } else if (currency == "THB" && parentCurrency == "USD") {
                                                    value.price = priceMarkup * USD / VND;
                                                }
                                                if(value.ChildAplly == 'Yes') {
                                                    for(var i = 0; i < value.priceChild.length; i++){
                                                        var vl = value.priceChild[i];
                                                        var priceMarkup = vl + (vl * parseInt(ctourpro || 0) / 100);                                                        
                                                        var parentCurrency = $scope.markup.ccurrency
                                                        if (currency == parentCurrency)
                                                            vl = priceMarkup;
                                                        else if (currency == "USD" && parentCurrency == "THB") {
                                                            vl = priceMarkup * VND / USD;
                                                        } else if (currency == "THB" && parentCurrency == "USD") {
                                                            vl = priceMarkup * USD / VND;
                                                        }
                                                        value.priceChild[i] = vl;
                                                    }                                      
                                                }
                                        });
                                        lsGrs.push(vl);
                                    });
                                    $scope.item.ListGroupcode = lsGrs;
                                })
                            }

                            angular.forEach($scope.item.tiers, function(value, key) {
                                var priceMarkup  =0;
                                if($scope.item.ValueEditModel == "package" && $scope.item.lsDirectModel.length == 0 )
                                {
                                     priceMarkup = $scope.item.CRM == true ? value.price + value.price * parseInt(ctourpro || 0) / 100: value.price;                                    
                                }
                                else{
                                     priceMarkup = value.price + value.price * parseInt(ctourpro || 0) / 100;                                   
                                }
                                var currency = $scope.item.currency;
                                var parentCurrency = $scope.markup.ccurrency
                                if (currency == parentCurrency)
                                    value.price = priceMarkup;
                                else if (currency == "USD" && parentCurrency == "THB") {
                                    value.price = priceMarkup * VND / USD;
                                } else if (currency == "THB" && parentCurrency == "USD") {
                                    value.price = priceMarkup * USD / VND;
                                }

                                if (value.ChildAplly == 'Yes') {
                                        for(var i = 0; i < value.priceChild.length; i++){
                                            var vl = value.priceChild[i];
                                            var priceMarkupChild = vl + (vl * parseInt(ctourpro || 0) / 100);
                                            var currency = $scope.item.currency;
                                            var parentCurrency = $scope.markup.ccurrency
                                            if (currency == parentCurrency)
                                                vl = priceMarkupChild;
                                            else if (currency == "USD" && parentCurrency == "THB") {
                                                vl = priceMarkupChild * VND / USD;
                                            } else if (currency == "THB" && parentCurrency == "USD") {
                                                vl = priceMarkupChild * USD / VND;
                                            }
                                            value.priceChild[i] = vl;
                                        }                                      
                                    }
                            });
                            angular.forEach($scope.item.DirectMarkup, function(value, key) {
                                var priceMarkup = $scope.markup.cpcode=="1"? value.byPrice: value.price;
                                var currency = $scope.item.currency;
                                var parentCurrency = $scope.markup.ccurrency
                                if (currency == parentCurrency)
                                    value.price = priceMarkup;
                                else if (currency == "USD" && parentCurrency == "THB") {
                                    value.price = priceMarkup * VND / USD;
                                } else if (currency == "THB" && parentCurrency == "USD") {
                                    value.price = priceMarkup * USD / VND;
                                }

                            });
                            angular.forEach($scope.item.lsExtra, function(value, key) {
                                var priceMarkup = value.price + value.price * parseInt(ctourpro || 0) / 100;
                                var currency = $scope.item.currency;
                                var parentCurrency = $scope.markup.ccurrency
                                if (currency == parentCurrency)
                                    value.price = priceMarkup;
                                else if (currency == "USD" && parentCurrency == "THB") {
                                    value.price = priceMarkup * VND / USD;
                                } else if (currency == "THB" && parentCurrency == "USD") {
                                    value.price = priceMarkup * USD / VND;
                                }

                            });
                            $("#timevalue").val(($scope.item.startTime.hour < 10 ? "0" + $scope.item.startTime.hour : $scope.item.startTime.hour) + ":" + ($scope.item.startTime.minute < 10 ? "0" + $scope.item.startTime.minute : $scope.item.startTime.minute))
                            $scope.maxTiming = $scope.item.maxTiming;
                            $scope.timeView = timeConvert($scope.item.maxTiming);
                            getTime($scope.item.lsTiming, h, m)
                                // $scope.item.lsTiming = defaulttime($scope.item.lsTiming);
                            $('.clockpicker').clockpicker({
                                placement: 'right',
                                align: 'right',
                                donetext: 'Done',
                                afterDone: function() {
                                    var time = $("#timevalue").val();
                                    var ath = parseInt(time.substr(0, 2));
                                    var atm = parseInt(time.substr(3, 2));
                                    getTime($scope.item.lsTiming, ath, atm)
                                }

                            });
                            // $scope.info.categoryName = rs.length > 0 ? rs[0].name : "";
                            function timeConvert(n) {
                                var minutes = (n % 60) < 10 ? "0" + (n % 60) : (n % 60);
                                var hours = (n - minutes) / 60;
                                return hours + "h" + minutes;
                            }

                            function getTime(data, h, m) {
                                var datatime = [];
                                angular.forEach(data, function(val, key) {
                                    var minutes = m + val.duration;
                                    var hours = h + parseInt(minutes / 60);
                                    minutes = minutes % 60;

                                    var vh = hours >= 10 ? hours : "0" + hours;
                                    var vms = minutes >= 10 ? minutes : "0" + minutes;
                                    val.TimeVl = vh + ":" + vms;

                                });
                                $scope.timeVal = data;
                            }
                            $scope.items = $scope.item;
                            console.log($scope.items);
                        }).error(function() {});
        } 
  }
}());

(function() {
    'use strict';

    // Tourview tls controller
    angular
        .module('tourview-tls')
        .config(function($mdDateLocaleProvider) {
            $mdDateLocaleProvider.formatDate = function(date) {
                return date ? moment(date).format('DD-MMM-YYYY') : '';
            };
        })
        .directive('ngEnter', function() {
            return function(scope, element, attrs) {
                element.bind("keydown keypress", function(event) {
                    if (event.which === 13) {
                        scope.$apply(function() {
                            scope.$eval(attrs.ngEnter);
                        });

                        event.preventDefault();
                    }
                });
            };
        })
        .filter('unique', function() {
            return function(input, key) {
                var unique = {};
                var uniqueList = [];
                for (var i = 0; i < input.length; i++) {
                    if (typeof unique[input[i][key]] == "undefined") {
                        unique[input[i][key]] = "";
                        uniqueList.push(input[i]);
                    }
                }
                return uniqueList;
            };
        })
        .filter('filterMultipleService', ['$filter', function($filter) {
            return function(items, keyObj) {
                var filterObj = {
                    data: items,
                    filteredData: [],
                    applyFilter: function(obj, key) {
                        var fData = [];
                        if (this.filteredData.length == 0)
                            this.filteredData = this.data;
                        if (obj) {
                            if (key == "dateFilter") {
                                if (obj !== "") {
                                    for (var i = 0; i < this.filteredData.length; i++) {
                                        var begin = new Date(this.filteredData[i].beginDate).getTime();
                                        var end = new Date(this.filteredData[i].endDate).getTime();
                                        var middle = new Date(obj).getTime();
                                        if (begin <= middle && middle <= end) {
                                            fData.push(this.filteredData[i]);
                                        }
                                    }
                                    this.filteredData = fData;
                                    this.data = fData;
                                } else {
                                    this.filteredData = this.data;
                                }
                            } else {
                                var fObj = {};
                                if (!angular.isArray(obj)) {
                                    fObj[key] = obj;
                                    fData = fData.concat($filter('filter')(this.filteredData, fObj));
                                } else if (angular.isArray(obj)) {
                                    if (obj.length > 0) {
                                        for (var i = 0; i < obj.length; i++) {
                                            if (angular.isDefined(obj[i])) {
                                                fObj[key] = obj[i];
                                                fData = fData.concat($filter('filter')(this.filteredData, fObj));
                                            }
                                        }

                                    }
                                }
                                if (fData.length > 0) {
                                    this.filteredData = fData;
                                } else if (fData.length == 0 && obj.length != 0) {
                                    this.filteredData = [];
                                }
                            }


                        }
                    }
                };
                if (keyObj) {
                    angular.forEach(keyObj, function(obj, key) {
                        if (obj != undefined)
                            filterObj.applyFilter(obj, key);
                    });
                }
                return filterObj.filteredData;
            }
        }])
        .filter('filterMultiple', ['$filter', function($filter) {
            return function(items, keyObj) {
                var filterObj = {
                    data: items,
                    filteredData: [],
                    applyFilter: function(obj, key) {
                        var fData = [];
                        if (this.filteredData.length == 0)
                            this.filteredData = this.data;
                        if (obj) {
                            if (key == "dateFilter") {
                                for (var i = 0; i < this.filteredData.length; i++) {
                                    var begin = new Date(this.filteredData[i].beginDate).getTime();
                                    var end = new Date(this.filteredData[i].endDate).getTime();
                                    var middle = obj.getTime();
                                    if (begin <= middle && middle <= end) {
                                        fData.push(this.filteredData[i]);
                                    }
                                }
                                this.filteredData = fData;
                                this.data = fData;
                            } else {
                                var fObj = {};
                                if (!angular.isArray(obj)) {
                                    fObj[key] = obj;
                                    fData = fData.concat($filter('filter')(this.filteredData, fObj));
                                } else if (angular.isArray(obj)) {
                                    if (obj.length > 0) {
                                        for (var i = 0; i < obj.length; i++) {
                                            if (angular.isDefined(obj[i])) {
                                                fObj[key] = obj[i];
                                                fData = fData.concat($filter('filter')(this.filteredData, fObj));
                                            }
                                        }

                                    }
                                }
                                if (fData.length > 0) {
                                    this.filteredData = fData;
                                } else if (fData.length == 0 && obj.length != 0) {
                                    this.filteredData = [];
                                }

                            }


                        }
                    }
                };
                if (keyObj) {
                    angular.forEach(keyObj, function(obj, key) {
                        if (obj != undefined)
                            filterObj.applyFilter(obj, key);
                    });
                }
                return filterObj.filteredData;
            }
        }])
        .controller('TourviewTlsController', TourviewTlsController);

    TourviewTlsController.$inject = ['$scope', '$state', '$window', '$location', '$http', '$timeout', 'md5', 'currencyFormatService', '$sce', 'store', '$stateParams'];

    function TourviewTlsController($scope, $state, $window, $location, $http, $timeout, md5, currencyFormatService, $sce, store, $stateParams) {
        var vm = this;
        var datePicker = new Date();
        $scope.datepickerConfig = {
            allowFuture: true,
            dateFormat: 'DD/MMM/YYYY'
        };
        $scope.subtractDay = function(Time) {
            var Datetime = new Date(Time)
            Datetime.setDate(Datetime.getDate() - 1);
            return Datetime;
        }
        var objDate = new Date();
        $scope.FilterDateDay = objDate.getDate() + "/" + objDate.toLocaleString("en-us", { month: "short" }) + "/" + objDate.getFullYear();
        
        $scope.priorityFIlter = [];
        $scope.search = "";
        $scope.clearFilter = function() {
            $scope.categoryName = "";
            $scope.location = "";
            $scope.search = "";
            $scope.ChangeFiter = "";
            $scope.ListpriorityFIlter = [];
            $scope.ListlocationFIlter = [];
            $scope.ListcategoryFIlter = [];
            $scope.dateFilter = $scope.vlDefault;
            $scope.currentPage = 1;
            clientLoading();
        }

        $scope.ListpriorityFIlter = [];
        $scope.vlChangeFiter = function() {
            if ($scope.ListpriorityFIlter.length == 0 && $scope.ChangeFiter !== "") {
                $scope.ListpriorityFIlter.push($scope.ChangeFiter);
            } else {
                var checkList = 0;
                for (var i = $scope.ListpriorityFIlter.length - 1; i >= 0; i--) {
                    if ($scope.ListpriorityFIlter[i] === $scope.ChangeFiter) {
                        checkList = 1
                    }

                }
                if ($scope.ChangeFiter == "") {
                    $scope.ListpriorityFIlter = [];
                }
                if (checkList == 0 && $scope.ChangeFiter !== "") $scope.ListpriorityFIlter.push($scope.ChangeFiter);
            }

        }
        $scope.deletevlChangeFiter = function($index) {
            $scope.ListpriorityFIlter.splice($index, 1);
            if ($scope.ListpriorityFIlter.length == 0)
                $scope.ListpriorityFIlter = [];
        }

        $scope.ListcategoryFIlter = [];
        $scope.vlcategoryChangeFiter = function() {
            if ($scope.ListcategoryFIlter.length == 0 && $scope.categoryName !== "") {
                $scope.ListcategoryFIlter.push($scope.categoryName);
            } else {
                var checkList = 0;
                for (var i = $scope.ListcategoryFIlter.length - 1; i >= 0; i--) {
                    if ($scope.ListcategoryFIlter[i] === $scope.categoryName) {
                        checkList = 1
                    }

                }
                if ($scope.categoryName == "") {
                    $scope.ListcategoryFIlter = [];
                }
                if (checkList == 0 && $scope.categoryName !== "") $scope.ListcategoryFIlter.push($scope.categoryName);
            }

        }
        $scope.deletevlcategoryChangeFiter = function($index) {
            $scope.ListcategoryFIlter.splice($index, 1);
            if ($scope.ListcategoryFIlter.length == 0)
                $scope.ListcategoryFIlter = [];
        }

        $scope.ListlocationFIlter = [];
        $scope.vllocationChangeFiter = function() {
            if ($scope.ListlocationFIlter.length == 0 && $scope.location !== "") {
                $scope.ListlocationFIlter.push($scope.location);
            } else {
                var checkList = 0;
                for (var i = $scope.ListlocationFIlter.length - 1; i >= 0; i--) {
                    if ($scope.ListlocationFIlter[i] === $scope.location) {
                        checkList = 1
                    }

                }
                if ($scope.location == "") {
                    $scope.ListlocationFIlter = [];
                }
                if (checkList == 0 && $scope.location !== "") $scope.ListlocationFIlter.push($scope.location);
            }

        }
        $scope.deletevllocationChangeFiter = function($index) {
            $scope.ListlocationFIlter.splice($index, 1);
            if ($scope.ListlocationFIlter.length == 0)
                $scope.ListlocationFIlter = [];
        }


        $scope.ReviseData = function(item, name) {
            var data = {}
            if (name == "group")
                data = { id: item._id, condition: { 'lsGroups.id': item._id }, name: "group", value: item.group };
            else if (name == "priceband")
                data = { id: item._id, condition: { 'lsVehicle.id': item._id }, name: "priceband", value: item.priceband };
            else if (name == "accumulated")
                data = { id: item._id, condition: { 'lsAccumulated.id': item._id }, name: "accumulated", value: item.accumulated };
            console.log(data);
            $http.post('/api/tourviewTls/loadrevisedata', data)
                .success(function(rs) {
                    if (rs == true)
                        alert("Success!");
                })
                .error(function(rs) {

                });

        }
        $scope.RevisePriceband = function(item) {

        }
        $scope.ReviseAccumulated = function(item) {

        }

        $scope.serviceNameFilter = "";
        $scope.locationFilter = "";
        $scope.typesFilter = "";
        $scope.FilterService = function(service) {
            if (($scope.locationFilter == undefined || $scope.locationFilter == "" || service.location == $scope.locationFilter) &&
                ($scope.typesFilter == undefined || $scope.typesFilter == "" || service.types == $scope.typesFilter) &&
                ($scope.supplierFilter == undefined || $scope.supplierFilter == "" || service.supplier == $scope.supplierFilter))
                return true;
            return false;
        }
        $scope.ClearServiceFilter = function() {
            console.log($scope.FilterDateDay);
            $scope.serviceNameFilter = "";
            $scope.locationFilter = "";
            $scope.typesFilter = "";
            $scope.supplierFilter = "";
            $scope.FilterDateDay = "";
        }
        $scope.ChooseSupplier = function(value) {
            $scope.sevgroup.supplier = value;
        }
        $scope.Expand = function(index) {
            $("#IdExpand" + index).slideToggle();
            var ele = $($("#IdExpand" + index + " ul li:not(.ng-hide) a")[0]);
            ele.addClass("active");
            $("#IdExpand" + index + " div#" + ele.attr("value") + index).css("display", "block");
        }
        $scope.action = 'add';

        //service group
        $scope.priceband = {};
        $scope.spbdSave = false;
        $scope.sevgroup = { group: { currency: "THB" }, priceband: { currency: "THB", list: [] }, accumulated: { currency: "THB" }, beginDate: new Date(), endDate: new Date() };
        $scope.listSevGroup = [];
        $scope.InitSevGroup = function() {
            $scope.InitGeoTree();
            $scope.InitServiceType();
            $scope.InitSupplier();
            $http.get('/api/tourviewTls/LoadSevGroup')
                .success(function(rs) {
                    $scope.listSevGroup = rs;
                })
                .error(function(rs) {

                });
        }
        $scope.ConformDeletService = function(item, index) {
            $scope.itemService = item;
            $scope.indexService = index;
            $("#confirmDeleteService").modal('show');
        }
        $scope.SubmitSevPriceBandDetail = function(action, priceband, $index) {
            if (action === "delete") {
                $scope.sevgroup.priceband.list.splice($index, 1);

            } else if (action === "add") {
                var temp = {
                    name: $scope.priceband.name,
                    price: $scope.priceband.price,
                    min: $scope.priceband.min,
                    max: $scope.priceband.max
                }
                $scope.sevgroup.priceband.list.push(temp);
                $scope.priceband = {};
            } else if (action === "edit") {
                $('html, body').animate({ scrollTop: 0 }, 'fast');
                $scope.priceband = priceband;
                $scope.spbdSave = true;
            } else {
                $scope.priceband = {};
                $scope.spbdSave = false;
            }
        }
        $scope.SubmitSevGroup = function(action, sevgroup, $index) {
            if (action === "delete") {
                $http.post('/api/tourviewTls/deleteSevGroup', sevgroup)
                    .success(function(rs) {
                        var index = $scope.listSevGroup.indexOf(sevgroup);
                        $scope.listSevGroup.splice(index, 1);
                        $("#confirmDeleteService").modal('hide');
                    })
                    .error(function(rs) {

                    })

            } else if (action === "add") {
                if (($scope.sevgroup.supplier || '') == '') {
                    alert('Supplier Name is not empty!');
                    return;
                }
                if (($scope.sevgroup.types || '') == '' || $scope.sevgroup.types === '--') {
                    alert('Service Type is not empty!');
                    return;
                }
                if (($scope.sevgroup.name || '') == '') {
                    alert('Service Name is not empty!');
                    return;
                }
                var temp = {
                    name: $scope.sevgroup.name,
                    supplierTourCode: $scope.sevgroup.supplierTourCode,
                    beginDate: $scope.sevgroup.beginDate,
                    endDate: $scope.sevgroup.endDate,
                    location: $scope.sevgroup.location,
                    supplier: $scope.sevgroup.supplier,
                    types: $scope.sevgroup.types,
                    address: $scope.sevgroup.address,
                    tel: $scope.sevgroup.tel,
                    email: $scope.sevgroup.email,
                    url: $scope.sevgroup.url,
                    group: $scope.sevgroup.group,
                    priceband: $scope.sevgroup.priceband,
                    accumulated: $scope.sevgroup.accumulated
                }
                $http.post('/api/tourviewTls/createSevGroup', temp)
                    .success(function(rs) {

                        $scope.listSevGroup.push(rs);
                        $scope.sevgroup = {
                            group: { currency: "THB" },
                            priceband: { currency: "THB", list: [] },
                            accumulated: { currency: "THB" },
                            location: $scope.sevgroup.location,
                            types: $scope.sevgroup.types,
                            beginDate: $scope.sevgroup.beginDate,
                            endDate: $scope.sevgroup.endDate,
                            supplier: $scope.sevgroup.supplier
                        };
                    })
                    .error(function(rs) {

                    });

            } else if (action === "edit") {
                $('html, body').animate({ scrollTop: 0 }, 'fast');
                console.log(sevgroup);

                $scope.sevgroup = sevgroup;
                $scope.sevgroup.beginDate = new Date(sevgroup.beginDate);
                $scope.sevgroup.endDate = new Date(sevgroup.endDate);
                $scope.sSave = true;
            } else if (action === "update") {
                if (($scope.sevgroup.supplier || '') == '') {
                    alert('Supplier Name is not empty!');
                    return;
                }
                if (($scope.sevgroup.types || '') == '' || $scope.sevgroup.types === '--') {
                    alert('Service Type is not empty!');
                    return;
                }
                if (($scope.sevgroup.name || '') == '') {
                    alert('Service Name is not empty!');
                    return;
                }
                $http.put('/api/tourviewTls/updateSevGroup', $scope.sevgroup)
                    .success(function(rs) {
                        $scope.sSave = false;
                        $scope.sevgroup = { group: { currency: "THB" }, priceband: { currency: "THB", list: [] }, accumulated: { currency: "THB" } };
                    })
                    .error(function(rs) {

                    })
            }
        }

        $scope.CancelSevGroup = function() {
            $scope.sevgroup = { group: { currency: "THB" }, priceband: { currency: "THB", list: [] }, accumulated: { currency: "THB" } };
            $scope.sSave = false;
        }
        $scope.OpenTabInTable = function(evt, id, index) {
            var i, tabcontent, tablinks;
            tabcontent = document.getElementsByClassName('clsTab_' + index);
            for (i = 0; i < tabcontent.length; i++) {
                tabcontent[i].style.display = "none";
            }
            tablinks = document.getElementsByClassName("tablinks_" + index);
            for (i = 0; i < tablinks.length; i++) {
                tablinks[i].className = tablinks[i].className.replace(" active", "");
            }
            document.getElementById(id).style.display = "block";
            evt.currentTarget.className += " active";
        }

        //category
        $scope.InitSevCategory = function() {
            $http.get('/api/tourviewTls/Category')
                .success(function(rs) {
                    $scope.lsCategory = rs;
                })
                .error(function(rs) {

                });
        }

        $scope.Category = {};
        $scope.lsCategory = [];
        $scope.cSave = false;
        $scope.CategoryValue = function(action, Category, $index) {
            if (action === "delete") {
                if (confirm("Are you sure to delete?")) {
                    $http.post('/api/tourviewTls/deleteCategory', Category)
                        .success(function(rs) {
                            $scope.lsCategory.splice($index, 1);
                        });
                }
            } else if (action === "add") {
                var temp = {
                    name: $scope.Category.name,
                    code: $scope.Category.code
                }
                $http.post('/api/tourviewTls/Category', temp)
                    .success(function(rs) {
                        $scope.lsCategory.push(rs);
                        $scope.Category = {};
                    });
            } else if (action === "edit") {
                $('html, body').animate({ scrollTop: 0 }, 'fast');
                $scope.Category = Category;
                $scope.cSave = true;
            } else {
                $http.put('/api/tourviewTls/Category', $scope.Category)
                    .success(function(rs) {
                        $scope.Category = {};
                        $scope.cSave = false;
                    });
            }
        }

        //servicetype

        $scope.ServiceType = {};
        $scope.lsServiceType = [];

        $scope.InitServiceType = function() {
            $http.get('/api/tourviewTls/servicetype')
                .success(function(rs) {
                    $scope.lsServiceType = rs;
                    $scope.sevgroup.types = rs.length > 0 ? rs[0].name : "";
                })
                .error(function(rs) {

                });
        }

        $scope.ServiceTypeValue = function(action, ServiceType, $index) {
            if (action === "delete") {
                if (confirm("Are you sure to delete?")) {
                    $http.post('/api/tourviewTls/deleteservicetype', ServiceType)
                        .success(function(rs) {
                            $scope.lsServiceType.splice($index, 1);
                        });
                }
            } else if (action === "add") {
                var temp = {
                    name: $scope.ServiceType.name,
                    code: $scope.ServiceType.code
                }
                $http.post('/api/tourviewTls/servicetype', temp)
                    .success(function(rs) {
                        $scope.lsServiceType.push(rs);
                        $scope.ServiceType = {};
                    });
            } else if (action === "edit") {
                $('html, body').animate({ scrollTop: 0 }, 'fast');
                $scope.ServiceType = ServiceType;
                $scope.cSave = true;
            } else {
                $http.put('/api/tourviewTls/servicetype', $scope.ServiceType)
                    .success(function(rs) {
                        $scope.ServiceType = {};
                        $scope.cSave = false;
                    });
            }
        }

        //geotree    
        $scope.GeoTree = {};
        $scope.lsGeoTree = [];

        $scope.InitGeoTree = function() {
            $http.get('/api/tourviewTls/geotree')
                .success(function(rs) {
                    $scope.lsGeoTree = rs;
                    $scope.sevgroup.location = rs.length > 0 ? rs[0].name : "";
                })
                .error(function(rs) {

                });
        }

        $scope.GeoTreeValue = function(action, GeoTree, $index) {
            if (action === "delete") {
                if (confirm("Are you sure to delete?")) {
                    $http.post('/api/tourviewTls/deletegeotree', GeoTree)
                        .success(function(rs) {
                            $scope.lsGeoTree.splice($index, 1);
                        });
                }
            } else if (action === "add") {
                var temp = {
                    name: $scope.GeoTree.name,
                    zone: $scope.GeoTree.zone
                }
                $http.post('/api/tourviewTls/geotree', temp)
                    .success(function(rs) {
                        $scope.lsGeoTree.push(rs);
                        $scope.GeoTree = {};
                    });
            } else if (action === "edit") {
                $('html, body').animate({ scrollTop: 0 }, 'fast');
                $scope.GeoTree = GeoTree;
                $scope.cSave = true;
            } else {
                $http.put('/api/tourviewTls/geotree', $scope.GeoTree)
                    .success(function(rs) {
                        $scope.GeoTree = {};
                        $scope.cSave = false;
                    });
            }
        }

        $scope.CancelGeoTree = function() {
            $scope.GeoTree = {};
            $scope.cSave = false;
        }

        //exchangerate    
        $scope.ExchangeRate = { USD: 1 };
        $scope.lsExchangeRate = [];

        $scope.InitExchangeRate = function() {
            $http.get('/api/tourviewTls/exchangerate')
                .success(function(rs) {
                    $scope.lsExchangeRate = rs;
                })
                .error(function(rs) {

                });
        }

        $scope.ExchangeRateValue = function(action, ExchangeRate, $index) {
            if (action === "delete") {
                if (confirm("Are you sure to delete?")) {
                    $http.post('/api/tourviewTls/deleteexchangerate', ExchangeRate)
                        .success(function(rs) {
                            $scope.lsExchangeRate.splice($index, 1);
                        });
                }
            } else if (action === "add") {
                var temp = {
                    beginDate: $scope.ExchangeRate.beginDate,
                    endDate: $scope.ExchangeRate.endDate,
                    VND: $scope.ExchangeRate.VND,
                    USD: $scope.ExchangeRate.USD
                }
                $http.post('/api/tourviewTls/exchangerate', temp)
                    .success(function(rs) {
                        $scope.lsExchangeRate.push(rs);
                        $scope.ExchangeRate = { USD: 1 };
                    });
            } else if (action === "edit") {
                $('html, body').animate({ scrollTop: 0 }, 'fast');
                ExchangeRate.beginDate = new Date(ExchangeRate.beginDate);
                ExchangeRate.endDate = new Date(ExchangeRate.endDate);
                $scope.ExchangeRate = ExchangeRate;
                $scope.cSave = true;
            } else {
                $http.put('/api/tourviewTls/ExchangeRate', $scope.ExchangeRate)
                    .success(function(rs) {
                        $scope.ExchangeRate = { USD: 1 };
                        $scope.cSave = false;
                    });
            }
        }

        $scope.CancelExchangeRate = function() {
            $scope.ExchangeRate = { USD: 1 };
            $scope.cSave = false;
            $("#beginDate").val("");
            $("#endDate").val("");
        };
        // TariffPeriod
        $scope.TariffPeriod = {};
        $scope.lsTariffPeriod = [];

        $scope.InitTariffPeriod = function() {
            $http.get('/api/tourviewTls/tariffperiod')
                .success(function(rs) {
                    $scope.lsTariffPeriod = rs;
                })
                .error(function(rs) {

                });
        };

        $scope.TariffPeriodValue = function(action, TariffPeriod, $index) {
            if (action === 'delete') {
                if (confirm('Are you sure to delete?')) {
                    $http.post('/api/tourviewTls/deletetariffperiod', TariffPeriod)
                        .success(function(rs) {
                            $scope.lsTariffPeriod.splice($scope.lsTariffPeriod.indexOf(TariffPeriod), 1);
                        });
                }
            } else if (action === 'add') {
                var temp = {
                    date: $scope.TariffPeriod.date,
                    name: $scope.TariffPeriod.name,
                    no: $scope.TariffPeriod.no,
                };
                $http.post('/api/tourviewTls/TariffPeriod', temp)
                    .success(function(rs) {
                        $scope.lsTariffPeriod.push(rs);
                        $scope.TariffPeriod = {};
                    });
            } else if (action === 'edit') {
                $('html, body').animate({ scrollTop: 0 }, 'fast');
                TariffPeriod.date = new Date(TariffPeriod.date);
                $scope.TariffPeriod = TariffPeriod;
                $scope.taSave = true;
            } else {
                $http.put('/api/tourviewTls/TariffPeriod', $scope.TariffPeriod)
                    .success(function(rs) {
                        $scope.TariffPeriod = {};
                        $scope.taSave = false;
                    });
            }
        };

        $scope.CancelTariffPeriod = function() {
            $scope.TariffPeriod = {};
            $scope.taSave = false;
            $('#beginDate').val('');
            $('#endDate').val('');
        };
        //supplier    
        $scope.Supplier = { USD: 1 };
        $scope.lsSupplier = [];

        $scope.InitSupplier = function() {
            $http.get('/api/tourviewTls/supplier')
                .success(function(rs) {
                    $scope.lsSupplier = rs;
                })
                .error(function(rs) {

                });
        }

        $scope.SupplierValue = function(action, Supplier, $index) {
            if (action === "delete") {
                if (confirm("Are you sure to delete?")) {
                    $http.post('/api/tourviewTls/deletesupplier', Supplier)
                        .success(function(rs) {
                            $scope.lsSupplier.splice($index, 1);
                        });
                }
            } else if (action === "add") {
                var temp = {
                    name: $scope.Supplier.name,
                    address: $scope.Supplier.address,
                    phone: $scope.Supplier.phone,
                    email: $scope.Supplier.email
                }
                $http.post('/api/tourviewTls/supplier', temp)
                    .success(function(rs) {
                        $scope.lsSupplier.push(rs);
                        $scope.Supplier = {};
                    });
            } else if (action === "edit") {
                $('html, body').animate({ scrollTop: 0 }, 'fast');
                $scope.Supplier = Supplier;
                $scope.cSave = true;
            } else {
                $http.put('/api/tourviewTls/supplier', $scope.Supplier)
                    .success(function(rs) {
                        $scope.Supplier = {};
                        $scope.cSave = false;
                    });
            }
        }

        $scope.CancelSupplier = function() {
            $scope.Supplier = {};
            $scope.cSave = false;
        }

        //Priority    
        $scope.Priority = { USD: 1 };
        $scope.lsPriority = [];

        $scope.InitPriority = function() {
            $http.get('/api/tourviewTls/priority')
                .success(function(rs) {
                    $scope.lsPriority = rs;
                })
                .error(function(rs) {

                });
        }

        $scope.PriorityValue = function(action, Priority, $index) {
            if (action === "delete") {
                if (confirm("Are you sure to delete?")) {
                    $http.post('/api/tourviewTls/deletepriority', Priority)
                        .success(function(rs) {
                            $scope.lsPriority.splice($index, 1);
                        });
                }
            } else if (action === "add") {
                var temp = {
                    name: $scope.Priority.name,
                    code: $scope.Priority.code
                }
                $http.post('/api/tourviewTls/priority', temp)
                    .success(function(rs) {
                        $scope.lsPriority.push(rs);
                        $scope.Priority = {};
                    });
            } else if (action === "edit") {
                $('html, body').animate({ scrollTop: 0 }, 'fast');
                $scope.Priority = Priority;
                $scope.cSave = true;
            } else {
                $http.put('/api/tourviewTls/priority', $scope.Priority)
                    .success(function(rs) {
                        $scope.Priority = {};
                        $scope.cSave = false;
                    });
            }
        }

        $scope.CancelPriority = function() {
            $scope.Priority = {};
            $scope.cSave = false;
        }


        //Notify     
        $scope.Notify = { USD: 1 };
        $scope.lsNotify = [];

        $scope.InitNotify = function() {
            $http.get('/api/tourviewTls/notify')
                .success(function(rs) {
                    $scope.lsNotify = rs;
                })
                .error(function(rs) {

                });
        }

        $scope.NotifyValue = function(action, Notify, $index) {
            if (action === "delete") {
                if (confirm("Are you sure to delete?")) {
                    $http.post('/api/tourviewTls/deletenotify', Notify)
                        .success(function(rs) {
                            $scope.lsNotify.splice($index, 1);
                        });
                }
            } else if (action === "add") {
                var temp = {
                    text: $scope.Notify.text,
                    isActive: $scope.Notify.isActive
                }
                $http.post('/api/tourviewTls/notify', temp)
                    .success(function(rs) {
                        $scope.lsNotify.push(rs);
                        $scope.Notify = {};
                    });
            } else if (action === "edit") {
                $('html, body').animate({ scrollTop: 0 }, 'fast');
                $scope.Notify = Notify;
                $scope.cSave = true;
            } else {
                $http.put('/api/tourviewTls/notify', $scope.Notify)
                    .success(function(rs) {
                        $scope.Notify = {};
                        $scope.cSave = false;
                    });
            }
        }

        $scope.CancelNotify = function() {
            $scope.Notify = {};
            $scope.cSave = false;
        }

        //Users
        $scope.Users = { role: [] };
        $scope.lsUsers = [];
        $scope.lsRole = ["User", "View", "Product", "Reservation", "Admin"];
        $scope.CheckRole = function(x) {
            for (var i = 0; i < $scope.Users.role.length; i++) {
                if (x == $scope.Users.role[i].name)
                    return true;
            }
            return false;
        }
        $scope.SetRole = function(x) {
            for (var i = 0; i < $scope.Users.role.length; i++) {
                if (x == $scope.Users.role[i].name) {
                    $scope.Users.role.splice(i, 1);
                    if ($scope.Users.role.length == 0) {

                    }
                    return;
                }
            }
            $scope.Users.role.push({ name: x, code: x });
        }
        $scope.InitUsers = function() {
            $http.get('/api/tourviewTls/users')
                .success(function(rs) {
                    $scope.lsUsers = rs;
                })
                .error(function(rs) {

                });

            $(".dropCheck dt a").on('click', function() {
                $(".dropCheck dd ul").slideToggle('fast');
            });

            $(".dropCheck dd ul li a").on('click', function() {
                $(".dropCheck dd ul").hide();
            });
            $(document).bind('click', function(e) {
                var $clicked = $(e.target);
                if (!$clicked.parents().hasClass("dropCheck")) $(".dropCheck dd ul").hide();
            });
        }
        $scope.UsersValue = function(action, Users, $index) {
            if (action === "delete") {
                if (confirm("Are you sure to delete?")) {
                    $http.post('/api/tourviewTls/deleteusers', Users)
                        .success(function(rs) {
                            $scope.lsUsers.splice($index, 1);
                        });
                }
            } else if (action === "add") {
                var temp = {
                    fullname: $scope.Users.fullname,
                    email: $scope.Users.email,
                    company: $scope.Users.company,
                    username: $scope.Users.username,
                    pass: $scope.Users.pass,
                    active: $scope.Users.active,
                    role: $scope.Users.role
                }
                $http.post('/api/tourviewTls/users', temp)
                    .success(function(rs) {
                        $scope.lsUsers.push(rs);
                        $scope.Users = {};
                    });
            } else if (action === "edit") {
                $('html, body').animate({ scrollTop: 0 }, 'fast');
                $scope.Users = Users;
                $scope.cSave = true;
            } else {
                $http.put('/api/tourviewTls/users', $scope.Users)
                    .success(function(rs) {
                        $scope.Users = {};
                        $scope.cSave = false;
                    });
            }
        }

        $scope.CancelUsers = function() {
            $scope.Users = { role: [] };
            $scope.cSave = false;
        }



        //Client
        $scope.jssor_1_slider_init = function() {
            var jssor_1_options = {
                $AutoPlay: true,
                $AutoPlaySteps: 4,
                $SlideDuration: 300,
                $SlideWidth: 290,
                $SlideSpacing: 4,
                $Cols: 4,
                $ArrowNavigatorOptions: {
                    $Class: $JssorArrowNavigator$,
                    $Steps: 4
                },
                $BulletNavigatorOptions: {
                    $Class: $JssorBulletNavigator$,
                    $SpacingX: 1,
                    $SpacingY: 1
                }
            };

            var jssor_1_slider = new $JssorSlider$("jssor_1", jssor_1_options);

            /*responsive code begin*/
            /*you can remove responsive code if you don't want the slider scales while window resizing*/
            function ScaleSlider() {
                var refSize = jssor_1_slider.$Elmt.parentNode.clientWidth;
                if (refSize) {
                    refSize = Math.min(refSize, 1170);
                    jssor_1_slider.$ScaleWidth(refSize);
                } else {
                    window.setTimeout(ScaleSlider, 400);
                }
            }
            ScaleSlider();
            /*responsive code end*/
        };

        $scope.ShowBy = function(act) {
                if (act == 'Proposal') {
                    $("#BtMenu .active").removeClass('active');
                    $("#Bt3").addClass('active')
                    $scope.isJourney = false;
                    $scope.isProposal = true;
                } else {
                    $("#BtMenu .active").removeClass('active');
                    $("#Bt2").addClass('active')
                    $scope.isProposal = false;
                    $scope.isJourney = true;
                }
                clientLoading();
            }
            //EXCURSIONS
        $scope.ShowExcursions = function() {
            $("#BtMenu .active").removeClass('active');
            $("#Bt1").addClass('active')
            $scope.isProposal = false;
            $scope.isJourney = false;
            clientLoading();
        }
        $scope.FilterDataSave = function() {
            clientLoading();
        }
        $http.get('/api/tourviewTls/webinfo')
            .success(function(rs) {
                if (rs) {
                    $scope.WebInfo = rs;
                    $scope.WebInfo.coverpage = $sce.trustAsHtml($scope.WebInfo.coverpage);
                }
            })
            .error(function(rs) {

            });
        $scope.ShotProposalStatus = function(txt) {
            var text = "";
            switch (txt) {
                case "NEW":
                    text = "N";
                    break;
                case "PENDING":
                    text = "P";
                    break;
                case "CANCELLED":
                    text = "CLX";
                    break;
                case "CONFIRMED":
                    text = "CFM";
                    break;
            }
            return text;
        }
        $scope.ObjectAgent = {};
        $scope.maxSize = 5;
        $scope.clientLoading = function() {
            $http.get('http://da-tariff.com/modules.php?name=HotelView_TL&op=agentjson&token=' + convertToken())
                .success(function(data) {
                    angular.forEach(data, function(vl, key) {
                        if (vl.md5code === $stateParams.idCode.substring(0, $stateParams.idCode.length - 1)) {
                            console.log(vl);
                            $scope.ObjectAgent = vl;
                        }
                    });
                });
            $http.get('/api/tourviewTls/geotree')
                .success(function(rs) {
                    $scope.lsGeoTree = rs;
                    // $scope.info.location = rs.length > 0 ? rs[0].name : "";
                });
             $http.get('/api/tourviewTls/tariffperiod')
                .success(function(rs) {
                    $scope.lstariffperiod = rs;
                    angular.forEach(rs, function(value){
                      if(value.no === 1)
                        {
                            $scope.dateFilter = value.date;
                            $scope.vlDefault = value.date;
                            // break;
                        }
                    });                    
                    
                });
            $http.get('/api/tourviewTls/loadnotifyclient')
                .success(function(rs) {
                    $scope.loadnotifyclient = rs;
                    $timeout(function() {
                        loadslider();
                    }, 100)

                });

            $http.get('/api/tourviewTls/priority')
                .success(function(rs) {
                    $scope.lsPriority = rs;
                });

            $http.get('/api/tourviewTls/Category')
                .success(function(rs) {
                    $scope.lsCategory = rs;
                    // $scope.info.categoryName = rs.length > 0 ? rs[0].name : "";
                });

            clientLoading();
        };
        $scope.pagingClick = function() {
            clientLoading();
        };

        function convertToken() {
            var date = new Date();
            var day = date.getDate() < 10 ? "0" + date.getDate() : date.getDate();
            var month = (date.getMonth() + 1) < 10 ? "0" + (date.getMonth() + 1) : (date.getMonth() + 1);
            var Year = date.getFullYear();
            var text = day +""+ month +""+ Year;
            return md5.createHash(text + 'DAVN')
        }

        function clientLoading() {
            $scope.clientLoading = true;
            $http.get('http://da-tariff.com/modules.php?name=HotelView_TL&op=crmjson&ccode=' + $stateParams.idCode)
                .success(function(rs) {
                    $scope.code = $stateParams.idCode;
                    $scope.markup = rs;
                    if ($scope.markup == 'false') {
                        $location.path('/tourview/sorry');
                    }
                    $scope.cmarket = $scope.markup.cmarket;

                    $http.get('/api/tourviewTls/exchangerate')
                        .success(function(lsExchangeRate) {
                            GeDataView();

                            function GeDataView() {
                                var param = {};
                                if ($scope.ListlocationFIlter.length > 0) param.location = { $in: $scope.ListlocationFIlter };
                                if ($scope.ListcategoryFIlter.length > 0) param.category = { $in: $scope.ListcategoryFIlter };
                                if ($scope.ListpriorityFIlter.length > 0) param.priority = { $in: $scope.ListpriorityFIlter };
                                param.isActive = true;
                                if ($scope.isProposal == true) {
                                    var isCode = $stateParams.idCode.substring(0, $stateParams.idCode.length - 1)
                                    param.md5code = { $in: isCode }
                                    param.ValueEditModel = { $in: ['proposal'] }
                                } else if ($scope.isJourney == true) {
                                    var isCode = $stateParams.idCode.substring(0, $stateParams.idCode.length - 1)
                                    param.md5code = { $in: [isCode] }
                                    param.beginDate = { "$lte": $scope.dateFilter };
                                    param.endDate = { "$gte": $scope.dateFilter };
                                    param.ValueEditModel = { $in: ['excursions', 'package'] }
                                } else {
                                    param.md5code = { $in: [isCode, undefined, ''] }
                                    param.beginDate = { "$lte": $scope.dateFilter };
                                    param.endDate = { "$gte": $scope.dateFilter };
                                }
                                var codemd5 = $stateParams.idCode.substring(0, $stateParams.idCode.length - 1);
                                $http.post('/api/tourviewTls/loaddata', {
                                        markup: $scope.markup,
                                        lsExchangeRate: lsExchangeRate,
                                        param: param,
                                        _pageNumber: $scope.currentPage,
                                        search: $scope.search,
                                        iScode: codemd5
                                    })
                                    .success(function(rs) {
                                        $scope.showDataClient = rs.showDataClient;
                                        $scope.countItem = rs.count;
                                        $scope.totalPaging = rs.count;
                                        $scope.clientLoading = false;
                                    }).error(function(rs) {})
                            }


                        }).error(function() {});

                }).error(function(rs) {});

        }

        $scope.contentShow = function(x) {
            splashOpen("/tourview-tl/detail/" + x._id + "/" + $stateParams.idCode);
        }

        function splashOpen(url) {
            var w = screen.availWidth - 10;
            var h = screen.availHeight - 20;
            var winFeatures = 'screenX=0,screenY=0,top=0,left=0,scrollbars,width=' + w + ',height=' + h + '';
            var winName = 'window';
            var win = window.open(url, winName, winFeatures);
            var extraWidth = win.screen.availWidth - win.outerWidth;
            var extraHeight = win.screen.availHeight - win.outerHeight;
            win.resizeBy(extraWidth, extraHeight);
            return win;
        }
        $scope.ViewVideo = function() {
            $("#popupVideo").modal('show');
            $("#BtMenu .active").removeClass('active');
            $("#Bt5").addClass('active')
        }
        $scope.offVideo = function() {
            $('#styled_video')[0].pause();
        }
        $scope.timeVal = [];
        $scope.ShownoteContent = false;

        function ClearLandingMkforPackage(price, LandingMK) {
            var vl = price / (1 + LandingMK / 100);
            return vl;
        }

        function decimalAdjust(type, value, exp) {
            // If the exp is undefined or zero...
            if (typeof exp === 'undefined' || +exp === 0) {
                return Math[type](value);
            }
            value = +value;
            exp = +exp;
            // If the value is not a number or the exp is not an integer...
            if (isNaN(value) || !(typeof exp === 'number' && exp % 1 === 0)) {
                return NaN;
            }
            // Shift
            value = value.toString().split('e');
            value = Math[type](+(value[0] + 'e' + (value[1] ? (+value[1] - exp) : -exp)));
            // Shift back
            value = value.toString().split('e');
            return +(value[0] + 'e' + (value[1] ? (+value[1] + exp) : exp));
        }

        $scope.DetailGet = function() {

            $('[data-toggle="tooltip"]').tooltip();
            var getDetailById = {
                _id: $stateParams.tourtariffId
            };
            $http.get('/api/tourviewTls/webinfo')
                .success(function(rs) {
                    if (rs) {
                        $scope.WebInfo = rs;
                        $scope.term = $sce.trustAsHtml(rs.term);
                        $scope.WebInfo.coverpage = $sce.trustAsHtml($scope.WebInfo.coverpage);
                    }
                })
                .error(function(rs) {

                });
            $scope.markup = 0;
            $http.get('http://da-tariff.com/modules.php?name=HotelView_TL&op=crmjson&ccode=' + $stateParams.codeID)
                .success(function(rs) {
                    $scope.markup = rs;
                    if ($scope.markup == 'false') {
                        $location.path('/tourview/sorry');
                    }
                }).error(function(rs) {});
            $scope.item = {};
            $http.post('/api/tourviewTls/LoadDataSaveById', getDetailById)
                .success(function(rs) {
                    $scope.item = rs;

                    if ($scope.item.availab == undefined)
                        $scope.item.availab = { sun: true, mon: true, tue: true, wed: true, thu: true, fri: true, sat: true };
                    $http.get('/api/tourviewTls/exchangerate')
                        .success(function(lsExchangeRate) {
                            var ctourpro = $scope.markup.cpcode == "1" ? 0 : parseInt($scope.markup.ctourpro);
                            $scope.lsExchangeRate = lsExchangeRate;
                            $scope.content = $sce.trustAsHtml($scope.item.content)
                            if (!angular.isUndefined($scope.item.note)) {
                                if ($scope.item.note !== "")
                                    $scope.ShownoteContent = true;
                            }
                            $scope.noteContent = $sce.trustAsHtml($scope.item.note)
                            var h = $scope.item.startTime.hour;
                            var m = $scope.item.startTime.minute;

                            var VND = 1,
                                USD = 1;
                            var middle = new Date($scope.item.beginDate.replace("T17:00:00.000Z", "")).getTime();
                            for (var i = 0; i < $scope.lsExchangeRate.length; i++) {
                                var begin = new Date($scope.lsExchangeRate[i].beginDate.replace("T17:00:00.000Z", "")).getTime();
                                var end = new Date($scope.lsExchangeRate[i].endDate.replace("T17:00:00.000Z", "")).getTime();
                                if (begin <= middle && middle <= end) {
                                    VND = $scope.lsExchangeRate[i].VND;
                                    USD = $scope.lsExchangeRate[i].USD;
                                    break;
                                }
                            }
                            // if($scope.item.tiers[0].price == null)
                            //     $scope.hidenTiers=  true;


                            $scope.totalSGlSUPPL = 0;
                            $scope.allHotel = "";
                            if ($scope.item.lsDirectModel.length > 0) {
                                $scope.item.lsDirectModel.forEach(function(value, key) {
                                    var currency = $scope.item.currency;
                                    var parentCurrency = $scope.markup.ccurrency
                                    value.SGLSuppl = changeVl(currency, parentCurrency, value.SGLSuppl);
                                    value.SGLSuppl = $scope.item.CRM == true ? Math.round(value.SGLSuppl + value.SGLSuppl * parseInt(ctourpro || 0) / 100) : value.SGLSuppl
                                    if ($scope.markup.cpcode == '1') {
                                        value.SGLSuppl = ClearLandingMkforPackage(value.SGLSuppl, $scope.item.HotelMkALL || value.directMkModel)
                                    }
                                    $scope.totalSGlSUPPL += value.SGLSuppl;
                                    if (value.name !== "undefined")
                                        $scope.allHotel += $sce.trustAsHtml("<br> &emsp;&emsp;&emsp;&emsp; - " + value.name);
                                    for (var y = 0; y < value.tiers.length; y++) {
                                        var ti = value.tiers[y];
                                        var priceMarkup = $scope.item.CRM == true ? Math.round(ti.price + ti.price * parseInt(ctourpro || 0) / 100) : ti.price;
                                        if ($scope.markup.cpcode == '1') {
                                            var priceHotelHaveMK = ti.price - $scope.item.tiers[y].price;
                                            var priceLading = ClearLandingMkforPackage($scope.item.tiers[y].price, $scope.item.LandingMK);
                                            var VLhotel = ClearLandingMkforPackage(priceHotelHaveMK, $scope.item.HotelMkALL || value.directMkModel)
                                            priceMarkup = VLhotel + priceLading;
                                        }
                                        ti.price = changeVl(currency, parentCurrency, priceMarkup);
                                    };
                                });
                            }

                            function changeVl(currency, parentCurrency, priceMarkup) {
                                var vl = 0;
                                if (currency == parentCurrency)
                                    vl = Math.round(priceMarkup);
                                else if (currency == "USD" && parentCurrency == "THB") {
                                    vl = Math.round(priceMarkup * VND / USD);
                                } else if (currency == "THB" && parentCurrency == "USD") {
                                    vl = Math.round(priceMarkup * USD / VND);
                                }
                                return vl;
                            }

                            $scope.lengthTiers = 1;
                            $scope.item.tiers.forEach(function(value) {
                                if (value.pax > 0) $scope.lengthTiers++;
                            });

                            if (rs.Groupcode) {
                                $http.post('/api/tourviewTls/GettiersByGroupCode', { Groupcode: $scope.item.Groupcode })
                                    .success(function(Grs) {
                                        var lsGrs = [];
                                        angular.forEach(Grs, function(vl, key) {
                                            var currency = vl.currency;
                                            angular.forEach(vl.tiers, function(value, key) {
                                                var priceMarkup = 0;

                                                if ($scope.item.ValueEditModel == "package" && $scope.item.lsDirectModel.length == 0) {
                                                    priceMarkup = $scope.item.CRM == true ? Math.round(value.price + value.price * parseInt(ctourpro || 0) / 100) : value.price;
                                                    if ($scope.markup.cpcode == '1') {
                                                        priceMarkup = ClearLandingMkforPackage(priceMarkup, $scope.item.LandingMK)
                                                    }
                                                } else if ($scope.item.ValueEditModel == "proposal" && $scope.item.lsDirectModel.length > 0) {
                                                    priceMarkup = $scope.item.CRM == true ? Math.round(value.price + value.price * parseInt(ctourpro || 0) / 100) : value.price;
                                                } else {
                                                    priceMarkup = Math.round(value.price + value.price * parseInt(ctourpro || 0) / 100);
                                                }

                                                var parentCurrency = $scope.markup.ccurrency
                                                if (currency == parentCurrency)
                                                    value.price = Math.round(priceMarkup);
                                                else if (currency == "USD" && parentCurrency == "THB") {
                                                    value.price = Math.round(priceMarkup * VND / USD);
                                                } else if (currency == "THB" && parentCurrency == "USD") {
                                                    value.price = Math.round(priceMarkup * USD / VND);
                                                }
                                                if (value.ChildAplly == 'Yes') {
                                                    for (var i = 0; i < value.priceChild.length; i++) {
                                                        if (value.priceChild[i].vl) {
                                                            var vl = value.priceChild[i].vl;
                                                            var priceMarkup = Math.round(vl + (vl * parseInt(ctourpro || 0) / 100));
                                                            var parentCurrency = $scope.markup.ccurrency
                                                            if (currency == parentCurrency)
                                                                vl = priceMarkup;
                                                            else if (currency == "USD" && parentCurrency == "THB") {
                                                                vl = Math.round(priceMarkup * VND / USD);
                                                            } else if (currency == "THB" && parentCurrency == "USD") {
                                                                vl = Math.round(priceMarkup * USD / VND);
                                                            }
                                                            value.priceChild[i].vl = vl;
                                                        }
                                                    }
                                                }
                                            });
                                            lsGrs.push(vl);
                                        });
                                        $scope.item.ListGroupcode = lsGrs;
                                    })
                            }

                            angular.forEach($scope.item.tiers, function(value, key) {
                                var priceMarkup = 0;

                                if ($scope.item.ValueEditModel == "package" && $scope.item.lsDirectModel.length == 0) {
                                    priceMarkup = $scope.item.CRM == true ? Math.round(value.price + value.price * parseInt(ctourpro || 0) / 100) : value.price;
                                    if ($scope.markup.cpcode == '1') {
                                        priceMarkup = ClearLandingMkforPackage(priceMarkup, $scope.item.LandingMK)
                                    }
                                } else if ($scope.item.ValueEditModel == "proposal" && $scope.item.lsDirectModel.length > 0) {
                                    priceMarkup = $scope.item.CRM == true ? Math.round(value.price + value.price * parseInt(ctourpro || 0) / 100) : value.price;
                                } else {
                                    priceMarkup = Math.round(value.price + value.price * parseInt(ctourpro || 0) / 100);
                                }
                                var currency = $scope.item.currency;
                                var parentCurrency = $scope.markup.ccurrency
                                if (currency == parentCurrency)
                                    value.price = Math.round(priceMarkup);
                                else if (currency == "USD" && parentCurrency == "THB") {
                                    value.price = Math.round(priceMarkup * VND / USD);
                                } else if (currency == "THB" && parentCurrency == "USD") {
                                    value.price = Math.round(priceMarkup * USD / VND);
                                }

                                if (value.ChildAplly == 'Yes') {
                                    for (var i = 0; i < value.priceChild.length; i++) {
                                        if (value.priceChild[i].vl) {
                                            var vl = value.priceChild[i].vl;
                                            var priceMarkupChild = Math.round(vl + (vl * parseInt(ctourpro || 0) / 100));
                                            var currency = $scope.item.currency;
                                            var parentCurrency = $scope.markup.ccurrency
                                            if (currency == parentCurrency)
                                                vl = priceMarkupChild;
                                            else if (currency == "USD" && parentCurrency == "THB") {
                                                vl = Math.round(priceMarkupChild * VND / USD);
                                            } else if (currency == "THB" && parentCurrency == "USD") {
                                                vl = Math.round(priceMarkupChild * USD / VND);
                                            }
                                            value.priceChild[i].vl = vl;
                                        }

                                    }
                                }
                            });
                            angular.forEach($scope.item.DirectMarkup, function(value, key) {
                                var priceMarkup = $scope.markup.cpcode == "1" ? value.byPrice : value.price;
                                var currency = $scope.item.currency;
                                var parentCurrency = $scope.markup.ccurrency
                                if (currency == parentCurrency)
                                    value.price = Math.round(priceMarkup);
                                else if (currency == "USD" && parentCurrency == "THB") {
                                    value.price = Math.round(priceMarkup * VND / USD);
                                } else if (currency == "THB" && parentCurrency == "USD") {
                                    value.price = Math.round(priceMarkup * USD / VND);
                                }

                            });
                            angular.forEach($scope.item.lsExtra, function(value, key) {
                                var priceMarkup = Math.round(value.price + value.price * parseInt(ctourpro || 0) / 100);
                                var currency = $scope.item.currency;
                                var parentCurrency = $scope.markup.ccurrency
                                if (currency == parentCurrency)
                                    value.price = priceMarkup;
                                else if (currency == "USD" && parentCurrency == "THB") {
                                    value.price = Math.round(priceMarkup * VND / USD);
                                } else if (currency == "THB" && parentCurrency == "USD") {
                                    value.price = Math.round(priceMarkup * USD / VND);
                                }
                            });
                            $scope.item.currency = $scope.markup.ccurrency;
                            $("#timevalue").val(($scope.item.startTime.hour < 10 ? "0" + $scope.item.startTime.hour : $scope.item.startTime.hour) + ":" + ($scope.item.startTime.minute < 10 ? "0" + $scope.item.startTime.minute : $scope.item.startTime.minute))
                            $scope.maxTiming = $scope.item.maxTiming;
                            $scope.timeView = timeConvert($scope.item.maxTiming);
                            getTime($scope.item.lsTiming, h, m)
                                // $scope.item.lsTiming = defaulttime($scope.item.lsTiming);
                            $('.clockpicker').clockpicker({
                                placement: 'right',
                                align: 'right',
                                donetext: 'Done',
                                afterDone: function() {
                                    var time = $("#timevalue").val();
                                    var ath = parseInt(time.substr(0, 2));
                                    var atm = parseInt(time.substr(3, 2));
                                    getTime($scope.item.lsTiming, ath, atm)
                                }

                            });
                            // $scope.info.categoryName = rs.length > 0 ? rs[0].name : "";
                            function timeConvert(n) {
                                var minutes = (n % 60) < 10 ? "0" + (n % 60) : (n % 60);
                                var hours = (n - minutes) / 60;
                                return hours + "h" + minutes;
                            }

                            function getTime(data, h, m) {
                                var datatime = [];
                                angular.forEach(data, function(val, key) {
                                    var minutes = m + val.duration;
                                    var hours = h + parseInt(minutes / 60);
                                    minutes = minutes % 60;

                                    var vh = hours >= 10 ? hours : "0" + hours;
                                    var vms = minutes >= 10 ? minutes : "0" + minutes;
                                    val.TimeVl = vh + ":" + vms;

                                });
                                $scope.timeVal = data;
                            }
                            $scope.items = $scope.item;
                            console.log($scope.items);
                        }).error(function() {});


                }).error(function(rs) {});

        }
        $scope.SubmitTime = function() {
            $scope.timeVal = $scope.timeVal;
        }
        $scope.defaulttime = function() {
            var time = $("#timevalue").val();
            var h = parseInt(time.substr(0, 2));
            var m = parseInt(time.substr(3, 2));
            var datatime = [];
            angular.forEach($scope.item.lsTiming, function(val, key) {
                var minutes = m + val.duration;
                var hours = h + parseInt(minutes / 60);
                minutes = minutes % 60;

                var vh = hours >= 10 ? hours : "0" + hours;
                var vms = minutes >= 10 ? minutes : "0" + minutes;

                val.TimeVl = vh + ":" + vms;
            });
        }

        $scope.Exportdata = function(name) {
            $("#content p").each(function(index) {
                var a = $(this).find("img");
                $(a).insertBefore(this);
            });

            if (navigator.userAgent.indexOf('Mac') > 1) {
                if (isSafari()) {
                    alert("This function is worked properly on Browses of Chrome,\n Firefox and Edge. Please try it again on those platforms.\n \t\t\t Thank you for your patience. \n\n \t\t\t ▬▬▬▬▬▬▬▬▬ஜ۩۞۩ஜ▬▬▬▬▬▬▬▬▬\n\n")
                } else {
                    $("#content").wordExport(name);
                }
            } else {
                $("#content").wordExport(name);
            }
        };

        function isSafari() {
            return /^((?!chrome).)*safari/i.test(navigator.userAgent);
        }

        $scope.PrinterCheck = function() {
            var printContents = document.getElementById('content').innerHTML;
            var myWindow = window.open('', '_blank');
            myWindow.document.write(printContents);
            myWindow.document.close();
            myWindow.focus();
            myWindow.print();
            myWindow.close();
        };

        var specialElementHandlers = {
            '#editor': function(element, renderer) {
                return true;
            }
        };
        $scope.ExportPDF = function(name) {
            var pdf = document.getElementById("content");
            pdf.print();

            // var doc = new jsPDF();
            // doc.fromHTML($('#dialog-pdf').html(), 15, 15, {
            //     'width': 170,'elementHandlers': specialElementHandlers
            // });
            // doc.save(name+'.pdf');







            // var pdf = new jsPDF('p', 'mm', 'a4');
            //  pdf.fromHTML($('#content').html(), 15, 15, {
            //      'width': 170
            //  });
            //  pdf.save(name+'.pdf');
        }

        $scope.ChangeTime = function(time) {
            var date = new Date(time);
            var day = date.getDate();
            var month = monthText(date.getMonth() + 1);
            var Year = date.getFullYear();
            return (day < 10 ? "0" + day : day) + "" + (month < 10 ? "0" + month : month) + "" + Year;
        }

        function CheckBrowse() {
            if (navigator.userAgent.indexOf('Mac') > 1) {
                if (isSafari()) {
                    alert("This function is worked properly on Browses of Chrome,\n Firefox and Edge. Please try it again on those platforms.\n \t\t\t Thank you for your patience. \n\n \t\t\t ▬▬▬▬▬▬▬▬▬ஜ۩۞۩ஜ▬▬▬▬▬▬▬▬▬\n\n")
                } else {
                    ExportDataExcell();

                }
            } else {
                ExportDataExcell();
            }

            function ExportDataExcell() {
                var blob = new Blob([document.getElementById('table_wrapper').innerHTML], {
                    type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
                });
                saveAs(blob, "tour_tariffs" + dateTour() + ".xls");
                $scope.clientLoading = false;

                function dateTour() {
                    var date = new Date();
                    var day = date.getDate();
                    var month = monthText(date.getMonth() + 1);
                    var Year = date.getFullYear();
                    return day + "" + month + "" + Year;
                }
            }
        }
        $scope.ExportExcel = function() {
            $("#BtMenu .active").removeClass('active');
            $("#step4").addClass('active')
            $scope.clientLoading = true;
            $http.get('/api/tourviewTls/exchangerate')
                .success(function(lsExchangeRate) {
                    var param = {};
                    if ($scope.ListlocationFIlter.length > 0) param.location = { $in: $scope.ListlocationFIlter };
                    if ($scope.ListcategoryFIlter.length > 0) param.category = { $in: $scope.ListcategoryFIlter };
                    if ($scope.ListpriorityFIlter.length > 0) param.priority = { $in: $scope.ListpriorityFIlter };
                    param.isActive = true;
                    if ($scope.isProposal == true) {
                        var isCode = $stateParams.idCode.substring(0, $stateParams.idCode.length - 1)
                        param.md5code = { $in: isCode }
                        param.ValueEditModel = { $in: ['proposal'] }
                    } else if ($scope.isJourney == true) {
                        var isCode = $stateParams.idCode.substring(0, $stateParams.idCode.length - 1)
                        param.md5code = { $in: isCode }
                        param.beginDate = { "$lte": $scope.dateFilter };
                        param.endDate = { "$gte": $scope.dateFilter };
                        param.ValueEditModel = { $in: ['excursions', 'package'] }
                    } else {
                        param.md5code = { $in: [isCode, undefined, ''] }
                        param.beginDate = { "$lte": $scope.dateFilter };
                        param.endDate = { "$gte": $scope.dateFilter };
                    }
                    $http.post('/api/tourviewTls/loaddatasaveexcel', { markup: $scope.markup, lsExchangeRate: lsExchangeRate, param: param, search: $scope.search })
                        .success(function(rs) {
                            $scope.showDataExcel = rs.showDataClient;
                            $timeout(function() {
                                CheckBrowse();
                            }, 3000);

                        }).error(function(rs) {
                            $scope.clientLoading = false;
                        });
                })
                .error(function(rs) {
                    $scope.clientLoading = false;
                });
        }

        $scope.CompletedEvent = function(scope) {
            console.log("Completed Event called");
        };

        $scope.CompletedEvent = function(scope) {
            console.log("Completed Event called");
        };

        $scope.ExitEvent = function(scope) {
            console.log("Exit Event called");
        };

        $scope.ChangeEvent = function(targetElement, scope) {
            console.log("Change Event called");
            console.log(targetElement); //The target element
            console.log(this); //The IntroJS object
        };

        $scope.BeforeChangeEvent = function(targetElement, scope) {
            console.log("Before Change Event called");
            console.log(targetElement);
        };

        $scope.AfterChangeEvent = function(targetElement, scope) {
            console.log("After Change Event called");
            console.log(targetElement);
        };

        $scope.IntroOptions = {
            steps: [{
                    element: document.querySelector('#step0'),
                    intro: 'Thank you for using this site, Please follow these steps to search the excursions!'
                },
                {
                    element: document.querySelector('#step1'),
                    intro: 'Click on a <strong> “Tariff Period” </strong> to select a <strong>“Searching Period”</strong>.'
                },
                {
                    element: document.querySelectorAll('#step2')[0],
                    intro: 'Click <strong>“SUBMIT”</strong> to list out all the excursions during the Tariff Period',
                    position: 'right'
                },
                {
                    element: document.querySelectorAll('#step3')[0],
                    intro: 'Select <strong>“Multi-Filters”</strong> to filter Locations, Tour Durations, or Tour Categories. Click <strong>“SUBMIT”</strong> again to apply',
                    position: 'right'
                },
                {
                    element: document.querySelectorAll('#step4')[0],
                    intro: 'Click <strong> “Export Excel” </strong> to extract the selected excursions',
                    position: 'left'
                }
            ],
            showStepNumbers: false,
            exitOnOverlayClick: true,
            exitOnEsc: true,
            nextLabel: '<strong>NEXT!</strong>',
            prevLabel: '<span style="color:green">Previous</span>',
            skipLabel: 'Exit',
            doneLabel: 'Thanks'
        };

        $scope.ShouldAutoStart = true;


        function loadslider() {
            if ($(".news-carousel").length && $.fn.slick) {
                $(".news-carousel").slick({
                    infinite: true,
                    slidesToShow: 1,
                    vertical: true,
                    autoplay: true,
                    //arrows: false,
                    autoplaySpeed: 5000,
                    slidesToScroll: 1,
                    dots: false
                        // arrows: false
                });
            }
        }

        function monthText(m) {
            var text = "";
            switch (m) {
                case 1:
                    text = "Jan";
                    break;
                case 2:
                    text = "Feb";
                    break;
                case 3:
                    text = "Mar";
                    break;
                case 4:
                    text = "Apr";
                    break;
                case 5:
                    text = "May";
                    break;
                case 6:
                    text = "June";
                    break;
                case 7:
                    text = "July";
                    break;
                case 8:
                    text = "Aug";
                    break;
                case 9:
                    text = "Sept ";
                    break;
                case 10:
                    text = "Oct";
                    break;
                case 11:
                    text = "Nov";
                    break;
                case 12:
                    text = "Dec";
                    break;
            }
            return text;
        }
    }
}());
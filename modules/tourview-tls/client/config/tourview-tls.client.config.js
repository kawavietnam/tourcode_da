(function () {
  'use strict';

  angular
    .module('tourview-tls')
    .run(menuConfig);

  menuConfig.$inject = ['menuService'];

  function menuConfig(menuService) {
    // Set top bar menu items
    menuService.addMenuItem('topbar', {
      title: 'Tourview tls',
      state: 'tourview-tls',
      type: 'dropdown',
      roles: ['*']
    });

    // Add the dropdown list item
    menuService.addSubMenuItem('topbar', 'tourview-tls', {
      title: 'List Tourview tls',
      state: 'tourview-tls.list'
    });

    // Add the dropdown create item
    menuService.addSubMenuItem('topbar', 'tourview-tls', {
      title: 'Create Tourview tl',
      state: 'tourview-tls.create',
      roles: ['user']
    });
  }
}());

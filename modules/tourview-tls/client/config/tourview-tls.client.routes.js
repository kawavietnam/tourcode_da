(function() {
    'use strict';

    angular
        .module('tourview-tls')
        .config(routeConfig);

    routeConfig.$inject = ['$stateProvider'];

    function routeConfig($stateProvider) {
        $stateProvider
            .state('tourview-tls', {
                abstract: true,
                url: '/tourview-tl',
                template: '<ui-view/>'
            })
            .state('tourview-tls.list', {
                url: '',
                templateUrl: 'modules/tourview-tls/client/views/list-tourview-tls.client.view.html',
                controller: 'TourviewTlsListController',
                controllerAs: 'vm',
                data: {
                    pageTitle: 'Tourview tls List'
                }
            })
            .state('tourview-tls.sevgroup', {
                url: '/servicegroup',
                templateUrl: '/modules/tourview-tls/client/views/sevgroup-tourview-tls.client.view.html',
                controller: 'TourviewTlsController',
                controllerAs: 'vm',
                data: {
                    pageTitle: 'tourview-tls service'
                }
            })
            .state('tourview-tls.sevcategory', {
                url: '/servicecategory',
                templateUrl: '/modules/tourview-tls/client/views/sevcategory-tourview-tls.client.view.html',
                controller: 'TourviewTlsController',
                controllerAs: 'vm',
                data: {
                    pageTitle: 'tourview-tls service'
                }
            })
            .state('tourview-tls.servicetype', {
                url: '/servicetype',
                templateUrl: '/modules/tourview-tls/client/views/servicetype-tourview-tls.client.view.html',
                controller: 'TourviewTlsController',
                controllerAs: 'vm',
                data: {
                    pageTitle: 'tourview-tls service'
                }
            })
            .state('tourview-tls.geotree', {
                url: '/geotree',
                templateUrl: '/modules/tourview-tls/client/views/geotree-tourview-tls.client.view.html',
                controller: 'TourviewTlsController',
                controllerAs: 'vm',
                data: {
                    pageTitle: 'tourview-tls service'
                }
            })
            .state('tourview-tls.exchangerate', {
                url: '/exchangerate',
                templateUrl: '/modules/tourview-tls/client/views/exchangerate-tourview-tls.client.view.html',
                controller: 'TourviewTlsController',
                controllerAs: 'vm',
                data: {
                    pageTitle: 'tourview-tls service'
                }
            })
            .state('tourview-tls.tariffperiod', {
                url: '/tariffperiod',
                templateUrl: '/modules/tourview-tls/client/views/tariffperiod-tourview-tls.client.view.html',
                controller: 'TourviewTlsController',
                controllerAs: 'vm',
                data: {
                    pageTitle: 'tourview-tls service'
                }
            })
            .state('tourview-tls.notify', {
                url: '/notify',
                templateUrl: '/modules/tourview-tls/client/views/notify-tourtariff.client.view.html',
                controller: 'TourviewTlsController',
                controllerAs: 'vm',
                data: {
                    pageTitle: 'tourview-tls service'
                }
            })
            .state('tourview-tls.supplier', {
                url: '/supplier',
                templateUrl: '/modules/tourview-tls/client/views/supplier-tourview-tls.client.view.html',
                controller: 'TourviewTlsController',
                controllerAs: 'vm',
                data: {
                    pageTitle: 'tourview-tls service'
                }
            })
            .state('tourview-tls.sorry', {
                url: '/sorry',
                templateUrl: '/modules/tourview-tls/client/views/sorrypage-tourtariff.client.view.html',
                controller: 'TourviewTlsController',
                controllerAs: 'vm',
                data: {
                    pageTitle: 'Tourtariffs service'
                }
            })
            .state('tourview-tls.priority', {
                url: '/priority',
                templateUrl: '/modules/tourview-tls/client/views/priority-tourview-tls.client.view.html',
                controller: 'TourviewTlsController',
                controllerAs: 'vm',
                data: {
                    pageTitle: 'tourview-tls service'
                }
            })
            .state('tourview-tls.users', {
                url: '/users',
                templateUrl: '/modules/tourview-tls/client/views/users-tourview-tls.client.view.html',
                controller: 'TourviewTlsController',
                controllerAs: 'vm',
                data: {
                    pageTitle: 'tourview-tls service'
                }
            })
            .state('tourview-tls.webinfo', {
                url: '/webinfo',
                templateUrl: '/modules/tourview-tls/client/views/webinfo-tourview-tls.client.view.html',
                controller: 'UsersController',
                controllerAs: 'vm',
                data: {
                    pageTitle: 'tourview-tls service'
                }
            })
            .state('tourview-tls.client', {
                url: '/client/:idCode',
                templateUrl: '/modules/tourview-tls/client/views/client-tourview-tls.client.view.html',
                controller: 'TourviewTlsController',
                controllerAs: 'vm',
                data: {
                    pageTitle: 'VN Tour Tariff'
                }
            })
            .state('tourview-tls.detail', {
                url: '/detail/:tourtariffId/:codeID',
                templateUrl: '/modules/tourview-tls/client/views/detailclient-tourview-tls.client.view.html',
                controller: 'TourviewTlsController',
                controllerAs: 'vm',
                data: {
                    pageTitle: 'tourview-tls service'
                }
            })

        ;
    }

    getTourviewTl.$inject = ['$stateParams', 'TourviewTlsService'];

    function getTourviewTl($stateParams, TourviewTlsService) {
        return TourviewTlsService.get({
            tourviewTlId: $stateParams.tourviewTlId
        }).$promise;
    }

    newTourviewTl.$inject = ['TourviewTlsService'];

    function newTourviewTl(TourviewTlsService) {
        return new TourviewTlsService();
    }
}());
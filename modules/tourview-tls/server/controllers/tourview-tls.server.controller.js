'use strict';

/**
 * Module dependencies.
 */
var path = require('path'),
    mongoose = require('mongoose'),
    Listimage = mongoose.model('TlsListimage'),
    SevGroup = mongoose.model('TlsSevGroup'),
    ServiceType = mongoose.model('TlsServiceType'),
    GeoTree = mongoose.model('TlsGeoTree'),
    Category = mongoose.model('TlsCategory'),
    ExchangeRate = mongoose.model('TlsExchangeRate'),
    TariffPeriod = mongoose.model('TlsTariffPeriod'),
    Supplier = mongoose.model('TlsSupplier'),
    Priority = mongoose.model('TlsPriority'),
    Notify = mongoose.model('TlsNotify'),
    Users = mongoose.model('TlsUsers'),
    WebInfo = mongoose.model('TlsWebInfo'),
    DataSave = mongoose.model('TlsDataSave'),
    GroupImage = mongoose.model('TlsGroupImage'),
    //ChildPolicy = mongoose.model('TlschildPolicy'),
    TourviewTl = mongoose.model('TourviewTl'),
    errorHandler = require(path.resolve('./modules/core/server/controllers/errors.server.controller')),
    _ = require('lodash');








exports.LoadDataSaveById = function(req, res) {
    DataSave.findOne({ _id: req.body._id })
        .then(function(data) {
            if (data) {
                res.jsonp(data);
            } else {
                res.json("null");
            }
        });
};



exports.createGroupImage = function(req, res) {
    var groupimage = new GroupImage(req.body);
    groupimage.user = req.user;

    groupimage.save(function(err) {
        if (err) {
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            res.jsonp(groupimage);
        }
    });
};
exports.LoadGroupImage = function(req, res) {
    GroupImage.find({})
        .then(function(data) {
            if (data) {
                res.jsonp(data);
            } else {
                res.json("null");
            }
        });
};

exports.updateGroupImage = function(req, res) {
    GroupImage.findOne({ '_id': req.body._id })
        .then(function(item) {
            item.name = req.body.name;
            item.save(function(err) {
                if (err) {
                    return res.status(400).send({
                        message: errorHandler.getErrorMessage(err)
                    });
                } else {
                    res.jsonp(item);
                }
            });
        });
};

exports.deleteGroupImage = function(req, res) {
    var groupimage = req.groupImage;

    GroupImage.findOne({ '_id': req.body._id })
        .then(function(item) {
            item.remove(function(err) {
                if (err) {
                    return res.status(400).send({
                        message: errorHandler.getErrorMessage(err)
                    });
                } else {
                    res.jsonp(groupimage);
                }
            });
        });
};

/**
 * Update a Tourtariff
 */
exports.update = function(req, res) {
    var tourtariff = req.tourtariff;

    tourtariff = _.extend(tourtariff, req.body)
    tourtariff.save(function(err) {
        if (err) {
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            res.jsonp(tourtariff);
        }
    });
};




exports.create = function(req, res) {
    var tourtariff = new Tourtariff(req.body);
    tourtariff.user = req.user;

    tourtariff.save(function(err) {
        if (err) {
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            res.jsonp(tourtariff);
        }
    });
};
/**
 * Show the current Tourtariff
 */
exports.read = function(req, res) {
    // convert mongoose document to JSON
    var tourtariff = req.tourtariff ? req.tourtariff.toJSON() : {};

    // Add a custom field to the Article, for determining if the current User is the "owner".
    // NOTE: This field is NOT persisted to the database, since it doesn't exist in the Article model.
    tourtariff.isCurrentUserOwner = req.user && tourtariff.user && tourtariff.user._id.toString() === req.user._id.toString();

    res.jsonp(tourtariff);
};

/**
 * Update a Tourtariff
 */
exports.update = function(req, res) {
    var tourtariff = req.tourtariff;

    tourtariff = _.extend(tourtariff, req.body)
    tourtariff.save(function(err) {
        if (err) {
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            res.jsonp(tourtariff);
        }
    });
};

/**
 * Delete an Tourtariff
 */
exports.delete = function(req, res) {
    var tourtariff = req.tourtariff;

    tourtariff.remove(function(err) {
        if (err) {
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            res.jsonp(tourtariff);
        }
    });
};

/**
 * List of Tourtariffs
 */
exports.list = function(req, res) {
    Tourtariff.find().sort('-created').populate('user', 'displayName').exec(function(err, tourtariffs) {
        if (err) {
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            res.jsonp(tourtariffs);
        }
    });
};

/**
 * Tourtariff middleware
 */
exports.tourtariffByID = function(req, res, next, id) {

    if (!mongoose.Types.ObjectId.isValid(id)) {
        return res.status(400).send({
            message: 'Tourtariff is invalid'
        });
    }

    Tourtariff.findById(id).populate('user', 'displayName').exec(function(err, tourtariff) {
        if (err) {
            return next(err);
        } else if (!tourtariff) {
            return res.status(404).send({
                message: 'No Tourtariff with that identifier has been found'
            });
        }
        req.tourtariff = tourtariff;
        next();
    });
};

exports.updateImage = function(req, res) {
    var path = "public/image/"
    var user = req.user;
    var fs = require("fs");
    var buf = new Buffer(req.body.Image, 'base64');
    fs.writeFile(path + req.body.NameImage, buf, "binary", function(err) {
        if (err) {} else {
            var data = {
                nameImage: req.body.NameImage,
                urlImage: '/image/' + req.body.NameImage,
                urlThumbnail: '/image/' + req.body.NameImage,
                group: req.body.group
            }
            var listimage = new Listimage(data);
            listimage.user = req.user;
            listimage.save(function(err) {
                if (err) {
                    return res.status(400).send({
                        message: errorHandler.getErrorMessage(err)
                    });
                } else {
                    res.jsonp(listimage);
                }
            });
        }
    });
}

exports.updateImageGroup = function(req, res) {
    Listimage.findOne({ '_id': req.body._id })
        .then(function(item) {
            item.group = req.body.group;
            item.save(function(err) {
                if (err) {
                    return res.status(400).send({
                        message: errorHandler.getErrorMessage(err)
                    });
                } else {
                    res.jsonp(item);
                }
            });
        });
};
exports.LoadListImage = function(req, res) {

    Listimage.find({ 'group': undefined })
        .then(function(image) {
            if (image) {
                var data = [];
                for (var i = 0; i < image.length; i++) {
                    var obj = {
                        _id: image[i]._id,
                        image: image[i].urlImage,
                        thumb: image[i].urlThumbnail,
                        group: image[i].group,
                        name: image[i].nameImage
                    }
                    data.push(obj)
                }
                res.jsonp(data);
            } else {
                res.json("null");
            }
        })
};

exports.LoadImageToContent = function(req, res) {
    Listimage.find()
        .then(function(image) {
            if (image) {
                var data = [];
                for (var i = 0; i < image.length; i++) {
                    var obj = {
                        _id: image[i]._id,
                        image: image[i].urlImage,
                        thumb: image[i].urlThumbnail,
                        folder: image[i].group,
                        name: image[i].nameImage
                    }
                    data.push(obj)
                }
                data = data.sort(function(a, b) {
                    if (a.folder < b.folder)
                        return -1;
                    if (a.folder > b.folder)
                        return 1;
                    return 0;
                });
                res.jsonp(data);
            } else {
                res.json("null");
            }
        })
};


exports.DeleteImage = function(req, res) {
    Listimage.findOne({ '_id': req.body._id })
        .then(function(item) {
            var listimage = item;

            listimage.remove(function(err) {
                if (err) {
                    return res.status(400).send({
                        message: errorHandler.getErrorMessage(err)
                    });
                } else {
                    res.json(true);
                }
            });
        });
};

exports.GeTListBygroup = function(req, res) {
    Listimage.find(req.body)
        .then(function(image) {
            if (image) {
                var data = [];
                for (var i = 0; i < image.length; i++) {
                    var obj = {
                        _id: image[i]._id,
                        image: image[i].urlImage,
                        thumb: image[i].urlThumbnail,
                        group: image[i].group,
                        name: image[i].nameImage
                    }
                    data.push(obj)
                }
                res.jsonp(data);
            } else {
                res.json("null");
            }
        })
};


//LoadService
exports.LoadService = function(req, res) {
    var name = {};
    if (req.body.name === 'SevGroup')
        name = { 'group.price': { $gt: 0 } };
    else if (req.body.name === 'SevAccumulated')
        name = { 'accumulated.price': { $gt: 0 }, 'accumulated.min': { $gt: 0 }, 'accumulated.max': { $gt: 1 } };
    else if (req.body.name === 'SevPriceBand')
        name = { 'priceband.list.0': { $exists: true } };

    SevGroup.find(name)
        .sort({ location: 1 })
        .sort({ types: 1 })
        .then(function(data) {
            if (data) {
                res.jsonp(data);
            } else {
                res.json("null");
            }
        });
};


//SevGroup

exports.LoadSevGroup = function(req, res) {
    SevGroup.find()
        .sort({ location: 1 })
        .sort({ types: 1 })
        .then(function(data) {
            if (data) {
                res.jsonp(data);
            } else {
                res.json("null");
            }
        });
};

exports.createSevGroup = function(req, res) {
    var sevgroup = new SevGroup(req.body);
    sevgroup.user = req.user;
    sevgroup.save(function(err) {
        if (err) {
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            res.jsonp(sevgroup);
        }
    });
};

exports.updateSevGroup = function(req, res) {
    SevGroup.findOne({ '_id': req.body._id })
        .then(function(item) {
            item.name = req.body.name;
            item.supplierTourCode = req.body.supplierTourCode;
            item.beginDate = req.body.beginDate;
            item.endDate = req.body.endDate;
            item.location = req.body.location;
            item.supplier = req.body.supplier;
            item.types = req.body.types;
            item.address = req.body.address;
            item.tel = req.body.tel;
            item.email = req.body.email;
            item.url = req.body.url;
            item.group = req.body.group;
            item.priceband = req.body.priceband;
            item.accumulated = req.body.accumulated;
            item.save(function(err) {
                if (err) {
                    return res.status(400).send({
                        message: errorHandler.getErrorMessage(err)
                    });
                } else {
                    res.jsonp(item);
                }
            });
        });
};

exports.deleteSevGroup = function(req, res) {
    var sevgroup = req.sevgroup;

    SevGroup.findOne({ '_id': req.body._id })
        .then(function(item) {
            item.remove(function(err) {
                if (err) {
                    return res.status(400).send({
                        message: errorHandler.getErrorMessage(err)
                    });
                } else {
                    res.jsonp(sevgroup);
                }
            });
        });
};

// category

exports.createCategory = function(req, res) {
    var category = new Category(req.body);
    category.user = req.user;

    category.save(function(err) {
        if (err) {
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            res.jsonp(category);
        }
    });
};

exports.loadCategory = function(req, res) {
    Category.find({})
        .sort({ name: 1 })
        .then(function(service) {
            if (service) {
                res.jsonp(service);
            } else {
                res.json("null");
            }
        })
};

exports.updateCategory = function(req, res) {
    Category.findOne({ '_id': req.body._id })
        .then(function(item) {
            item.name = req.body.name;
            item.code = req.body.code;
            item.save(function(err) {
                if (err) {
                    return res.status(400).send({
                        message: errorHandler.getErrorMessage(err)
                    });
                } else {
                    res.jsonp(item);
                }
            });
        });
};

exports.deleteCategory = function(req, res) {
    var category = req.category;
    Category.findOne({ '_id': req.body._id })
        .then(function(item) {
            item.remove(function(err) {
                if (err) {
                    return res.status(400).send({
                        message: errorHandler.getErrorMessage(err)
                    });
                } else {
                    res.jsonp(category);
                }
            });
        });
};

// servicetype

exports.createServiceType = function(req, res) {
    var servicetype = new ServiceType(req.body);
    servicetype.user = req.user;

    servicetype.save(function(err) {
        if (err) {
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            res.jsonp(servicetype);
        }
    });
};

exports.loadServiceType = function(req, res) {
    ServiceType.find({})
        .sort({ name: 1 })
        .then(function(servicetype) {
            if (servicetype) {
                res.jsonp(servicetype);
            } else {
                res.json("null");
            }
        })
};

exports.updateServiceType = function(req, res) {
    ServiceType.findOne({ '_id': req.body._id })
        .then(function(item) {
            item.name = req.body.name;
            item.code = req.body.code;
            item.save(function(err) {
                if (err) {
                    return res.status(400).send({
                        message: errorHandler.getErrorMessage(err)
                    });
                } else {
                    res.jsonp(item);
                }
            });
        });
};

exports.deleteServiceType = function(req, res) {
    var servicetype = req.servicetype;
    ServiceType.findOne({ '_id': req.body._id })
        .then(function(item) {
            item.remove(function(err) {
                if (err) {
                    return res.status(400).send({
                        message: errorHandler.getErrorMessage(err)
                    });
                } else {
                    res.jsonp(servicetype);
                }
            });
        });
};


// geotree
exports.createGeoTree = function(req, res) {
    var geotree = new GeoTree(req.body);
    geotree.user = req.user;

    geotree.save(function(err) {
        if (err) {
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            res.jsonp(geotree);
        }
    });
};

exports.loadGeoTree = function(req, res) {
    GeoTree.find({})
        .sort({ name: 1 })
        .then(function(geotree) {
            if (geotree) {
                res.jsonp(geotree);
            } else {
                res.json("null");
            }
        })
};

exports.updateGeoTree = function(req, res) {
    GeoTree.findOne({ '_id': req.body._id })
        .then(function(item) {
            item.name = req.body.name;
            item.zone = req.body.zone;
            item.save(function(err) {
                if (err) {
                    return res.status(400).send({
                        message: errorHandler.getErrorMessage(err)
                    });
                } else {
                    res.jsonp(item);
                }
            });
        });
};

exports.deleteGeoTree = function(req, res) {
    var geotree = req.geotree;
    GeoTree.findOne({ '_id': req.body._id })
        .then(function(item) {
            item.remove(function(err) {
                if (err) {
                    return res.status(400).send({
                        message: errorHandler.getErrorMessage(err)
                    });
                } else {
                    res.jsonp(geotree);
                }
            });
        });
};

// exchangerate
exports.createExchangeRate = function(req, res) {
    var exchangerate = new ExchangeRate(req.body);
    exchangerate.user = req.user;

    exchangerate.save(function(err) {
        if (err) {
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            res.jsonp(exchangerate);
        }
    });
};

exports.loadExchangeRate = function(req, res) {
    ExchangeRate.find({})
        .sort({ beginDate: 1 })
        .then(function(exchangerate) {
            if (exchangerate) {
                res.jsonp(exchangerate);
            } else {
                res.json("null");
            }
        })
};

exports.updateExchangeRate = function(req, res) {
    ExchangeRate.findOne({ '_id': req.body._id })
        .then(function(item) {
            item.beginDate = req.body.beginDate;
            item.endDate = req.body.endDate;
            item.VND = req.body.VND;
            item.USD = req.body.USD;
            item.save(function(err) {
                if (err) {
                    return res.status(400).send({
                        message: errorHandler.getErrorMessage(err)
                    });
                } else {
                    res.jsonp(item);
                }
            });
        });
};

exports.deleteExchangeRate = function(req, res) {
    var exchangerate = req.exchangerate;
    ExchangeRate.findOne({ '_id': req.body._id })
        .then(function(item) {
            item.remove(function(err) {
                if (err) {
                    return res.status(400).send({
                        message: errorHandler.getErrorMessage(err)
                    });
                } else {
                    res.jsonp(exchangerate);
                }
            });
        });
};
// TariffPeriod
exports.createTariffPeriod = function(req, res) {
    var tariffperiod = new TariffPeriod(req.body);
    tariffperiod.user = req.user;

    tariffperiod.save(function(err) {
        if (err) {
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            res.jsonp(tariffperiod);
        }
    });
};

exports.loadTariffPeriod = function(req, res) {
    TariffPeriod.find({})
        .sort({ date: 1 })
        .then(function(TariffPeriod) {
            if (TariffPeriod) {
                res.jsonp(TariffPeriod);
            } else {
                res.json("null");
            }
        });
};

exports.updateTariffPeriod = function(req, res) {
    TariffPeriod.findOne({ '_id': req.body._id })
        .then(function(item) {
            item.date = req.body.date;
            item.name = req.body.name;
            item.no = req.body.no;
            item.save(function(err) {
                if (err) {
                    return res.status(400).send({
                        message: errorHandler.getErrorMessage(err)
                    });
                } else {
                    res.jsonp(item);
                }
            });
        });
};

exports.deleteTariffPeriod = function(req, res) {
    var tariffperiod = req.tariffperiod;
    console.log(req.body);
    TariffPeriod.findOne({ '_id': req.body._id })
        .then(function(item) {
            item.remove(function(err) {
                if (err) {
                    return res.status(400).send({
                        message: errorHandler.getErrorMessage(err)
                    });
                } else {
                    res.jsonp(tariffperiod);
                }
            });
        });
};

// Supplier
exports.createSupplier = function(req, res) {
    var supplier = new Supplier(req.body);
    supplier.user = req.user;

    supplier.save(function(err) {
        if (err) {
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            res.jsonp(supplier);
        }
    });
};

exports.loadSupplier = function(req, res) {
    Supplier.find({})
        .sort({ name: 1 })
        .then(function(service) {
            if (service) {
                res.jsonp(service);
            } else {
                res.json("null");
            }
        })
};

exports.updateSupplier = function(req, res) {
    Supplier.findOne({ '_id': req.body._id })
        .then(function(item) {
            item.name = req.body.name;
            item.address = req.body.address;
            item.phone = req.body.phone;
            item.email = req.body.email;
            item.save(function(err) {
                if (err) {
                    return res.status(400).send({
                        message: errorHandler.getErrorMessage(err)
                    });
                } else {
                    res.jsonp(item);
                }
            });
        });
};

exports.deleteSupplier = function(req, res) {
    var supplier = req.supplier;
    Supplier.findOne({ '_id': req.body._id })
        .then(function(item) {
            item.remove(function(err) {
                if (err) {
                    return res.status(400).send({
                        message: errorHandler.getErrorMessage(err)
                    });
                } else {
                    res.jsonp(supplier);
                }
            });
        });
};


// Priority
exports.createPriority = function(req, res) {
    var priority = new Priority(req.body);
    priority.user = req.user;

    priority.save(function(err) {
        if (err) {
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            res.jsonp(priority);
        }
    });
};

exports.loadPriority = function(req, res) {
    Priority.find({})
        .sort({ name: 1 })
        .then(function(service) {
            if (service) {
                res.jsonp(service);
            } else {
                res.json("null");
            }
        })
};

exports.updatePriority = function(req, res) {
    Priority.findOne({ '_id': req.body._id })
        .then(function(item) {
            item.name = req.body.name;
            item.code = req.body.code;
            item.save(function(err) {
                if (err) {
                    return res.status(400).send({
                        message: errorHandler.getErrorMessage(err)
                    });
                } else {
                    res.jsonp(item);
                }
            });
        });
};

exports.deletePriority = function(req, res) {
    var priority = req.priority;
    Priority.findOne({ '_id': req.body._id })
        .then(function(item) {
            item.remove(function(err) {
                if (err) {
                    return res.status(400).send({
                        message: errorHandler.getErrorMessage(err)
                    });
                } else {
                    res.jsonp(priority);
                }
            });
        });
};

// Notify
exports.createNotify = function(req, res) {

    var notify = new Notify(req.body);
    notify.user = req.user;

    notify.save(function(err) {
        if (err) {
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            res.jsonp(notify);
        }
    });
};

exports.loadNotify = function(req, res) {
    Notify.find({})
        .sort({ name: 1 })
        .then(function(service) {
            if (service) {
                res.jsonp(service);
            } else {
                res.json("null");
            }
        })
};
exports.loadNotifyClient = function(req, res) {
    Notify.find({ 'isActive': true })
        .sort({ name: 1 })
        .then(function(service) {
            if (service) {
                res.jsonp(service);
            } else {
                res.json("null");
            }
        })
};

exports.updateNotify = function(req, res) {
    Notify.findOne({ '_id': req.body._id })
        .then(function(item) {
            item.text = req.body.text;
            item.isActive = req.body.isActive;
            item.save(function(err) {
                if (err) {
                    return res.status(400).send({
                        message: errorHandler.getErrorMessage(err)
                    });
                } else {
                    res.jsonp(item);
                }
            });
        });
};

exports.deleteNotify = function(req, res) {
    var notify = req.notify;
    Notify.findOne({ '_id': req.body._id })
        .then(function(item) {
            item.remove(function(err) {
                if (err) {
                    return res.status(400).send({
                        message: errorHandler.getErrorMessage(err)
                    });
                } else {
                    res.jsonp(notify);
                }
            });
        });
};

// User
exports.loginUsers = function(req, res) {
    Users.find(req.body)
        .then(function(service) {
            if (service) {
                res.jsonp(service);
            } else {
                res.json("null");
            }
        })
};

exports.createUsers = function(req, res) {
    var users = new Users(req.body);
    users.user = req.user;

    users.save(function(err) {
        if (err) {
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            res.jsonp(users);
        }
    });
};

exports.loadUsers = function(req, res) {
    Users.find({})
        .sort({ name: 1 })
        .then(function(service) {
            if (service) {
                res.jsonp(service);
            } else {
                res.json("null");
            }
        })
};

exports.updateUsers = function(req, res) {
    Users.findOne({ '_id': req.body._id })
        .then(function(item) {
            item.fullname = req.body.fullname;
            item.email = req.body.email;
            item.company = req.body.company;
            item.username = req.body.username;
            item.pass = req.body.pass;
            item.role = req.body.role;
            item.active = req.body.active;
            item.save(function(err) {
                if (err) {
                    return res.status(400).send({
                        message: errorHandler.getErrorMessage(err)
                    });
                } else {
                    res.jsonp(item);
                }
            });
        });
};
exports.updateProfileUsers = function(req, res) {
    Users.findOne({ $or: [{ 'username': req.body.username }, { email: req.body.email }] })
        .then(function(item) {
            if (item && item._id != req.body._id) {
                res.jsonp("Fail! Login name or email belong to other user.");
            } else {
                Users.findOne({ '_id': req.body._id })
                    .then(function(item) {
                        item.fullname = req.body.fullname;
                        item.email = req.body.email;
                        item.company = req.body.company;
                        item.username = req.body.username;
                        item.save(function(err) {
                            if (err) {
                                return res.status(400).send({
                                    message: errorHandler.getErrorMessage(err)
                                });
                            } else {
                                res.jsonp("Update success!");
                            }
                        });
                    });
            }
        });
};

exports.updatePassUsers = function(req, res) {
    Users.findOne({ '_id': req.body._id })
        .then(function(item) {
            item.pass = req.body.newpass;
            item.save(function(err) {
                if (err) {
                    return res.status(400).send({
                        message: errorHandler.getErrorMessage(err)
                    });
                } else {
                    res.jsonp(item);
                }
            });
        });
};

exports.updateImgProfile = function(req, res) {
    var path = "public/image/users/"
    var user = req.user;
    var fs = require("fs");
    var buf = new Buffer(req.body.Image, 'base64');
    fs.writeFile(path + req.body.NameImage, buf, "binary", function(err) {
        if (err) {

        } else {
            Users.findOne({ '_id': req.body._id })
                .then(function(item) {
                    item.nameImg = req.body.NameImage;
                    item.save(function(err) {
                        if (err) {
                            return res.status(400).send({
                                message: errorHandler.getErrorMessage(err)
                            });
                        } else {
                            res.jsonp("Success!");
                        }
                    });
                });
        }
    });
}


exports.deleteUsers = function(req, res) {
    var users = req.users;
    Users.findOne({ '_id': req.body._id })
        .then(function(item) {
            item.remove(function(err) {
                if (err) {
                    return res.status(400).send({
                        message: errorHandler.getErrorMessage(err)
                    });
                } else {
                    res.jsonp(users);
                }
            });
        });
};

// WebInfo

exports.UpdateImgWebBanner = function(req, res) {
    var path = "public/image/"
    var user = req.user;
    var fs = require("fs");
    var buf = new Buffer(req.body.Image, 'base64');
    fs.writeFile(path + req.body.NameImage, buf, "binary", function(err) {
        if (err) {

        } else {
            WebInfo.findOne({ '_id': req.body._id })
                .then(function(item) {
                    item.webBanner = req.body.NameImage;
                    item.datewebBanner = (new Date()).getTime();
                    item.save(function(err) {
                        if (err) {
                            return res.status(400).send({
                                message: errorHandler.getErrorMessage(err)
                            });
                        } else {
                            res.jsonp("Success!");
                        }
                    });
                });
        }
    });
}

exports.AddImgSlide = function(req, res) {
    var path = "public/image/slider/"
    var user = req.user;
    var fs = require("fs");
    var buf = new Buffer(req.body.Image, 'base64');
    fs.writeFile(path + req.body.NameImage, buf, "binary", function(err) {
        if (err) {

        } else {
            WebInfo.findOne({ '_id': req.body._id })
                .then(function(item) {
                    item.listSlide.push({ name: req.body.NameImage });
                    item.save(function(err) {
                        if (err) {
                            return res.status(400).send({
                                message: errorHandler.getErrorMessage(err)
                            });
                        } else {
                            res.jsonp("Success!");
                        }
                    });
                });
        }
    });
}

exports.DeleteImgSlide = function(req, res) {
    WebInfo.findOne({ '_id': req.body._id })
        .then(function(item) {
            item.listSlide = req.body.listSlide;
            item.save(function(err) {
                if (err) {
                    return res.status(400).send({
                        message: errorHandler.getErrorMessage(err)
                    });
                } else {
                    res.jsonp(item);
                }
            });
        });
};

exports.UpdateTern = function(req, res) {
    WebInfo.findOne({ '_id': req.body._id })
        .then(function(item) {
            item.term = req.body.term;
            item.save(function(err) {
                if (err) {
                    return res.status(400).send({
                        message: errorHandler.getErrorMessage(err)
                    });
                } else {
                    res.jsonp(item);
                }
            });
        });
};
exports.UpdateCoverPage = function(req, res) {
    WebInfo.findOne({ '_id': req.body._id })
        .then(function(item) {
            item.coverpage = req.body.coverpage;
            item.save(function(err) {
                if (err) {
                    return res.status(400).send({
                        message: errorHandler.getErrorMessage(err)
                    });
                } else {
                    res.jsonp(item);
                }
            });
        });
};
exports.createWebInfo = function(req, res) {
    var webinfo = new WebInfo(req.body);
    webinfo.user = req.user;
    webinfo.save(function(err) {
        if (err) {
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            res.jsonp(webinfo);
        }
    });
};

exports.loadWebInfo = function(req, res) {
    WebInfo.findOne({})
        .sort({ name: 1 })
        .then(function(webinfo) {
            if (webinfo) {
                res.jsonp(webinfo);
            } else {
                res.json("null");
            }
        })
};

exports.updateWebInfo = function(req, res) {
    WebInfo.findOne({ '_id': req.body._id })
        .then(function(item) {
            item.expBanner = req.body.expBanner;
            item.webBanner = req.body.webBanner;
            item.listSlide = req.body.listSlide;
            item.term = req.body.term;
            item.save(function(err) {
                if (err) {
                    return res.status(400).send({
                        message: errorHandler.getErrorMessage(err)
                    });
                } else {
                    res.jsonp(item);
                }
            });
        });
};



//Datasave

exports.createDataSave = function(req, res) {
    var dataSave = new DataSave(req.body);
    dataSave.user = req.user;
    dataSave.save(function(err) {
        if (err) {
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            res.jsonp(dataSave);
        }
    });
};
exports.updateDataSave = function(req, res) {

    DataSave.findOne({ '_id': req.body._id })
        .then(function(item) {
            item.tiers = req.body.tiers;
            item.code = req.body.code;
            item.showGroupCode = req.body.showGroupCode;
            item.Groupcode = req.body.Groupcode;
            item.noteTiers = req.body.noteTiers;
            item.isActive = req.body.isActive;
            item.excursionName = req.body.excursionName;
            item.category = req.body.category;
            item.location = req.body.location;
            item.market = req.body.market;
            item.currency = req.body.currency;
            item.priority = req.body.priority;
            item.beginDate = req.body.beginDate;
            item.endDate = req.body.endDate;
            item.DirectMarkup = req.body.DirectMarkup;
            item.lsGroups = req.body.lsGroups;
            item.lsVehicle = req.body.lsVehicle;

            item.listMarketExcept = req.body.listMarketExcept;
            item.listAgentExcept = req.body.listAgentExcept;

            item.lsChildPolicy = req.body.lsChildPolicy;
            item.lsAccumulated = req.body.lsAccumulated;
            item.lsAccommodation = req.body.lsAccommodation;
            item.lsExtra = req.body.lsExtra;
            item.lsTiming = req.body.lsTiming;
            item.maxTiming = req.body.maxTiming;
            item.content = req.body.content;
            item.slideImages = req.body.slideImages;
            item.startTime = req.body.startTime;
            item.updateTime = req.body.updateTime;
            item.createTime = req.body.createTime;
            item.updatePer = req.body.updatePer;
            item.createPer = req.body.createPer;
            item.availab = req.body.availab;
            item.note = req.body.note;
            item.CRM = req.body.CRM;
            item.DirectSetup = req.body.DirectSetup;
            item.lsDirectModel = req.body.lsDirectModel;
            item.checkDirectModel = req.body.checkDirectModel;
            item.noteDirectModel = req.body.noteDirectModel;
            item.ValueEditModel = req.body.ValueEditModel;
            item.agname = req.body.agname;
            item.md5code = req.body.md5code;
            item.ObjectAgent = req.body.ObjectAgent;
            item.Proposalstatus = req.body.Proposalstatus;
            item.LandingMK = req.body.LandingMK;
            item.HotelMkALL = req.body.HotelMkALL;
            item.save(function(err) {
                if (err) {
                    return res.status(400).send({
                        message: errorHandler.getErrorMessage(err)
                    });
                } else {
                    res.jsonp(item);
                }
            });
        });
};

exports.LoadDataSave = function(req, res) {
    DataSave.find({ isActive: true })
        .sort({ location: 1 })
        .sort({ category: 1 })
        .then(function(dataSave) {
            if (dataSave) {
                res.jsonp(dataSave);

            } else {
                res.json("null");
            }
        })
};
exports.FilterDataSave = function(req, res) {
    var _pageNumber = 2,
        _pageSize = 5;
    DataSave.count(req.body, function(err, count) {
        DataSave.find(req.body)
            .sort({ location: 1 })
            .sort({ category: 1 })
            .skip(_pageNumber > 0 ? ((_pageNumber - 1) * _pageSize) : 0).limit(_pageSize).exec(function(err, dataSave) {
                if (dataSave) {
                    res.jsonp(dataSave);
                } else {
                    res.json("null");
                }
            });
    });
};

exports.LoadDataSaveExcel = function(req, res) {
    var markup = req.body.markup;
    var lsExchangeRate = req.body.lsExchangeRate;
    var re = new RegExp(req.body.search.toLowerCase(), 'i');
    DataSave.count(req.body.param)
        .or([{ 'code': { $regex: re } },
            { 'excursionName': { $regex: re } },
            { 'location': { $regex: re } },
            { 'category': { $regex: re } },
            { 'priority': { $regex: re } }
        ])
        .exec(function(err, count) {
            DataSave.find(req.body.param)
                .sort({ location: 1 })
                .sort({ category: 1 })
                .sort({ code: 1 })
                .or([{ 'code': { $regex: re } },
                    { 'excursionName': { $regex: re } },
                    { 'location': { $regex: re } },
                    { 'category': { $regex: re } },
                    { 'priority': { $regex: re } }
                ]).exec(function(err, showDataClient) {
                    if (showDataClient) {
                        var ctourpro = markup.cpcode == "1" ? 0 : parseInt(markup.ctourpro);
                        var showDataClientTemp = [];

                        function ClearLandingMkforPackage(price, LandingMK) {
                            return Math.round(price - (price * LandingMK / 100))
                        }
                        showDataClient.forEach(function(vl) {
                            var VND = 1,
                                USD = 1;
                            var middle = new Date(vl.beginDate).getTime();
                            for (var i = 0; i < lsExchangeRate.length; i++) {
                                var begin = new Date(lsExchangeRate[i].beginDate.replace("T17:00:00.000Z", "")).getTime();
                                var end = new Date(lsExchangeRate[i].endDate.replace("T17:00:00.000Z", "")).getTime();
                                if (begin <= middle && middle <= end) {
                                    VND = lsExchangeRate[i].VND;
                                    USD = lsExchangeRate[i].USD;
                                    break;
                                }
                            }
                            vl.tiers.forEach(function(value) {
                                var priceMarkup = 0;
                                if (vl.ValueEditModel == "package" && vl.lsDirectModel.length == 0) {
                                    priceMarkup = vl.CRM == true ? Math.round(value.price + value.price * parseInt(ctourpro || 0) / 100) : Math.round(value.price);
                                    if (markup.cpcode == '1') {
                                        priceMarkup = ClearLandingMkforPackage(priceMarkup, vl.LandingMK)
                                    }
                                } else if (vl.ValueEditModel == "proposal" && vl.lsDirectModel.length > 0)
                                    priceMarkup = vl.CRM == true ? Math.round(value.price + value.price * parseInt(ctourpro || 0) / 100) : Math.ceil(value.price);
                                else
                                    priceMarkup = Math.round(value.price + value.price * parseInt(ctourpro || 0) / 100);
                                var currency = vl.currency;
                                var parentCurrency = markup.ccurrency
                                if (currency == parentCurrency)
                                    value.price = priceMarkup;
                                else if (currency == "USD" && parentCurrency == "THB") {
                                    value.price = Math.round(priceMarkup * VND / USD);
                                } else if (currency == "THB" && parentCurrency == "USD") {
                                    value.price = Math.round(priceMarkup * USD / VND);
                                }
                            });
                            if (vl.lsDirectModel.length > 0) {
                                vl.lsDirectModel.forEach(function(value) {
                                    value.tiers.forEach(function(ti) {
                                        var priceMarkup = vl.CRM == true ? Math.round(ti.price + ti.price * parseInt(ctourpro || 0) / 100) : ti.price;
                                        if (markup.cpcode == '1') {
                                            priceMarkup = ClearLandingMkforPackage(priceMarkup, vl.HotelMkALL || (value.directMkModel || 0))
                                        }
                                        var currency = vl.currency;
                                        var parentCurrency = markup.ccurrency
                                        if (currency == parentCurrency)
                                            ti.price = Math.round(priceMarkup);
                                        else if (currency == "USD" && parentCurrency == "THB") {
                                            ti.price = Math.round(priceMarkup * VND / USD);
                                        } else if (currency == "THB" && parentCurrency == "USD") {
                                            ti.price = Math.round(priceMarkup * USD / VND);
                                        }
                                    });
                                });
                            }


                            vl.currency = markup.ccurrency;
                            var its = false;
                            for (var i = 0; i < vl.market.length; i++) {
                                if (vl.market[i].vlMarket == markup.cmarket || vl.market[i].vlMarket === "WW") {
                                    its = true;
                                    break;
                                }
                            }

                            if (its == true) {
                                if (vl.Groupcode == undefined) {
                                    showDataClientTemp.push(vl);
                                } else if (vl.Groupcode === vl.code) {
                                    showDataClientTemp.push(vl);
                                } else {
                                    count--;
                                }
                            }

                        });
                        var data = {
                            showDataClient: showDataClientTemp,
                            count: count
                        };
                        res.jsonp(data);

                    } else {
                        res.json("null");
                    }
                });
        });
};

exports.LoadDataSaveByMarkup = function(req, res) {
    var markup = req.body.markup;
    var lsExchangeRate = req.body.lsExchangeRate;
    var re = new RegExp(req.body.search.toLowerCase(), 'i');
    var _pageNumber = req.body._pageNumber,
        _pageSize = 20;
    DataSave.count(req.body.param)
        .or([{ 'code': { $regex: re } },
            { 'excursionName': { $regex: re } },
            { 'location': { $regex: re } },
            { 'category': { $regex: re } },
            { 'priority': { $regex: re } }
        ])
        .exec(function(err, count) {
            DataSave.find(req.body.param)
                .sort({ location: 1 })
                .sort({ category: 1 })
                .sort({ code: 1 })
                .or([{ 'code': { $regex: re } },
                    { 'excursionName': { $regex: re } },
                    { 'location': { $regex: re } },
                    { 'category': { $regex: re } },
                    { 'priority': { $regex: re } }
                ])
                .skip(_pageNumber > 0 ? ((_pageNumber - 1) * _pageSize) : 0).limit(_pageSize).exec(function(err, showDataClient) {
                    if (showDataClient) {
                        var ctourpro = markup.cpcode == "1" ? 0 : parseInt(markup.ctourpro);
                        var showDataClientTemp = [];

                        function ClearLandingMkforPackage(price, LandingMK) {
                            return Math.round(price - (price * LandingMK / 100))
                        }
                        showDataClient.forEach(function(vl) {
                            var VND = 1,
                                USD = 1;
                            var middle = new Date(vl.beginDate).getTime();
                            for (var i = 0; i < lsExchangeRate.length; i++) {
                                var begin = new Date(lsExchangeRate[i].beginDate.replace("T17:00:00.000Z", "")).getTime();
                                var end = new Date(lsExchangeRate[i].endDate.replace("T17:00:00.000Z", "")).getTime();
                                if (begin <= middle && middle <= end) {
                                    VND = lsExchangeRate[i].VND;
                                    USD = lsExchangeRate[i].USD;
                                    break;
                                }
                            }
                            vl.tiers.forEach(function(value) {
                                var priceMarkup = 0;
                                if (vl.ValueEditModel == "package" && vl.lsDirectModel.length == 0) {
                                    priceMarkup = vl.CRM == true ? Math.round(value.price + value.price * parseInt(ctourpro || 0) / 100) : Math.round(value.price);
                                    if (markup.cpcode == '1') {
                                        priceMarkup = ClearLandingMkforPackage(priceMarkup, vl.LandingMK)
                                    }
                                } else if (vl.ValueEditModel == "proposal" && vl.lsDirectModel.length > 0)
                                    priceMarkup = vl.CRM == true ? Math.round(value.price + value.price * parseInt(ctourpro || 0) / 100) : Math.round(value.price);
                                else
                                    priceMarkup = Math.round(value.price + value.price * parseInt(ctourpro || 0) / 100);
                                var currency = vl.currency;
                                var parentCurrency = markup.ccurrency
                                if (currency == parentCurrency)
                                    value.price = priceMarkup;
                                else if (currency == "USD" && parentCurrency == "THB") {
                                    value.price = Math.round(priceMarkup * VND / USD);
                                } else if (currency == "THB" && parentCurrency == "USD") {
                                    value.price = Math.round(priceMarkup * USD / VND);
                                }
                            });
                            if (vl.lsDirectModel.length > 0) {
                                vl.lsDirectModel.forEach(function(value) {
                                    value.tiers.forEach(function(ti) {
                                        var priceMarkup = vl.CRM == true ? Math.round(ti.price + ti.price * parseInt(ctourpro || 0) / 100) : ti.price;
                                        if (markup.cpcode == '1') {
                                            priceMarkup = ClearLandingMkforPackage(priceMarkup, vl.HotelMkALL || (value.directMkModel || 0))
                                        }
                                        var currency = vl.currency;
                                        var parentCurrency = markup.ccurrency
                                        if (currency == parentCurrency)
                                            ti.price = Math.round(priceMarkup);
                                        else if (currency == "USD" && parentCurrency == "THB") {
                                            ti.price = Math.round(priceMarkup * VND / USD);
                                        } else if (currency == "THB" && parentCurrency == "USD") {
                                            ti.price = Math.round(priceMarkup * USD / VND);
                                        }
                                    });
                                });
                            }


                            vl.currency = markup.ccurrency;
                            var its = false;
                            var itsMarketExcept = false;
                            var itsAgentExcept = false;
                            for (var i = 0; i < vl.market.length; i++) {
                                if (vl.market[i].vlMarket == markup.cmarket || vl.market[i].vlMarket === "WW") {
                                    its = true;
                                    break;
                                }
                            }

                            for (var i = 0; i < vl.listMarketExcept.length; i++) {
                                if (vl.listMarketExcept[i].vlMarket == markup.cmarket) {
                                    itsMarketExcept = true;
                                    break;
                                }
                            }

                            for (var i = 0; i < vl.listAgentExcept.length; i++) {
                                if (vl.listAgentExcept[i].md5code == req.body.iScode) {
                                    itsAgentExcept = true;
                                    break;
                                }
                            }


                            if (its == true) {
                                if (vl.Groupcode == undefined && !itsMarketExcept && !itsAgentExcept) {
                                    showDataClientTemp.push(vl);
                                } else if (vl.Groupcode === vl.code && !itsMarketExcept && !itsAgentExcept) {
                                    showDataClientTemp.push(vl);
                                } else {
                                    count--;
                                }
                            }

                        });
                        var data = {
                            showDataClient: showDataClientTemp,
                            count: count
                        };
                        res.jsonp(data);

                    } else {
                        res.json("null");
                    }
                });
        });
};
exports.CheckExistCode = function(req, res) {
    DataSave.findOne({ code: req.body.code })
        .then(function(dataSave) {
            if (dataSave && req.body.action == 'add') {
                res.json("Exist");
            }
            if (!dataSave && req.body.action == 'add') {
                res.json("notExist");
            } else if (dataSave && req.body.action == 'update' && req.body._id.toString() !== dataSave._id.toString()) {

                res.json("Exist");

            } else if (dataSave && req.body.action == 'update' && req.body._id.toString() === dataSave._id.toString()) {
                res.json("notExist");
            } else {
                res.json("notExist");
            }
        })
};
exports.LoadAllDataSave = function(req, res) {
    var re = new RegExp(req.body.search.toLowerCase(), 'i');
    var _pageNumber = req.body._pageNumber,
        _pageSize = 10;
    console.log(req.body);
    DataSave.count(req.body.param)
        .or([{ 'code': { $regex: re } },
            { 'excursionName': { $regex: re } },
            { 'location': { $regex: re } },
            { 'category': { $regex: re } },
            { 'priority': { $regex: re } }
        ])
        .exec(function(err, count) {
            DataSave.find(req.body.param)
                .sort({ location: 1 })
                .sort({ category: 1 })
                .or([{ 'code': { $regex: re } },
                    { 'excursionName': { $regex: re } },
                    { 'location': { $regex: re } },
                    { 'category': { $regex: re } },
                    { 'priority': { $regex: re } }
                ])
                .skip(_pageNumber > 0 ? ((_pageNumber - 1) * _pageSize) : 0).limit(_pageSize).exec(function(err, dataSave) {

                    if (dataSave) {
                        var data = {
                            showDataEdit: dataSave,
                            count: count
                        };
                        res.jsonp(data);
                    } else {
                        res.json("null");
                    }

                });
        });
};

exports.LoadReviseDataSave = function(req, res) {
    DataSave.find(req.body.condition)
        .then(function(dataSave) {
            if (dataSave) {
                ExchangeRate.find({}).then(function(dataEx) {
                    var VND = 1,
                        USD = 1;
                    if (req.body.name == 'group') {
                        for (var i = 0; i < dataSave.length; i++) {
                            SetVndUsd(dataEx, dataSave[i].beginDate);
                            var temp = dataSave[i].lsGroups;
                            for (var j = 0; j < temp.length; j++) {
                                if (req.body.id == temp[j].id) {
                                    temp[j].price = GetPriceFromExchangeRate(req.body.value.price, req.body.value.currency, dataSave[i].currency);
                                }
                            }
                            SaveRevise(dataSave[i]);
                        }
                    } else if (req.body.name == 'priceband') {
                        for (var i = 0; i < dataSave.length; i++) {
                            SetVndUsd(dataEx, dataSave[i].beginDate);
                            var ListSave = dataPriceBand(req.body.value.list, req.body.value.currency, dataSave[i].currency)

                            var temp = dataSave[i].lsVehicle;
                            for (var j = 0; j < temp.length; j++) {
                                if (req.body.id == temp[j].id) {
                                    temp[j].listvalue = ListSave;
                                }
                            }
                            SaveRevise(dataSave[i]);
                        }
                    } else if (req.body.name == 'accumulated') {
                        for (var i = 0; i < dataSave.length; i++) {
                            SetVndUsd(dataEx, dataSave[i].beginDate);
                            var temp = dataSave[i].lsAccumulated;
                            for (var j = 0; j < temp.length; j++) {
                                if (req.body.id == temp[j].id) {
                                    temp[j].price = GetPriceFromExchangeRate(req.body.value.price, req.body.value.currency, dataSave[i].currency, dataSave[i].beginDate);
                                    temp[j].types = req.body.value.types;
                                    temp[j].min = req.body.value.min;
                                    temp[j].max = req.body.value.max;
                                    temp[j].name = req.body.value.name;
                                }
                            }
                            SaveRevise(dataSave[i]);
                        }
                    }

                    function dataPriceBand(data, currency, parentCurrency) {
                        for (var j = 0; j < data.length; j++) {
                            data[j].price = GetPriceFromExchangeRate(data[j].price, currency, parentCurrency)
                        }
                        return data;
                    }

                    function SetVndUsd(data, beginDate) {
                        VND = 1;
                        USD = 1;
                        var middle = new Date(beginDate.replace("T17:00:00.000Z", ""));
                        for (var i = 0; i < data.length; i++) {
                            var begin = new Date(data[i].beginDate.replace("T17:00:00.000Z", ""));
                            var end = new Date(data[i].endDate.replace("T17:00:00.000Z", ""));
                            if (begin <= middle && middle <= end) {
                                VND = data[i].VND;
                                USD = data[i].USD;
                                break;
                            }
                        }

                    }

                    function GetPriceFromExchangeRate(priceservice, currency, parentCurrency) {
                        if (currency == parentCurrency)
                            return priceservice;
                        if (parentCurrency == "THB") {
                            return priceservice * VND / USD;
                        }
                        return priceservice * USD / VND;
                    }

                    function filltermax(list) {
                        var max = list[0].max;
                        for (var i = 0; i < list.length; i++)
                            if (list[i].max > max)
                                max = list[i].max;
                        return max;
                    }

                    function SaveRevise(dataSave) {

                        //////////////////////////////////////////
                        for (var i = 0; i < dataSave.tiers.length; i++) {
                            var PaxValue = {};
                            var tem = 0;
                            var datatem = 0;
                            dataSave.lsVehicle.forEach(function(value) {
                                value.listvalue.forEach(function(vl) {
                                    if (dataSave.tiers[i].pax >= vl.min && dataSave.tiers[i].pax <= vl.max) {
                                        tem += vl.price;
                                    } else if (dataSave.tiers[i].pax > filltermax(value.listvalue))
                                        datatem = (Math.ceil(dataSave.tiers[i].pax / vl.max) * vl.price)
                                });
                                if (dataSave.tiers[i].pax > filltermax(value.listvalue)) {
                                    tem += datatem;
                                }
                            });
                            PaxValue.Vehicle = (tem / dataSave.tiers[i].pax);


                            dataSave.lsAccumulated.forEach(function(value) {
                                if (dataSave.tiers[i].pax >= value.min && dataSave.tiers[i].pax <= value.max) {
                                    if (PaxValue.Accumulated) {
                                        PaxValue.Accumulated += (Math.ceil(value.price / dataSave.tiers[i].pax));
                                    } else {
                                        PaxValue.Accumulated = (Math.ceil(value.price / dataSave.tiers[i].pax));
                                    }
                                } else {
                                    if (PaxValue.Accumulated) {
                                        PaxValue.Accumulated += (Math.ceil(dataSave.tiers[i].pax / value.max) * value.price) / dataSave.tiers[i].pax;
                                    } else {
                                        PaxValue.Accumulated = (Math.ceil(dataSave.tiers[i].pax / value.max) * value.price) / dataSave.tiers[i].pax;
                                    }
                                }
                            });

                            for (var index = dataSave.lsAccommodation.length - 1; index >= 0; index--) {
                                if (dataSave.tiers[i].pax >= dataSave.lsAccommodation[index].occupancy) {
                                    PaxValue.Accommodation = (dataSave.lsAccommodation[index].price / dataSave.lsAccommodation[index].occupancy);
                                    break;
                                }
                            }

                            var TotalPerson = 0,
                                TotalGroups = 0;
                            dataSave.lsGroups.forEach(function(value) {
                                if (value.groupPerson === "Per Group")
                                    TotalGroups += value.price;
                                else TotalPerson += value.price;

                            });

                            PaxValue.buyPrice = ((TotalGroups || 0) / dataSave.tiers[i].pax) + (TotalPerson || 0) + (PaxValue.Vehicle || 0) + (PaxValue.Accommodation || 0) + (PaxValue.Accumulated || 0);
                            var pricecheck = PaxValue.buyPrice + PaxValue.buyPrice * dataSave.tiers[i].makeUp / 100;
                            var profitCheck = (dataSave.tiers[i].price - PaxValue.buyPrice) * dataSave.tiers[i].pax;
                            dataSave.tiers[i].price = pricecheck > 0 ? pricecheck : 0;
                            dataSave.tiers[i].profit = profitCheck > 0 ? profitCheck : 0;

                        }
                        // //////////////////////////////////////////     
                        dataSave.save(function(err) {
                            if (err) {
                                return res.status(400).send({
                                    message: errorHandler.getErrorMessage(err)
                                });
                            } else {

                                res.jsonp(true);
                            }
                        });
                    }
                });
            } else {
                res.json("null");
            }
        })
};

exports.deleteTour = function(req, res) {
    var dataSave = req.dataSave;
    DataSave.findOne({ '_id': req.body._id })
        .then(function(item) {
            item.remove(function(err) {
                if (err) {
                    return res.status(400).send({
                        message: errorHandler.getErrorMessage(err)
                    });
                } else {
                    res.jsonp(dataSave);
                }
            });
        });
};


exports.SaveCode = function(req, res) {
    console.log(req.body)
};


/**
 * Create a Tourview tl
 */
exports.create = function(req, res) {
    var tourviewTl = new TourviewTl(req.body);
    tourviewTl.user = req.user;

    tourviewTl.save(function(err) {
        if (err) {
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            res.jsonp(tourviewTl);
        }
    });
};

/**
 * Show the current Tourview tl
 */
exports.read = function(req, res) {
    // convert mongoose document to JSON
    var tourviewTl = req.tourviewTl ? req.tourviewTl.toJSON() : {};

    // Add a custom field to the Article, for determining if the current User is the "owner".
    // NOTE: This field is NOT persisted to the database, since it doesn't exist in the Article model.
    tourviewTl.isCurrentUserOwner = req.user && tourviewTl.user && tourviewTl.user._id.toString() === req.user._id.toString();

    res.jsonp(tourviewTl);
};

/**
 * Update a Tourview tl
 */
exports.update = function(req, res) {
    var tourviewTl = req.tourviewTl;

    tourviewTl = _.extend(tourviewTl, req.body);

    tourviewTl.save(function(err) {
        if (err) {
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            res.jsonp(tourviewTl);
        }
    });
};

/**
 * Delete an Tourview tl
 */
exports.delete = function(req, res) {
    var tourviewTl = req.tourviewTl;

    tourviewTl.remove(function(err) {
        if (err) {
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            res.jsonp(tourviewTl);
        }
    });
};
exports.GettiersByGroupCode = function(req, res) {
    var rsData = [];
    console.log(req.body.Groupcode)
    DataSave.find({ 'Groupcode': req.body.Groupcode })
        .sort({ code: 1 })
        .then(function(item) {
            item.forEach(function(vl) {
                var newItem = {
                    currency: vl.currency,
                    code: vl.code,
                    tiers: vl.tiers,
                    noteTiers: vl.noteTiers,
                    beginDate: vl.beginDate,
                    endDate: vl.endDate,
                    lsChildPolicy: vl.lsChildPolicy
                }
                rsData.push(newItem);
            });
            res.json(rsData);
        });
};
/**
 * List of Tourview tls
 */
exports.list = function(req, res) {
    TourviewTl.find().sort('-created').populate('user', 'displayName').exec(function(err, tourviewTls) {
        if (err) {
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            res.jsonp(tourviewTls);
        }
    });
};

/**
 * Tourview tl middleware
 */
exports.tourviewTlByID = function(req, res, next, id) {

    if (!mongoose.Types.ObjectId.isValid(id)) {
        return res.status(400).send({
            message: 'Tourview tl is invalid'
        });
    }

    TourviewTl.findById(id).populate('user', 'displayName').exec(function(err, tourviewTl) {
        if (err) {
            return next(err);
        } else if (!tourviewTl) {
            return res.status(404).send({
                message: 'No Tourview tl with that identifier has been found'
            });
        }
        req.tourviewTl = tourviewTl;
        next();
    });
};



/// tool Update Data. ========================
exports.ToolUpdate = function(req, res) {
    DataSave.find()
        .then(function(item) {
            function decimalAdjust(type, value, exp) {
                // If the exp is undefined or zero...
                if (typeof exp === 'undefined' || +exp === 0) {
                    return Math[type](value);
                }
                value = +value;
                exp = +exp;
                // If the value is not a number or the exp is not an integer...
                if (isNaN(value) || !(typeof exp === 'number' && exp % 1 === 0)) {
                    return NaN;
                }
                // Shift
                value = value.toString().split('e');
                value = Math[type](+(value[0] + 'e' + (value[1] ? (+value[1] - exp) : -exp)));
                // Shift back
                value = value.toString().split('e');
                return +(value[0] + 'e' + (value[1] ? (+value[1] + exp) : exp));
            }

            var d = item.length;
            var m = 0;
            for (var i = 0; i < item.length; i++) {
                if (item[i].lsChildPolicy) {
                    for (var y = 0; y < item[i].tiers.length; y++) {
                        item[i].tiers[y].priceChild = [];
                        if (item[i].tiers[y].ChildAplly == 'Yes') {
                            item[i].lsChildPolicy.forEach(function(vl) {

                                // if(vl.discount = '%'){
                                //     vl.discount = 'Discount by (%)';
                                //     console.log("%" + i);
                                // }
                                // else if(vl.discount = 'Amount'){
                                //     vl.discount = 'Discount by (Amount)';
                                //     console.log("Amount" + i);
                                // }
                                // else{
                                //     vl.discount = 'Fixed Rate';
                                //      console.log("Fixed" + i);
                                // }

                                if (vl.discount == 'Discount by (%)') {
                                    var priceMarkup = item[i].tiers[y].price - (item[i].tiers[y].price * vl.value / 100)
                                    priceMarkup = decimalAdjust('round', priceMarkup, 1);
                                    var vlchange = { vl: priceMarkup, vlFix: false }
                                    item[i].tiers[y].priceChild.push(vlchange);
                                    console.log("%" + y);
                                } else if (vl.discount == 'Discount by (Amount)') {
                                    var priceMarkup = item[i].tiers[y].price - vl.value;
                                    priceMarkup = decimalAdjust('round', priceMarkup, 1);
                                    var vlchange = { vl: priceMarkup, vlFix: false }
                                    item[i].tiers[y].priceChild.push(vlchange);
                                    console.log("Amount" + y);
                                } else {
                                    var vlchange = { vl: decimalAdjust('round', vl.value, 1), vlFix: true }
                                    item[i].tiers[y].priceChild.push(vlchange);
                                    console.log("Amount" + y);
                                }
                            });
                        }
                    }
                }
                item[i].save(function(err) {
                    if (err) {
                        return res.status(400).send({
                            message: errorHandler.getErrorMessage(err)
                        });
                    } else {
                        m++;

                    }
                });
            }
            res.json("Success!");
        });
};

exports.ToolUpdateValueEditModel = function(req, res) {
    DataSave.find()
        .then(function(item) {
            for (var i = 0; i < item.length; i++) {
                item[i].ValueEditModel = 'excursions';
                item[i].save(function(err) {
                    if (err) {
                        return res.status(400).send({
                            message: errorHandler.getErrorMessage(err)
                        });
                    }
                });
            }
            res.json("Success!");
        });
};


exports.ToolUpdateValuelistMarketExcept = function(req, res) {
    DataSave.find()
        .then(function(item) {
            for (var i = 0; i < item.length; i++) {
                item[i].listMarketExcept = [{ vlMarket: 'SP' }];
                item[i].save(function(err) {
                    if (err) {
                        return res.status(400).send({
                            message: errorHandler.getErrorMessage(err)
                        });
                    }
                });
            }
            res.json("Success!");
        });
};
'use strict';

/**
 * Module dependencies
 */
var tourviewTlsPolicy = require('../policies/tourview-tls.server.policy'),
    tourviewTls = require('../controllers/tourview-tls.server.controller');

module.exports = function(app) {
    // Tourview tls Routes
    app.route('/api/tourview-tls').all(tourviewTlsPolicy.isAllowed)
        .get(tourviewTls.list)
        .post(tourviewTls.create);

    app.route('/api/tourview-tls/:tourviewTlId').all(tourviewTlsPolicy.isAllowed)
        .get(tourviewTls.read)
        .put(tourviewTls.update)
        .delete(tourviewTls.delete);


    app.route('/api/tourviewTls/code')
        .post(tourviewTls.SaveCode)

    app.route('/api/tourviewTls/updateImage')
        .post(tourviewTls.updateImage)
    app.route('/api/tourviewTls/updateimagegroup')
        .put(tourviewTls.updateImageGroup)
    app.route('/api/tourviewTls/deleteimage')
        .post(tourviewTls.DeleteImage)

    app.route('/api/tourviewTls/GeTListBygroup')
        .post(tourviewTls.GeTListBygroup)

    app.route('/api/tourviewTls/loadimage')
        .get(tourviewTls.LoadListImage)

    app.route('/api/tourviewTls/LoadImageToContent')
        .get(tourviewTls.LoadImageToContent)



    //Tool Update Data
    app.route('/api/tourviewTls/ToolUpdate')
        .get(tourviewTls.ToolUpdateValueEditModel)
        //.get(tourviewTls.ToolUpdateValuelistMarketExcept)
        //.get(tourviewTls.ToolUpdate)
        // service
    app.route('/api/tourviewTls/GettiersByGroupCode')
        .post(tourviewTls.GettiersByGroupCode)

    //SevGroup
    app.route('/api/tourviewTls/LoadSevGroup').get(tourviewTls.LoadSevGroup)
    app.route('/api/tourviewTls/createSevGroup').post(tourviewTls.createSevGroup)
    app.route('/api/tourviewTls/updateSevGroup').put(tourviewTls.updateSevGroup)
    app.route('/api/tourviewTls/deleteSevGroup').post(tourviewTls.deleteSevGroup)

    app.route('/api/tourviewTls/LoadService').post(tourviewTls.LoadService)


    //Category
    app.route('/api/tourviewTls/Category')
        .post(tourviewTls.createCategory)
        .get(tourviewTls.loadCategory)
        .put(tourviewTls.updateCategory)

    app.route('/api/tourviewTls/deleteCategory')
        .post(tourviewTls.deleteCategory)

    //servicetype
    app.route('/api/tourviewTls/servicetype')
        .post(tourviewTls.createServiceType)
        .get(tourviewTls.loadServiceType)
        .put(tourviewTls.updateServiceType)

    app.route('/api/tourviewTls/deleteservicetype')
        .post(tourviewTls.deleteServiceType)

    //geotree
    app.route('/api/tourviewTls/geotree')
        .post(tourviewTls.createGeoTree)
        .get(tourviewTls.loadGeoTree)
        .put(tourviewTls.updateGeoTree)
    app.route('/api/tourviewTls/deletegeotree')
        .post(tourviewTls.deleteGeoTree)
        //ChildPolicy
        //  app.route('/api/tourviewTls/childpolicy')
        //     .post(tourviewTls.createChildPolicy)
        //     .get(tourviewTls.loadChildPolicy)
        //     .put(tourviewTls.updateChildPolicy)

    // app.route('/api/tourviewTls/deletechildpolicy')
    //     .post(tourviewTls.deleteGeoTree)

    //exchangerate
    app.route('/api/tourviewTls/exchangerate')
        .post(tourviewTls.createExchangeRate)
        .get(tourviewTls.loadExchangeRate)
        .put(tourviewTls.updateExchangeRate)

    app.route('/api/tourviewTls/deleteexchangerate')
        .post(tourviewTls.deleteExchangeRate)


    //tariffperiod
    app.route('/api/tourviewTls/tariffperiod')
        .post(tourviewTls.createTariffPeriod)
        .get(tourviewTls.loadTariffPeriod)
        .put(tourviewTls.updateTariffPeriod)

    app.route('/api/tourviewTls/deletetariffperiod')
        .post(tourviewTls.deleteTariffPeriod)

    //Supplier
    app.route('/api/tourviewTls/supplier')
        .post(tourviewTls.createSupplier)
        .get(tourviewTls.loadSupplier)
        .put(tourviewTls.updateSupplier)

    app.route('/api/tourviewTls/deletesupplier')
        .post(tourviewTls.deleteSupplier)

    //Priority
    app.route('/api/tourviewTls/priority')
        .post(tourviewTls.createPriority)
        .get(tourviewTls.loadPriority)
        .put(tourviewTls.updatePriority)

    app.route('/api/tourviewTls/deletepriority')
        .post(tourviewTls.deletePriority)
        //notify
    app.route('/api/tourviewTls/notify')
        .post(tourviewTls.createNotify)
        .get(tourviewTls.loadNotify)
        .put(tourviewTls.updateNotify)

    app.route('/api/tourviewTls/loadnotifyclient')
        .get(tourviewTls.loadNotifyClient)

    app.route('/api/tourviewTls/deletenotify')
        .post(tourviewTls.deleteNotify)

    //User
    app.route('/api/tourviewTls/users')
        .post(tourviewTls.createUsers)
        .get(tourviewTls.loadUsers)
        .put(tourviewTls.updateUsers)

    app.route('/api/tourviewTls/updateprofileusers').put(tourviewTls.updateProfileUsers)
    app.route('/api/tourviewTls/updatepassusers').put(tourviewTls.updatePassUsers)
    app.route('/api/tourviewTls/updateimgprofile').post(tourviewTls.updateImgProfile)

    app.route('/api/tourviewTls/deleteusers')
        .post(tourviewTls.deleteUsers)
    app.route('/api/tourviewTls/login')
        .post(tourviewTls.loginUsers)

    //WebInfo
    app.route('/api/tourviewTls/webinfo')
        .post(tourviewTls.createWebInfo)
        .get(tourviewTls.loadWebInfo)
        .put(tourviewTls.updateWebInfo)

    app.route('/api/tourviewTls/updateimgwebbanner').post(tourviewTls.UpdateImgWebBanner)
    app.route('/api/tourviewTls/addimgslide').post(tourviewTls.AddImgSlide)
    app.route('/api/tourviewTls/deleteimgslide').post(tourviewTls.DeleteImgSlide)
    app.route('/api/tourviewTls/updateterm').post(tourviewTls.UpdateTern)
    app.route('/api/tourviewTls/updatecoverpage').post(tourviewTls.UpdateCoverPage)

    //big data

    app.route('/api/tourviewTls/datasave')
        .post(tourviewTls.createDataSave)
        .put(tourviewTls.updateDataSave)
    app.route('/api/tourviewTls/filerdatasave')
        .post(tourviewTls.FilterDataSave)

    app.route('/api/tourviewTls/loaddata')
        .get(tourviewTls.LoadDataSave)
        .post(tourviewTls.LoadDataSaveByMarkup)
    app.route('/api/tourviewTls/loaddatasaveexcel')
        .post(tourviewTls.LoadDataSaveExcel)
    app.route('/api/tourviewTls/loadalldata')
        .post(tourviewTls.LoadAllDataSave)
    app.route('/api/tourviewTls/checkexistcode')
        .post(tourviewTls.CheckExistCode)
    app.route('/api/tourviewTls/loadrevisedata')
        .post(tourviewTls.LoadReviseDataSave)

    app.route('/api/tourviewTls/deletetour')
        .post(tourviewTls.deleteTour)
    app.route('/api/tourviewTls/LoadDataSaveById')
        .post(tourviewTls.LoadDataSaveById)

    app.route('/api/tourviewTls/groupImage')
        .get(tourviewTls.LoadGroupImage)
        .post(tourviewTls.createGroupImage)
        .put(tourviewTls.updateGroupImage)
    app.route('/api/tourviewTls/deletegroupimage')
        .post(tourviewTls.deleteGroupImage)


    // Finish by binding the Tourview tl middleware
    app.param('tourviewTlId', tourviewTls.tourviewTlByID);
};
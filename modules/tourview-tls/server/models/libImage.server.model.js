'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

/**
 * Tourtariff Schema
 */
var TlsListimageSchema = new Schema({
    nameImage: {
        type: String,
        default: '',
        required: 'Please fill Listimage name',
        trim: true
    },
    urlImage: String,
    urlThumbnail: String,
    group: String,
});

mongoose.model('TlsListimage', TlsListimageSchema);
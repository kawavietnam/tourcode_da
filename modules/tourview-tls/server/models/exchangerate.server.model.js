'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
  Schema = mongoose.Schema;

/**
 * Tourtariff Schema
 */
var TlsExchangeRateSchema = new Schema({
  beginDate: {
    type: String,
    default: '',
    required: 'Please fill begin date',
    trim: true
  },
  endDate: String,
  USD: Number,
  VND: Number
});

mongoose.model('TlsExchangeRate', TlsExchangeRateSchema);
  
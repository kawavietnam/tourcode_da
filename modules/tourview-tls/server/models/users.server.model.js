'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

/**
 * Tourtariff Schema
 */
var TlsUsersSchema = new Schema({
    fullname: {
        type: String,
        default: '',
        required: 'Please fill Users fullname',
        trim: true
    },
    email: String,
    company: String,
    username: {
        type: String,
       lowercase: true
     },
    pass: String,
    active: Boolean,
    role: [{
        name: String,
        code: String
    }],
    nameImg: String
});

mongoose.model('TlsUsers', TlsUsersSchema);
'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

/**
 * Tourtariff Schema
 */
var TlsNotifySchema = new Schema({
   text: {
        type: String,
        default: '',
        required: 'Please fill GeoTree name',
        trim: true
    },
    isActive: Boolean
});

mongoose.model('TlsNotify', TlsNotifySchema);
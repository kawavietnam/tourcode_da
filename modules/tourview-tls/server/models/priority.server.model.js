'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

/**
 * Tourtariff Schema
 */
var TlsPrioritySchema = new Schema({
    name: {
        type: String,
        default: '',
        required: 'Please fill GeoTree name',
        trim: true
    },
    code: String
});

mongoose.model('TlsPriority', TlsPrioritySchema);
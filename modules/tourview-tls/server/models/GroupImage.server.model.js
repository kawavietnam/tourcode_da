'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
  Schema = mongoose.Schema;

/**
 * Tourtariff Schema
 */
var TlsGroupImageSchema = new Schema({
  name: {
    type: String,
    default: '',
    required: 'Please fill GroupImage name',
    trim: true
  }
});

mongoose.model('TlsGroupImage', TlsGroupImageSchema);

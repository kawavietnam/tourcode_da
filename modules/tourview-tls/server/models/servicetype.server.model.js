'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
  Schema = mongoose.Schema;

/**
 * Tourtariff Schema
 */
var TlsServiceTypeSchema = new Schema({
  name: {
    type: String,
    default: '',
    required: 'Please fill ServiceType name',
    trim: true
  },
  code:String
});

mongoose.model('TlsServiceType', TlsServiceTypeSchema);
  
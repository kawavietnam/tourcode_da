'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

/**
 * Tourtariff Schema
 */
var TlsSevGroupSchema = new Schema({
    name: {
        type: String,
        default: '',
        required: 'Please fill name',
        trim: true
    },
    location: String,
    supplier: String,
    supplierTourCode: String,
    beginDate: Date,
    endDate: Date,
    types: String,
    address: String,
    tel: String,
    email: String,
    url: String,
    group: {
        price: Number,
        currency: String
    },
    priceband: {
        currency: String,
        list: [{
            name: String,
            min: Number,
            max: Number,
            price: Number
        }]
    },
    accumulated: {
        name: String,
        min: Number,
        max: Number,
        price: Number,
        currency: String
    }

});

mongoose.model('TlsSevGroup', TlsSevGroupSchema);
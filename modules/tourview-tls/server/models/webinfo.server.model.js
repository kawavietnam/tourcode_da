'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

/**
 * Tourtariff Schema
 */
var TlsWebInfoSchema = new Schema({
    expBanner: String,
    webBanner: String,
    listSlide: [{
        name: String
    }],
    term: String,
    coverpage: String
});

mongoose.model('TlsWebInfo', TlsWebInfoSchema);
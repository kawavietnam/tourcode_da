'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

/**
 * Tourtariff Schema
 */
var TlsDataSaveSchema = new Schema({
     _idUser: String,
    code: String,
    showGroupCode: Boolean,
    Groupcode:String,
    excursionName: String,
    category: String,
    location: String,
    noteTiers: String,
    isActive: Boolean,
    market: [{ vlMarket: String }],
    currency: String,
    priority: String,
    beginDate: Date,
    endDate: Date,
    beginDateTemp: Date,
    endDateTemp: Date,
    updateTime: Number,
    createTime: Number,
    updatePer: String,
    createPer: String,
    note:String,
    DirectMarkup: [{
        name: String,
        charge: String,
        byPrice: Number,
        directMarkup: Number,
        price:Number,
        note: String,
        DateGroup: String,
        beginDate: Date,
        endDate: Date,
        time: Number
    }],
     tiers: [{
        pax: Number,
        price: Number,
        makeUp: Number,
        profit: Number,
        ChildAplly: String,
        priceChild:[{vl:Number,vlFix:Boolean}]
    }],
    lsChildPolicy:[{
       ageFrom: Number,
       ageTo: Number,
       discount : String,
       value : Number,
       note: String
    }],
    lsGroups: [{
        id: Schema.Types.ObjectId,
        day:Number,
        supplier: String,
        endDate: Date,
        beginDate: Date,
        supplierTourCode: String,
        name: String,
        temp: Number,
        markup: Number,
        types: String,
        price: Number,
        groupPerson: String
    }],
    lsVehicle: [{
        id: Schema.Types.ObjectId,
        name: String,
        excursion:String,
        location:String,
        content: String,
        supplier: String,
        endDate: Date,
        beginDate: Date,
        supplierTourCode: String,
        day:Number,
        code:String,
        types: String,
        listvalue: [{
            name: String,
            markup: Number,
            temp: Number,
            min: Number,
            max: Number,
            price: Number
        }]
    }],
    lsAccumulated: [{
        id: Schema.Types.ObjectId,
        name: String,
        supplier: String,
        supplierTourCode: String,
        day:Number,
        endDate: Date,
        beginDate: Date,
        types: String,
        temp: Number,
        markup: Number,
        min: Number,
        max: Number,
        price: Number
    }],
    lsAccommodation: [{
        day:Number,
        supplierTourCode: String,
        name: String,
        occupancy: Number,
        price: Number
    }],
    lsTiming: [{
        duration: Number,
        description: String,
        lenghts: Number
    }],
    lsExtra: [{
        extra: String,
        charge: String,
        note: String,
        price: Number
    }],

    startTime: {
        hour: Number,
        minute: Number
    },
    maxTiming: Number,
    content: String,
    slideImages: [{ image: String }],
    availab: {
        sun: Boolean,
        mon: Boolean,
        tue: Boolean,
        wed: Boolean,
        thu: Boolean,
        fri: Boolean,
        sat: Boolean
    },
    checkDirectModel: Boolean,
    CRM:Boolean,
    DirectSetup: Boolean,
    noteDirectModel: String,
    agname: String,
    md5code: String,
    ObjectAgent : {
        agmail: String,
        agname: String,
        agref: String,
        ccode: String,
        cemail: String,
        cmarket: String,
        cname: String,
        md5code: String
    },
    ValueEditModel: String,
    Proposalstatus:String,
    LandingMK: Number,
    HotelMkALL: Number,
    listMarketExcept : [
       { vlMarket: String }
    ],
    listAgentExcept: [
        {
            agmail: String,
            agname: String,
            agref: String,
            ccode: String,
            cemail: String,
            cmarket: String,
            cname: String,
            md5code: String
        }
    ],
    lsDirectModel: [
        {
            day: Number,
            name: String,
            charge: String,
            roomcategory: String,
            directMkModel: Number,                        
            occupancy: Number,
            priceShareRoom: Number,
            priceSGLRoom: Number,
            nights: Number,
            rooms:Number,
            DateGroup: String,
            endDate: Date,
            beginDate: Date,
            time: Number,
            twinShare: Number,
            SGLSuppl: Number,
            tiers:[
             {
                pax: Number,
                price: Number
             }
            ]
        }
    ]
});

mongoose.model('TlsDataSave', TlsDataSaveSchema);
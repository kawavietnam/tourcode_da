'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
  Schema = mongoose.Schema;

/**
 * Tourtariff Schema
 */
var TlsGeoTreeSchema = new Schema({
  name: {
    type: String,
    default: '',
    required: 'Please fill GeoTree name',
    trim: true
  },
  zone: String
});

mongoose.model('TlsGeoTree', TlsGeoTreeSchema);
  
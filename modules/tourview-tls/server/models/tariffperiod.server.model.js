'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

/**
 * Tourtariff Schema
 */
var TlsTariffPeriodSchema = new Schema({
    date: Date,
    name: String,
    no: Number
});

mongoose.model('TlsTariffPeriod', TlsTariffPeriodSchema);
(function () {
  'use strict';

  describe('Tourview tls Route Tests', function () {
    // Initialize global variables
    var $scope,
      TourviewTlsService;

    // We can start by loading the main application module
    beforeEach(module(ApplicationConfiguration.applicationModuleName));

    // The injector ignores leading and trailing underscores here (i.e. _$httpBackend_).
    // This allows us to inject a service but then attach it to a variable
    // with the same name as the service.
    beforeEach(inject(function ($rootScope, _TourviewTlsService_) {
      // Set a new global scope
      $scope = $rootScope.$new();
      TourviewTlsService = _TourviewTlsService_;
    }));

    describe('Route Config', function () {
      describe('Main Route', function () {
        var mainstate;
        beforeEach(inject(function ($state) {
          mainstate = $state.get('tourview-tls');
        }));

        it('Should have the correct URL', function () {
          expect(mainstate.url).toEqual('/tourview-tls');
        });

        it('Should be abstract', function () {
          expect(mainstate.abstract).toBe(true);
        });

        it('Should have template', function () {
          expect(mainstate.template).toBe('<ui-view/>');
        });
      });

      describe('View Route', function () {
        var viewstate,
          TourviewTlsController,
          mockTourviewTl;

        beforeEach(inject(function ($controller, $state, $templateCache) {
          viewstate = $state.get('tourview-tls.view');
          $templateCache.put('modules/tourview-tls/client/views/view-tourview-tl.client.view.html', '');

          // create mock Tourview tl
          mockTourviewTl = new TourviewTlsService({
            _id: '525a8422f6d0f87f0e407a33',
            name: 'Tourview tl Name'
          });

          // Initialize Controller
          TourviewTlsController = $controller('TourviewTlsController as vm', {
            $scope: $scope,
            tourviewTlResolve: mockTourviewTl
          });
        }));

        it('Should have the correct URL', function () {
          expect(viewstate.url).toEqual('/:tourviewTlId');
        });

        it('Should have a resolve function', function () {
          expect(typeof viewstate.resolve).toEqual('object');
          expect(typeof viewstate.resolve.tourviewTlResolve).toEqual('function');
        });

        it('should respond to URL', inject(function ($state) {
          expect($state.href(viewstate, {
            tourviewTlId: 1
          })).toEqual('/tourview-tls/1');
        }));

        it('should attach an Tourview tl to the controller scope', function () {
          expect($scope.vm.tourviewTl._id).toBe(mockTourviewTl._id);
        });

        it('Should not be abstract', function () {
          expect(viewstate.abstract).toBe(undefined);
        });

        it('Should have templateUrl', function () {
          expect(viewstate.templateUrl).toBe('modules/tourview-tls/client/views/view-tourview-tl.client.view.html');
        });
      });

      describe('Create Route', function () {
        var createstate,
          TourviewTlsController,
          mockTourviewTl;

        beforeEach(inject(function ($controller, $state, $templateCache) {
          createstate = $state.get('tourview-tls.create');
          $templateCache.put('modules/tourview-tls/client/views/form-tourview-tl.client.view.html', '');

          // create mock Tourview tl
          mockTourviewTl = new TourviewTlsService();

          // Initialize Controller
          TourviewTlsController = $controller('TourviewTlsController as vm', {
            $scope: $scope,
            tourviewTlResolve: mockTourviewTl
          });
        }));

        it('Should have the correct URL', function () {
          expect(createstate.url).toEqual('/create');
        });

        it('Should have a resolve function', function () {
          expect(typeof createstate.resolve).toEqual('object');
          expect(typeof createstate.resolve.tourviewTlResolve).toEqual('function');
        });

        it('should respond to URL', inject(function ($state) {
          expect($state.href(createstate)).toEqual('/tourview-tls/create');
        }));

        it('should attach an Tourview tl to the controller scope', function () {
          expect($scope.vm.tourviewTl._id).toBe(mockTourviewTl._id);
          expect($scope.vm.tourviewTl._id).toBe(undefined);
        });

        it('Should not be abstract', function () {
          expect(createstate.abstract).toBe(undefined);
        });

        it('Should have templateUrl', function () {
          expect(createstate.templateUrl).toBe('modules/tourview-tls/client/views/form-tourview-tl.client.view.html');
        });
      });

      describe('Edit Route', function () {
        var editstate,
          TourviewTlsController,
          mockTourviewTl;

        beforeEach(inject(function ($controller, $state, $templateCache) {
          editstate = $state.get('tourview-tls.edit');
          $templateCache.put('modules/tourview-tls/client/views/form-tourview-tl.client.view.html', '');

          // create mock Tourview tl
          mockTourviewTl = new TourviewTlsService({
            _id: '525a8422f6d0f87f0e407a33',
            name: 'Tourview tl Name'
          });

          // Initialize Controller
          TourviewTlsController = $controller('TourviewTlsController as vm', {
            $scope: $scope,
            tourviewTlResolve: mockTourviewTl
          });
        }));

        it('Should have the correct URL', function () {
          expect(editstate.url).toEqual('/:tourviewTlId/edit');
        });

        it('Should have a resolve function', function () {
          expect(typeof editstate.resolve).toEqual('object');
          expect(typeof editstate.resolve.tourviewTlResolve).toEqual('function');
        });

        it('should respond to URL', inject(function ($state) {
          expect($state.href(editstate, {
            tourviewTlId: 1
          })).toEqual('/tourview-tls/1/edit');
        }));

        it('should attach an Tourview tl to the controller scope', function () {
          expect($scope.vm.tourviewTl._id).toBe(mockTourviewTl._id);
        });

        it('Should not be abstract', function () {
          expect(editstate.abstract).toBe(undefined);
        });

        it('Should have templateUrl', function () {
          expect(editstate.templateUrl).toBe('modules/tourview-tls/client/views/form-tourviewTl.client.view.html');
        });

        xit('Should go to unauthorized route', function () {

        });
      });

    });
  });
}());

'use strict';

var should = require('should'),
  request = require('supertest'),
  path = require('path'),
  mongoose = require('mongoose'),
  User = mongoose.model('User'),
  TourviewTl = mongoose.model('TourviewTl'),
  express = require(path.resolve('./config/lib/express'));

/**
 * Globals
 */
var app,
  agent,
  credentials,
  user,
  tourviewTl;

/**
 * Tourview tl routes tests
 */
describe('Tourview tl CRUD tests', function () {

  before(function (done) {
    // Get application
    app = express.init(mongoose);
    agent = request.agent(app);

    done();
  });

  beforeEach(function (done) {
    // Create user credentials
    credentials = {
      username: 'username',
      password: 'M3@n.jsI$Aw3$0m3'
    };

    // Create a new user
    user = new User({
      firstName: 'Full',
      lastName: 'Name',
      displayName: 'Full Name',
      email: 'test@test.com',
      username: credentials.username,
      password: credentials.password,
      provider: 'local'
    });

    // Save a user to the test db and create new Tourview tl
    user.save(function () {
      tourviewTl = {
        name: 'Tourview tl name'
      };

      done();
    });
  });

  it('should be able to save a Tourview tl if logged in', function (done) {
    agent.post('/api/auth/signin')
      .send(credentials)
      .expect(200)
      .end(function (signinErr, signinRes) {
        // Handle signin error
        if (signinErr) {
          return done(signinErr);
        }

        // Get the userId
        var userId = user.id;

        // Save a new Tourview tl
        agent.post('/api/tourviewTls')
          .send(tourviewTl)
          .expect(200)
          .end(function (tourviewTlSaveErr, tourviewTlSaveRes) {
            // Handle Tourview tl save error
            if (tourviewTlSaveErr) {
              return done(tourviewTlSaveErr);
            }

            // Get a list of Tourview tls
            agent.get('/api/tourviewTls')
              .end(function (tourviewTlsGetErr, tourviewTlsGetRes) {
                // Handle Tourview tls save error
                if (tourviewTlsGetErr) {
                  return done(tourviewTlsGetErr);
                }

                // Get Tourview tls list
                var tourviewTls = tourviewTlsGetRes.body;

                // Set assertions
                (tourviewTls[0].user._id).should.equal(userId);
                (tourviewTls[0].name).should.match('Tourview tl name');

                // Call the assertion callback
                done();
              });
          });
      });
  });

  it('should not be able to save an Tourview tl if not logged in', function (done) {
    agent.post('/api/tourviewTls')
      .send(tourviewTl)
      .expect(403)
      .end(function (tourviewTlSaveErr, tourviewTlSaveRes) {
        // Call the assertion callback
        done(tourviewTlSaveErr);
      });
  });

  it('should not be able to save an Tourview tl if no name is provided', function (done) {
    // Invalidate name field
    tourviewTl.name = '';

    agent.post('/api/auth/signin')
      .send(credentials)
      .expect(200)
      .end(function (signinErr, signinRes) {
        // Handle signin error
        if (signinErr) {
          return done(signinErr);
        }

        // Get the userId
        var userId = user.id;

        // Save a new Tourview tl
        agent.post('/api/tourviewTls')
          .send(tourviewTl)
          .expect(400)
          .end(function (tourviewTlSaveErr, tourviewTlSaveRes) {
            // Set message assertion
            (tourviewTlSaveRes.body.message).should.match('Please fill Tourview tl name');

            // Handle Tourview tl save error
            done(tourviewTlSaveErr);
          });
      });
  });

  it('should be able to update an Tourview tl if signed in', function (done) {
    agent.post('/api/auth/signin')
      .send(credentials)
      .expect(200)
      .end(function (signinErr, signinRes) {
        // Handle signin error
        if (signinErr) {
          return done(signinErr);
        }

        // Get the userId
        var userId = user.id;

        // Save a new Tourview tl
        agent.post('/api/tourviewTls')
          .send(tourviewTl)
          .expect(200)
          .end(function (tourviewTlSaveErr, tourviewTlSaveRes) {
            // Handle Tourview tl save error
            if (tourviewTlSaveErr) {
              return done(tourviewTlSaveErr);
            }

            // Update Tourview tl name
            tourviewTl.name = 'WHY YOU GOTTA BE SO MEAN?';

            // Update an existing Tourview tl
            agent.put('/api/tourviewTls/' + tourviewTlSaveRes.body._id)
              .send(tourviewTl)
              .expect(200)
              .end(function (tourviewTlUpdateErr, tourviewTlUpdateRes) {
                // Handle Tourview tl update error
                if (tourviewTlUpdateErr) {
                  return done(tourviewTlUpdateErr);
                }

                // Set assertions
                (tourviewTlUpdateRes.body._id).should.equal(tourviewTlSaveRes.body._id);
                (tourviewTlUpdateRes.body.name).should.match('WHY YOU GOTTA BE SO MEAN?');

                // Call the assertion callback
                done();
              });
          });
      });
  });

  it('should be able to get a list of Tourview tls if not signed in', function (done) {
    // Create new Tourview tl model instance
    var tourviewTlObj = new TourviewTl(tourviewTl);

    // Save the tourviewTl
    tourviewTlObj.save(function () {
      // Request Tourview tls
      request(app).get('/api/tourviewTls')
        .end(function (req, res) {
          // Set assertion
          res.body.should.be.instanceof(Array).and.have.lengthOf(1);

          // Call the assertion callback
          done();
        });

    });
  });

  it('should be able to get a single Tourview tl if not signed in', function (done) {
    // Create new Tourview tl model instance
    var tourviewTlObj = new TourviewTl(tourviewTl);

    // Save the Tourview tl
    tourviewTlObj.save(function () {
      request(app).get('/api/tourviewTls/' + tourviewTlObj._id)
        .end(function (req, res) {
          // Set assertion
          res.body.should.be.instanceof(Object).and.have.property('name', tourviewTl.name);

          // Call the assertion callback
          done();
        });
    });
  });

  it('should return proper error for single Tourview tl with an invalid Id, if not signed in', function (done) {
    // test is not a valid mongoose Id
    request(app).get('/api/tourviewTls/test')
      .end(function (req, res) {
        // Set assertion
        res.body.should.be.instanceof(Object).and.have.property('message', 'Tourview tl is invalid');

        // Call the assertion callback
        done();
      });
  });

  it('should return proper error for single Tourview tl which doesnt exist, if not signed in', function (done) {
    // This is a valid mongoose Id but a non-existent Tourview tl
    request(app).get('/api/tourviewTls/559e9cd815f80b4c256a8f41')
      .end(function (req, res) {
        // Set assertion
        res.body.should.be.instanceof(Object).and.have.property('message', 'No Tourview tl with that identifier has been found');

        // Call the assertion callback
        done();
      });
  });

  it('should be able to delete an Tourview tl if signed in', function (done) {
    agent.post('/api/auth/signin')
      .send(credentials)
      .expect(200)
      .end(function (signinErr, signinRes) {
        // Handle signin error
        if (signinErr) {
          return done(signinErr);
        }

        // Get the userId
        var userId = user.id;

        // Save a new Tourview tl
        agent.post('/api/tourviewTls')
          .send(tourviewTl)
          .expect(200)
          .end(function (tourviewTlSaveErr, tourviewTlSaveRes) {
            // Handle Tourview tl save error
            if (tourviewTlSaveErr) {
              return done(tourviewTlSaveErr);
            }

            // Delete an existing Tourview tl
            agent.delete('/api/tourviewTls/' + tourviewTlSaveRes.body._id)
              .send(tourviewTl)
              .expect(200)
              .end(function (tourviewTlDeleteErr, tourviewTlDeleteRes) {
                // Handle tourviewTl error error
                if (tourviewTlDeleteErr) {
                  return done(tourviewTlDeleteErr);
                }

                // Set assertions
                (tourviewTlDeleteRes.body._id).should.equal(tourviewTlSaveRes.body._id);

                // Call the assertion callback
                done();
              });
          });
      });
  });

  it('should not be able to delete an Tourview tl if not signed in', function (done) {
    // Set Tourview tl user
    tourviewTl.user = user;

    // Create new Tourview tl model instance
    var tourviewTlObj = new TourviewTl(tourviewTl);

    // Save the Tourview tl
    tourviewTlObj.save(function () {
      // Try deleting Tourview tl
      request(app).delete('/api/tourviewTls/' + tourviewTlObj._id)
        .expect(403)
        .end(function (tourviewTlDeleteErr, tourviewTlDeleteRes) {
          // Set message assertion
          (tourviewTlDeleteRes.body.message).should.match('User is not authorized');

          // Handle Tourview tl error error
          done(tourviewTlDeleteErr);
        });

    });
  });

  it('should be able to get a single Tourview tl that has an orphaned user reference', function (done) {
    // Create orphan user creds
    var _creds = {
      username: 'orphan',
      password: 'M3@n.jsI$Aw3$0m3'
    };

    // Create orphan user
    var _orphan = new User({
      firstName: 'Full',
      lastName: 'Name',
      displayName: 'Full Name',
      email: 'orphan@test.com',
      username: _creds.username,
      password: _creds.password,
      provider: 'local'
    });

    _orphan.save(function (err, orphan) {
      // Handle save error
      if (err) {
        return done(err);
      }

      agent.post('/api/auth/signin')
        .send(_creds)
        .expect(200)
        .end(function (signinErr, signinRes) {
          // Handle signin error
          if (signinErr) {
            return done(signinErr);
          }

          // Get the userId
          var orphanId = orphan._id;

          // Save a new Tourview tl
          agent.post('/api/tourviewTls')
            .send(tourviewTl)
            .expect(200)
            .end(function (tourviewTlSaveErr, tourviewTlSaveRes) {
              // Handle Tourview tl save error
              if (tourviewTlSaveErr) {
                return done(tourviewTlSaveErr);
              }

              // Set assertions on new Tourview tl
              (tourviewTlSaveRes.body.name).should.equal(tourviewTl.name);
              should.exist(tourviewTlSaveRes.body.user);
              should.equal(tourviewTlSaveRes.body.user._id, orphanId);

              // force the Tourview tl to have an orphaned user reference
              orphan.remove(function () {
                // now signin with valid user
                agent.post('/api/auth/signin')
                  .send(credentials)
                  .expect(200)
                  .end(function (err, res) {
                    // Handle signin error
                    if (err) {
                      return done(err);
                    }

                    // Get the Tourview tl
                    agent.get('/api/tourviewTls/' + tourviewTlSaveRes.body._id)
                      .expect(200)
                      .end(function (tourviewTlInfoErr, tourviewTlInfoRes) {
                        // Handle Tourview tl error
                        if (tourviewTlInfoErr) {
                          return done(tourviewTlInfoErr);
                        }

                        // Set assertions
                        (tourviewTlInfoRes.body._id).should.equal(tourviewTlSaveRes.body._id);
                        (tourviewTlInfoRes.body.name).should.equal(tourviewTl.name);
                        should.equal(tourviewTlInfoRes.body.user, undefined);

                        // Call the assertion callback
                        done();
                      });
                  });
              });
            });
        });
    });
  });

  afterEach(function (done) {
    User.remove().exec(function () {
      TourviewTl.remove().exec(done);
    });
  });
});

'use strict';

/**
 * Module dependencies.
 */
var should = require('should'),
  mongoose = require('mongoose'),
  User = mongoose.model('User'),
  TourviewTl = mongoose.model('TourviewTl');

/**
 * Globals
 */
var user,
  tourviewTl;

/**
 * Unit tests
 */
describe('Tourview tl Model Unit Tests:', function() {
  beforeEach(function(done) {
    user = new User({
      firstName: 'Full',
      lastName: 'Name',
      displayName: 'Full Name',
      email: 'test@test.com',
      username: 'username',
      password: 'password'
    });

    user.save(function() {
      tourviewTl = new TourviewTl({
        name: 'Tourview tl Name',
        user: user
      });

      done();
    });
  });

  describe('Method Save', function() {
    it('should be able to save without problems', function(done) {
      this.timeout(0);
      return tourviewTl.save(function(err) {
        should.not.exist(err);
        done();
      });
    });

    it('should be able to show an error when try to save without name', function(done) {
      tourviewTl.name = '';

      return tourviewTl.save(function(err) {
        should.exist(err);
        done();
      });
    });
  });

  afterEach(function(done) {
    TourviewTl.remove().exec(function() {
      User.remove().exec(function() {
        done();
      });
    });
  });
});

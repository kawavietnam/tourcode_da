(function () {
  'use strict';

  angular
    .module('core')
    .controller('HomeController', HomeController);
    HomeController.$inject = ['$scope','$location'];
  function HomeController($scope,$location) {
    var vm = this;
    $scope.Code ={};
    $scope.login = function(){
      if($scope.Code.txtCode =="codedanv")
      {
        $location.path('/tourtariffs')
      }
      else
      {
        $(".alert-danger").removeClass("display-hide")
      }
    }
  }
}());

'use strict';

var should = require('should'),
  request = require('supertest'),
  path = require('path'),
  mongoose = require('mongoose'),
  User = mongoose.model('User'),
  Tourtariff = mongoose.model('Tourtariff'),
  express = require(path.resolve('./config/lib/express'));

/**
 * Globals
 */
var app,
  agent,
  credentials,
  user,
  tourtariff;

/**
 * Tourtariff routes tests
 */
describe('Tourtariff CRUD tests', function () {

  before(function (done) {
    // Get application
    app = express.init(mongoose);
    agent = request.agent(app);

    done();
  });

  beforeEach(function (done) {
    // Create user credentials
    credentials = {
      username: 'username',
      password: 'M3@n.jsI$Aw3$0m3'
    };

    // Create a new user
    user = new User({
      firstName: 'Full',
      lastName: 'Name',
      displayName: 'Full Name',
      email: 'test@test.com',
      username: credentials.username,
      password: credentials.password,
      provider: 'local'
    });

    // Save a user to the test db and create new Tourtariff
    user.save(function () {
      tourtariff = {
        name: 'Tourtariff name'
      };

      done();
    });
  });

  it('should be able to save a Tourtariff if logged in', function (done) {
    agent.post('/api/auth/signin')
      .send(credentials)
      .expect(200)
      .end(function (signinErr, signinRes) {
        // Handle signin error
        if (signinErr) {
          return done(signinErr);
        }

        // Get the userId
        var userId = user.id;

        // Save a new Tourtariff
        agent.post('/api/tourtariffs')
          .send(tourtariff)
          .expect(200)
          .end(function (tourtariffSaveErr, tourtariffSaveRes) {
            // Handle Tourtariff save error
            if (tourtariffSaveErr) {
              return done(tourtariffSaveErr);
            }

            // Get a list of Tourtariffs
            agent.get('/api/tourtariffs')
              .end(function (tourtariffsGetErr, tourtariffsGetRes) {
                // Handle Tourtariffs save error
                if (tourtariffsGetErr) {
                  return done(tourtariffsGetErr);
                }

                // Get Tourtariffs list
                var tourtariffs = tourtariffsGetRes.body;

                // Set assertions
                (tourtariffs[0].user._id).should.equal(userId);
                (tourtariffs[0].name).should.match('Tourtariff name');

                // Call the assertion callback
                done();
              });
          });
      });
  });

  it('should not be able to save an Tourtariff if not logged in', function (done) {
    agent.post('/api/tourtariffs')
      .send(tourtariff)
      .expect(403)
      .end(function (tourtariffSaveErr, tourtariffSaveRes) {
        // Call the assertion callback
        done(tourtariffSaveErr);
      });
  });

  it('should not be able to save an Tourtariff if no name is provided', function (done) {
    // Invalidate name field
    tourtariff.name = '';

    agent.post('/api/auth/signin')
      .send(credentials)
      .expect(200)
      .end(function (signinErr, signinRes) {
        // Handle signin error
        if (signinErr) {
          return done(signinErr);
        }

        // Get the userId
        var userId = user.id;

        // Save a new Tourtariff
        agent.post('/api/tourtariffs')
          .send(tourtariff)
          .expect(400)
          .end(function (tourtariffSaveErr, tourtariffSaveRes) {
            // Set message assertion
            (tourtariffSaveRes.body.message).should.match('Please fill Tourtariff name');

            // Handle Tourtariff save error
            done(tourtariffSaveErr);
          });
      });
  });

  it('should be able to update an Tourtariff if signed in', function (done) {
    agent.post('/api/auth/signin')
      .send(credentials)
      .expect(200)
      .end(function (signinErr, signinRes) {
        // Handle signin error
        if (signinErr) {
          return done(signinErr);
        }

        // Get the userId
        var userId = user.id;

        // Save a new Tourtariff
        agent.post('/api/tourtariffs')
          .send(tourtariff)
          .expect(200)
          .end(function (tourtariffSaveErr, tourtariffSaveRes) {
            // Handle Tourtariff save error
            if (tourtariffSaveErr) {
              return done(tourtariffSaveErr);
            }

            // Update Tourtariff name
            tourtariff.name = 'WHY YOU GOTTA BE SO MEAN?';

            // Update an existing Tourtariff
            agent.put('/api/tourtariffs/' + tourtariffSaveRes.body._id)
              .send(tourtariff)
              .expect(200)
              .end(function (tourtariffUpdateErr, tourtariffUpdateRes) {
                // Handle Tourtariff update error
                if (tourtariffUpdateErr) {
                  return done(tourtariffUpdateErr);
                }

                // Set assertions
                (tourtariffUpdateRes.body._id).should.equal(tourtariffSaveRes.body._id);
                (tourtariffUpdateRes.body.name).should.match('WHY YOU GOTTA BE SO MEAN?');

                // Call the assertion callback
                done();
              });
          });
      });
  });

  it('should be able to get a list of Tourtariffs if not signed in', function (done) {
    // Create new Tourtariff model instance
    var tourtariffObj = new Tourtariff(tourtariff);

    // Save the tourtariff
    tourtariffObj.save(function () {
      // Request Tourtariffs
      request(app).get('/api/tourtariffs')
        .end(function (req, res) {
          // Set assertion
          res.body.should.be.instanceof(Array).and.have.lengthOf(1);

          // Call the assertion callback
          done();
        });

    });
  });

  it('should be able to get a single Tourtariff if not signed in', function (done) {
    // Create new Tourtariff model instance
    var tourtariffObj = new Tourtariff(tourtariff);

    // Save the Tourtariff
    tourtariffObj.save(function () {
      request(app).get('/api/tourtariffs/' + tourtariffObj._id)
        .end(function (req, res) {
          // Set assertion
          res.body.should.be.instanceof(Object).and.have.property('name', tourtariff.name);

          // Call the assertion callback
          done();
        });
    });
  });

  it('should return proper error for single Tourtariff with an invalid Id, if not signed in', function (done) {
    // test is not a valid mongoose Id
    request(app).get('/api/tourtariffs/test')
      .end(function (req, res) {
        // Set assertion
        res.body.should.be.instanceof(Object).and.have.property('message', 'Tourtariff is invalid');

        // Call the assertion callback
        done();
      });
  });

  it('should return proper error for single Tourtariff which doesnt exist, if not signed in', function (done) {
    // This is a valid mongoose Id but a non-existent Tourtariff
    request(app).get('/api/tourtariffs/559e9cd815f80b4c256a8f41')
      .end(function (req, res) {
        // Set assertion
        res.body.should.be.instanceof(Object).and.have.property('message', 'No Tourtariff with that identifier has been found');

        // Call the assertion callback
        done();
      });
  });

  it('should be able to delete an Tourtariff if signed in', function (done) {
    agent.post('/api/auth/signin')
      .send(credentials)
      .expect(200)
      .end(function (signinErr, signinRes) {
        // Handle signin error
        if (signinErr) {
          return done(signinErr);
        }

        // Get the userId
        var userId = user.id;

        // Save a new Tourtariff
        agent.post('/api/tourtariffs')
          .send(tourtariff)
          .expect(200)
          .end(function (tourtariffSaveErr, tourtariffSaveRes) {
            // Handle Tourtariff save error
            if (tourtariffSaveErr) {
              return done(tourtariffSaveErr);
            }

            // Delete an existing Tourtariff
            agent.delete('/api/tourtariffs/' + tourtariffSaveRes.body._id)
              .send(tourtariff)
              .expect(200)
              .end(function (tourtariffDeleteErr, tourtariffDeleteRes) {
                // Handle tourtariff error error
                if (tourtariffDeleteErr) {
                  return done(tourtariffDeleteErr);
                }

                // Set assertions
                (tourtariffDeleteRes.body._id).should.equal(tourtariffSaveRes.body._id);

                // Call the assertion callback
                done();
              });
          });
      });
  });

  it('should not be able to delete an Tourtariff if not signed in', function (done) {
    // Set Tourtariff user
    tourtariff.user = user;

    // Create new Tourtariff model instance
    var tourtariffObj = new Tourtariff(tourtariff);

    // Save the Tourtariff
    tourtariffObj.save(function () {
      // Try deleting Tourtariff
      request(app).delete('/api/tourtariffs/' + tourtariffObj._id)
        .expect(403)
        .end(function (tourtariffDeleteErr, tourtariffDeleteRes) {
          // Set message assertion
          (tourtariffDeleteRes.body.message).should.match('User is not authorized');

          // Handle Tourtariff error error
          done(tourtariffDeleteErr);
        });

    });
  });

  it('should be able to get a single Tourtariff that has an orphaned user reference', function (done) {
    // Create orphan user creds
    var _creds = {
      username: 'orphan',
      password: 'M3@n.jsI$Aw3$0m3'
    };

    // Create orphan user
    var _orphan = new User({
      firstName: 'Full',
      lastName: 'Name',
      displayName: 'Full Name',
      email: 'orphan@test.com',
      username: _creds.username,
      password: _creds.password,
      provider: 'local'
    });

    _orphan.save(function (err, orphan) {
      // Handle save error
      if (err) {
        return done(err);
      }

      agent.post('/api/auth/signin')
        .send(_creds)
        .expect(200)
        .end(function (signinErr, signinRes) {
          // Handle signin error
          if (signinErr) {
            return done(signinErr);
          }

          // Get the userId
          var orphanId = orphan._id;

          // Save a new Tourtariff
          agent.post('/api/tourtariffs')
            .send(tourtariff)
            .expect(200)
            .end(function (tourtariffSaveErr, tourtariffSaveRes) {
              // Handle Tourtariff save error
              if (tourtariffSaveErr) {
                return done(tourtariffSaveErr);
              }

              // Set assertions on new Tourtariff
              (tourtariffSaveRes.body.name).should.equal(tourtariff.name);
              should.exist(tourtariffSaveRes.body.user);
              should.equal(tourtariffSaveRes.body.user._id, orphanId);

              // force the Tourtariff to have an orphaned user reference
              orphan.remove(function () {
                // now signin with valid user
                agent.post('/api/auth/signin')
                  .send(credentials)
                  .expect(200)
                  .end(function (err, res) {
                    // Handle signin error
                    if (err) {
                      return done(err);
                    }

                    // Get the Tourtariff
                    agent.get('/api/tourtariffs/' + tourtariffSaveRes.body._id)
                      .expect(200)
                      .end(function (tourtariffInfoErr, tourtariffInfoRes) {
                        // Handle Tourtariff error
                        if (tourtariffInfoErr) {
                          return done(tourtariffInfoErr);
                        }

                        // Set assertions
                        (tourtariffInfoRes.body._id).should.equal(tourtariffSaveRes.body._id);
                        (tourtariffInfoRes.body.name).should.equal(tourtariff.name);
                        should.equal(tourtariffInfoRes.body.user, undefined);

                        // Call the assertion callback
                        done();
                      });
                  });
              });
            });
        });
    });
  });

  afterEach(function (done) {
    User.remove().exec(function () {
      Tourtariff.remove().exec(done);
    });
  });
});

(function () {
  'use strict';

  describe('Tourtariffs Route Tests', function () {
    // Initialize global variables
    var $scope,
      TourtariffsService;

    // We can start by loading the main application module
    beforeEach(module(ApplicationConfiguration.applicationModuleName));

    // The injector ignores leading and trailing underscores here (i.e. _$httpBackend_).
    // This allows us to inject a service but then attach it to a variable
    // with the same name as the service.
    beforeEach(inject(function ($rootScope, _TourtariffsService_) {
      // Set a new global scope
      $scope = $rootScope.$new();
      TourtariffsService = _TourtariffsService_;
    }));

    describe('Route Config', function () {
      describe('Main Route', function () {
        var mainstate;
        beforeEach(inject(function ($state) {
          mainstate = $state.get('tourtariffs');
        }));

        it('Should have the correct URL', function () {
          expect(mainstate.url).toEqual('/tourtariffs');
        });

        it('Should be abstract', function () {
          expect(mainstate.abstract).toBe(true);
        });

        it('Should have template', function () {
          expect(mainstate.template).toBe('<ui-view/>');
        });
      });

      describe('View Route', function () {
        var viewstate,
          TourtariffsController,
          mockTourtariff;

        beforeEach(inject(function ($controller, $state, $templateCache) {
          viewstate = $state.get('tourtariffs.view');
          $templateCache.put('modules/tourtariffs/client/views/view-tourtariff.client.view.html', '');

          // create mock Tourtariff
          mockTourtariff = new TourtariffsService({
            _id: '525a8422f6d0f87f0e407a33',
            name: 'Tourtariff Name'
          });

          // Initialize Controller
          TourtariffsController = $controller('TourtariffsController as vm', {
            $scope: $scope,
            tourtariffResolve: mockTourtariff
          });
        }));

        it('Should have the correct URL', function () {
          expect(viewstate.url).toEqual('/:tourtariffId');
        });

        it('Should have a resolve function', function () {
          expect(typeof viewstate.resolve).toEqual('object');
          expect(typeof viewstate.resolve.tourtariffResolve).toEqual('function');
        });

        it('should respond to URL', inject(function ($state) {
          expect($state.href(viewstate, {
            tourtariffId: 1
          })).toEqual('/tourtariffs/1');
        }));

        it('should attach an Tourtariff to the controller scope', function () {
          expect($scope.vm.tourtariff._id).toBe(mockTourtariff._id);
        });

        it('Should not be abstract', function () {
          expect(viewstate.abstract).toBe(undefined);
        });

        it('Should have templateUrl', function () {
          expect(viewstate.templateUrl).toBe('modules/tourtariffs/client/views/view-tourtariff.client.view.html');
        });
      });

      describe('Create Route', function () {
        var createstate,
          TourtariffsController,
          mockTourtariff;

        beforeEach(inject(function ($controller, $state, $templateCache) {
          createstate = $state.get('tourtariffs.create');
          $templateCache.put('modules/tourtariffs/client/views/form-tourtariff.client.view.html', '');

          // create mock Tourtariff
          mockTourtariff = new TourtariffsService();

          // Initialize Controller
          TourtariffsController = $controller('TourtariffsController as vm', {
            $scope: $scope,
            tourtariffResolve: mockTourtariff
          });
        }));

        it('Should have the correct URL', function () {
          expect(createstate.url).toEqual('/create');
        });

        it('Should have a resolve function', function () {
          expect(typeof createstate.resolve).toEqual('object');
          expect(typeof createstate.resolve.tourtariffResolve).toEqual('function');
        });

        it('should respond to URL', inject(function ($state) {
          expect($state.href(createstate)).toEqual('/tourtariffs/create');
        }));

        it('should attach an Tourtariff to the controller scope', function () {
          expect($scope.vm.tourtariff._id).toBe(mockTourtariff._id);
          expect($scope.vm.tourtariff._id).toBe(undefined);
        });

        it('Should not be abstract', function () {
          expect(createstate.abstract).toBe(undefined);
        });

        it('Should have templateUrl', function () {
          expect(createstate.templateUrl).toBe('modules/tourtariffs/client/views/form-tourtariff.client.view.html');
        });
      });

      describe('Edit Route', function () {
        var editstate,
          TourtariffsController,
          mockTourtariff;

        beforeEach(inject(function ($controller, $state, $templateCache) {
          editstate = $state.get('tourtariffs.edit');
          $templateCache.put('modules/tourtariffs/client/views/form-tourtariff.client.view.html', '');

          // create mock Tourtariff
          mockTourtariff = new TourtariffsService({
            _id: '525a8422f6d0f87f0e407a33',
            name: 'Tourtariff Name'
          });

          // Initialize Controller
          TourtariffsController = $controller('TourtariffsController as vm', {
            $scope: $scope,
            tourtariffResolve: mockTourtariff
          });
        }));

        it('Should have the correct URL', function () {
          expect(editstate.url).toEqual('/:tourtariffId/edit');
        });

        it('Should have a resolve function', function () {
          expect(typeof editstate.resolve).toEqual('object');
          expect(typeof editstate.resolve.tourtariffResolve).toEqual('function');
        });

        it('should respond to URL', inject(function ($state) {
          expect($state.href(editstate, {
            tourtariffId: 1
          })).toEqual('/tourtariffs/1/edit');
        }));

        it('should attach an Tourtariff to the controller scope', function () {
          expect($scope.vm.tourtariff._id).toBe(mockTourtariff._id);
        });

        it('Should not be abstract', function () {
          expect(editstate.abstract).toBe(undefined);
        });

        it('Should have templateUrl', function () {
          expect(editstate.templateUrl).toBe('modules/tourtariffs/client/views/form-tourtariff.client.view.html');
        });

        xit('Should go to unauthorized route', function () {

        });
      });

    });
  });
}());

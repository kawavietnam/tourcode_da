// Tourtariffs service used to communicate Tourtariffs REST endpoints
(function () {
  'use strict';

  angular
    .module('tourtariffs')
    .factory('TourtariffsService', TourtariffsService);

  TourtariffsService.$inject = ['$resource'];

  function TourtariffsService($resource) {
    return $resource('api/tourtariffs/:tourtariffId', {
      tourtariffId: '@_id'
    }, {
      update: {
        method: 'PUT'
      }
    });
  }
}());

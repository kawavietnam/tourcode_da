(function() {
    'use strict';

    // UserController
    angular
        .module('tourtariffs')
        .directive('loginSignup', function() {
            return {
                restrict: 'E',
                templateUrl: '/modules/tourtariffs/client/views/templareUserInfo.view.html',
                controller: function($scope, store, $location, $http) {
                   
                    
                    $scope.IsUser = false;
                    $scope.IsView = false;
                    $scope.IsAdmin = false;
                    $scope.IsEdit = false;
                    $scope.IsProduct = false;
                    $scope.IsReservation = false;

                    $scope.init = function() {
                        var pgurl = window.location.href;
                        var remove_host = window.location.host;
                        var link_li = pgurl.replace('http://' + remove_host, '');

                        $scope.User = store.get("User");
                        if ($scope.User == null || $scope.User == "") {
                            $location.path('/login');
                        } else {

                            if(link_li === '/tourview-tl' && store.get("User").country !== 'thailand')
                            {
                                $location.path('/login');
                            }
                            else if(link_li === '/tourview' && store.get("User").country !== 'vietnam')
                            {
                                $location.path('/login');
                            }
                            else{
                                for (var i = 0; i < $scope.User.role.length; i++) {
                                    $scope["Is" + $scope.User.role[i].code] = true;
                                }
                                
                                if ($scope.IsAdmin == false && $scope.IsEdit == false && $scope.IsUser == true) {
                                    $location.path('/login');
                                } 
                                else if ($scope.IsAdmin == true) {
                                    $scope.IsProduct = true;
                                    
                                }
                                $scope.IsEdit = $scope.IsProduct; 
                                $scope.checkDirectModel = $scope.IsReservation ? true : false;
                                $scope.ValueEditModel = $scope.IsProduct ? 'excursions' : "proposal";
                                $scope.isProposal = (!$scope.IsProduct && $scope.IsAdmin) || $scope.IsReservation ? true : false; 
                            }
                            
                        }
                    }
                    $scope.init();

                    $scope.Logout = function() {
                        store.set("User", null);
                        // $scope.User = {};
                        location.href = '/login';
                    }
                    $scope.ShowProfile = function() {
                        $("#popupProfile").modal('show');
                    }
                    $scope.ShowChangePass = false;
                    $scope.ClickShowChangePass = function() {
                        if ($scope.ShowChangePass)
                            $scope.ShowChangePass = false;
                        else $scope.ShowChangePass = true;
                    }
                    $scope.UpdateProfile = function() {
                        if(store.get("User").country ==='vietnam'){
                            $http.put('/api/tourtariffs/updateprofileusers', $scope.User)
                            .success(function(rs) {
                                if (rs == "Update success!") {
                                    store.set("User", $scope.User);
                                }
                                alert(rs);
                            });
                        }
                        else{
                            $http.put('/api/tourviewTls/updateprofileusers', $scope.User)
                            .success(function(rs) {
                                if (rs == "Update success!") {
                                    store.set("User", $scope.User);
                                }
                                alert(rs);
                            });
                        }
                    }
                    $scope.UserPass = { _id: "", oldpass: "", newpass: "", cfmpass: "" };
                    $scope.ChangePass = function() {
                        if ($scope.UserPass.oldpass != $scope.User.pass) {
                            alert("Old password is not true");
                            return;
                        }
                        if ($scope.UserPass.newpass.length < 5) {
                            alert("New password least 5 characters");
                            return;
                        }
                        if ($scope.UserPass.newpass != $scope.UserPass.cfmpass) {
                            alert("Confirm password is false!");
                            return;
                        }
                        $scope.UserPass._id = $scope.User._id;
                         if(store.get("User").country ==='vietnam'){
                            $http.put('/api/tourtariffs/updatepassusers', $scope.UserPass)
                            .success(function(rs) {
                                alert("Success!");
                                var DataSend = rs;
                                DataSend.country = 'vietnam';
                                store.set("User", DataSend);
                            });
                         }
                         else{
                            $http.put('/api/tourviewTls/updatepassusers', $scope.UserPass)
                            .success(function(rs) {
                                alert("Success!");
                                var DataSend = rs;
                                DataSend.country = 'thailand';
                                store.set("User", DataSend);
                            });
                         }
                        
                    }
                    $scope.ImgProfile = null;
                    $scope.UpdateImgProfile = function() {
                        if ($scope.ImgProfile !== null) {
                            var nameSplit = $scope.ImgProfile.filename.split(".");
                            var dataimage = {
                                _id: $scope.User._id,
                                Image: $scope.ImgProfile.base64,
                                NameImage: $scope.User.username + "." + nameSplit[nameSplit.length - 1]
                            }

                             if(store.get("User").country ==='vietnam')
                             {
                                $http.post('/api/tourtariffs/updateimgprofile', dataimage)
                                    .success(function(rs) {
                                        $scope.User.nameImg = dataimage.NameImage;
                                        alert(rs);

                                    })
                                    .error(function(data) {
                                        alert("Error!")
                                    });
                              }
                             else{
                                $http.post('/api/tourviewTls/updateimgprofile', dataimage)
                                    .success(function(rs) {
                                        $scope.User.nameImg = dataimage.NameImage;
                                        alert(rs);

                                    })
                                    .error(function(data) {
                                        alert("Error!")
                                    });
                            }
                        } else alert("Please select image!");
                    }
                },
                link: function(scope) {
                    // scope.init();

                }
            };
        })
        .controller('UsersController', UsersController);

    UsersController.$inject = ['$scope', '$http', 'store', '$location'];

    function UsersController($scope, $http, store, $location) {
        var vm = this;

        $scope.WebInfo = {};

        $scope.ImgWebBanner = null;
        $scope.UpdateImgWebBanner = function() {
            if ($scope.ImgWebBanner !== null) {
                var nameSplit = $scope.ImgWebBanner.filename.split(".");
                if (nameSplit[nameSplit.length - 1].toLowerCase() == "png") {

                    var dataimage = {
                        _id: $scope.WebInfo._id,
                        Image: $scope.ImgWebBanner.base64,
                        NameImage: "baner." + nameSplit[nameSplit.length - 1]
                    }
                    if(store.get("User").country ==='vietnam')
                    {
                         $http.post('/api/tourtariffs/updateimgwebbanner', dataimage)
                            .success(function(rs) {
                                alert(rs);
                            })
                            .error(function(data) {
                                alert("Error!")
                            });
                    }
                    else{
                         $http.post('/api/tourviewTls/updateimgwebbanner', dataimage)
                            .success(function(rs) {
                                alert(rs);
                            })
                            .error(function(data) {
                                alert("Error!")
                            });
                    }
                   
                } else alert("Fail! The file has to be '.png' extend");

            } else alert("Please select image!");
        }

        $scope.ImgExpBanner = null;
        $scope.UpdateImgExpBanner = function() {
            if ($scope.ImgExpBanner !== null) {
                $scope.WebInfo.expBanner = $scope.ImgExpBanner.base64;
                $scope.UpdateWebInfo();
            } else {
                alert("Please select image!");
            }
        }
        $scope.ImgSlide = null;
        $scope.AddImgSlide = function() {
            if ($scope.ImgSlide !== null) {
                var dataimage = {
                    _id: $scope.WebInfo._id,
                    Image: $scope.ImgSlide.base64,
                    NameImage: $scope.ImgSlide.filename
                }
                if(store.get("User").country ==='vietnam')
                {
                    $http.post('/api/tourtariffs/addimgslide', dataimage)
                    .success(function(rs) {
                        if (rs == "Success!") {
                            $scope.WebInfo.listSlide.push({ name: $scope.ImgSlide.filename });
                            $scope.ImgSlide = null;
                        }

                    })
                    .error(function(data) {
                        alert("Error!")
                    });
                }else{
                    $http.post('/api/tourviewTls/addimgslide', dataimage)
                    .success(function(rs) {
                        if (rs == "Success!") {
                            $scope.WebInfo.listSlide.push({ name: $scope.ImgSlide.filename });
                            $scope.ImgSlide = null;
                        }

                    })
                    .error(function(data) {
                        alert("Error!")
                    });
                }
            } else alert("Please select image!");
        }
        $scope.DeleteImgSlide = function($index) {
            var data = [];
            for (var i = 0; i < $scope.WebInfo.listSlide.length; i++) {
                if (i != $index)
                    data.push($scope.WebInfo.listSlide[i]);
            }
            if(store.get("User").country ==='vietnam')
            {
                $http.post('/api/tourtariffs/deleteimgslide', { _id: $scope.WebInfo._id, listSlide: data })
                .success(function(rs) {
                    if (rs) {
                        $scope.WebInfo.listSlide.splice($index, 1);
                    }
                    console.log($scope.WebInfo);
                })
                .error(function(rs) {

                });
            }else{
                $http.post('/api/tourviewTls/deleteimgslide', { _id: $scope.WebInfo._id, listSlide: data })
                .success(function(rs) {
                    if (rs) {
                        $scope.WebInfo.listSlide.splice($index, 1);
                    }
                    console.log($scope.WebInfo);
                })
                .error(function(rs) {

                });
            }
        }

        $scope.InitWebInfo = function() {
            if(store.get("User").country ==='vietnam')
            {
                $http.get('/api/tourtariffs/webinfo')
                    .success(function(rs) {
                    if (rs) {
                        $scope.WebInfo = rs;
                        $scope.WebInfo.webBanner += '?' + new Date().getTime();
                        if ($scope.WebInfo.expBanner != "") {
                            $scope.ImgExpBanner = {};
                            $scope.ImgExpBanner.base64 = $scope.WebInfo.expBanner;
                        }
                        CKEDITOR.instances.editor.setData($scope.WebInfo.term);
                        CKEDITOR.instances.editorCoverPage.setData($scope.WebInfo.coverpage);
                    }
                    console.log($scope.WebInfo);
                    })
                    .error(function(rs) {

                    });
                }else{
                $http.get('/api/tourviewTls/webinfo')
                    .success(function(rs) {
                        if (rs) {
                            $scope.WebInfo = rs;
                            if($scope.WebInfo !=="null"){
                                $scope.WebInfo.webBanner += '?' + new Date().getTime();
                                if ($scope.WebInfo.expBanner != "") {
                                    $scope.ImgExpBanner = {};
                                    $scope.ImgExpBanner.base64 = $scope.WebInfo.expBanner;
                                }
                            }
                            CKEDITOR.instances.editor.setData($scope.WebInfo.term);
                            CKEDITOR.instances.editorCoverPage.setData($scope.WebInfo.coverpage);
                        }
                        console.log($scope.WebInfo);
                    })
                    .error(function(rs) {

                    });
            }

            CKEDITOR.replace('editor', {
                toolbar: [
                    { name: 'document', items: ['Source', '-', 'NewPage', 'Preview', '-', 'Templates'] }, // Defines toolbar group with name (used to create voice label) and items in 3 subgroups.
                    ['Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo'], // Defines toolbar group without name.
                    '/', // Line break - next group will be placed in new line.
                    { name: 'basicstyles', items: ['Bold', 'Italic'] },
                    { name: 'basicstyles', groups: ['basicstyles', 'cleanup'], items: ['Bold', 'Italic', 'Strike', '-', 'RemoveFormat'] },
                    { name: 'paragraph', groups: ['list', 'indent', 'blocks', 'align', 'bidi'], items: ['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote'] },
                    { name: 'styles', items: ['Styles', 'Format'] }
                ],
                height: '100px',
            });
            var apiImage = store.get("User").country ==='vietnam' ? '/api/tourtariffs/LoadImageToContent': '/api/tourviewTls/LoadImageToContent';
            CKEDITOR.replace('editorCoverPage', {               
                "extraPlugins": 'imagebrowser',
                "imageBrowser_listUrl": apiImage,
                height: '297mm',
            });
        }

        $scope.SaveTerm = function() {
             if(store.get("User").country ==='vietnam')
            {
                $http.post('/api/tourtariffs/updateterm', { _id: $scope.WebInfo._id, term: CKEDITOR.instances.editor.getData() })
                .success(function(rs) {
                    alert("Success!");
                })
                .error(function(rs) {

                });
            }else{
                $http.post('/api/tourviewTls/updateterm', { _id: $scope.WebInfo._id, term: CKEDITOR.instances.editor.getData() })
                .success(function(rs) {
                    alert("Success!");
                })
                .error(function(rs) {

                });
            }
        }

        $scope.SaveCoverPage = function() {
             if(store.get("User").country ==='vietnam')
            {
                $http.post('/api/tourtariffs/updatecoverpage', { _id: $scope.WebInfo._id, coverpage: CKEDITOR.instances.editorCoverPage.getData() })
                .success(function(rs) {
                    alert("Success!");
                })
                .error(function(rs) {

                });
            }else{
                $http.post('/api/tourviewTls/updatecoverpage', { _id: $scope.WebInfo._id, coverpage: CKEDITOR.instances.editorCoverPage.getData() })
                .success(function(rs) {
                    alert("Success!");
                })
                .error(function(rs) {

                });
            }
        }

        $scope.UpdateWebInfo = function() {
             if(store.get("User").country ==='vietnam')
            {
                $http.put('/api/tourtariffs/webinfo', $scope.WebInfo)
                .success(function(rs) {
                    alert("Success!");
                })
                .error(function(rs) {

                });
            }else{
                    $http.put('/api/tourviewTls/webinfo', $scope.WebInfo)
                .success(function(rs) {
                    alert("Success!");
                })
                .error(function(rs) {

                });
                }
        }
        $scope.LoginData = { username: "", pass: "", email: "",country: "" }
        $scope.Login = function() {
            var data = { username: angular.lowercase($scope.LoginData.username), pass: $scope.LoginData.pass, active: true };
            console.log($scope.LoginData);
            if($scope.LoginData.country === 'vietnam')
            {

                $http.post('/api/tourtariffs/login', data)
                .success(function(rs) {
                    if (rs.length > 0) {
                        var DataSend = rs[0];
                        DataSend.country = 'vietnam';
                        store.set("User", DataSend);
                        location.href = '/tourview';
                    } else {
                        alert("Login fail! Check Username or Active!");
                    }
                });
            }
            else if($scope.LoginData.country === 'thailand'){
                $http.post('/api/tourviewTls/login', data)
                .success(function(rs) {
                    if (rs.length > 0) {
                        var DataSend = rs[0];
                        DataSend.country = 'thailand';
                        store.set("User", DataSend);
                        location.href = '/tourview-tl';
                    } else {
                        alert("Login fail! Check Username or Active!");
                    }
                });
            }
            else{
                alert("select Country, please!");
            }
        }

        // Login.init();
        // App.init();
        // $scope.boolLogin = true;
        // $scope.Users = { fullname: "", username: "", email: "", company: "", pass: "", passCfn: "", role: [{ name: "User", code: "User" }] };
        // $scope.ClickTab = function(event, bool) {
        //     if ($(event.currentTarget).has("layer-2-2")) {
        //         $(".layer-2").addClass("layer-2-2").removeClass("layer-2");
        //         $(".layer-1").addClass("layer-1-1").removeClass("layer-1");
        //         $(event.currentTarget).addClass("layer-2").removeClass("layer-2-2");
        //         $(event.currentTarget).parent().addClass("layer-1").removeClass("layer-1-1");
        //         $scope.boolLogin = bool;
        //     }

        // }
        // $scope.SignUp = function() {
        //     if ($scope.Users.fullname.length < 10) {
        //         alert("Full name least 10 characters");
        //         return;
        //     }
        //     if ($scope.Users.username.length < 5) {
        //         alert("Login name least 5 characters");
        //         return;
        //     }
        //     if (!validateEmail($scope.Users.email)) {
        //         alert("Email isn't true fomat");
        //         return;
        //     }
        //     if ($scope.Users.pass.length < 5) {
        //         alert("Password least 5 characters");
        //         return;
        //     }
        //     if ($scope.Users.pass != $scope.Users.passCfn) {
        //         alert("Confirm password is false!");
        //         return;
        //     }
        //     $scope.Users.pass = md5($scope.Users.pass);
        //     $http.post('/api/tourtariffs/users', $scope.Users)
        //         .success(function(rs) {
        //             $scope.Users = { fullname: "", username: "", email: "", company: "", pass: "", passCfn: "", role: [{ name: "User", code: "User" }] };
        //             alert("Sign up success! Please login to enter system.");
        //             $(".layer-2-2").click();
        //         });

        //     function validateEmail(email) {
        //         var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        //         return re.test(email);
        //     }
        // }



    }
}());
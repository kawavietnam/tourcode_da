(function() {
    'use strict';

    angular
        .module('tourtariffs')
        .config(routeConfig);

    routeConfig.$inject = ['$stateProvider'];

    function routeConfig($stateProvider) {
        $stateProvider
            .state('tourtariffs', {
                abstract: true,
                url: '/tourview',
                template: '<ui-view/>'
            })

        .state('login', {
            url: '/login',
            templateUrl: '/modules/tourtariffs/client/views/login-tourtariff.client.view.html',
            controller: 'UsersController',
            controllerAs: 'vm',
            data: {
                pageTitle: 'Tourtariffs login'
            }
        })

        .state('tourtariffs.list', {
            url: '',
            templateUrl: 'modules/tourtariffs/client/views/list-tourtariffs.client.view.html',
            controller: 'TourtariffsListController',
            controllerAs: 'vm',
            data: {
                pageTitle: 'Tourtariffs List'
            }
        })

        .state('tourtariffs.sevgroup', {
                url: '/servicegroup',
                templateUrl: '/modules/tourtariffs/client/views/sevgroup-tourtariff.client.view.html',
                controller: 'TourtariffsController',
                controllerAs: 'vm',
                data: {
                    pageTitle: 'Tourtariffs service'
                }
            })
            .state('tourtariffs.sevcategory', {
                url: '/servicecategory',
                templateUrl: '/modules/tourtariffs/client/views/sevcategory-tourtariff.client.view.html',
                controller: 'TourtariffsController',
                controllerAs: 'vm',
                data: {
                    pageTitle: 'Tourtariffs service'
                }
            })
            .state('tourtariffs.servicetype', {
                url: '/servicetype',
                templateUrl: '/modules/tourtariffs/client/views/servicetype-tourtariff.client.view.html',
                controller: 'TourtariffsController',
                controllerAs: 'vm',
                data: {
                    pageTitle: 'Tourtariffs service'
                }
            })
            .state('tourtariffs.geotree', {
                url: '/geotree',
                templateUrl: '/modules/tourtariffs/client/views/geotree-tourtariff.client.view.html',
                controller: 'TourtariffsController',
                controllerAs: 'vm',
                data: {
                    pageTitle: 'Tourtariffs service'
                }
            })

        .state('tourtariffs.exchangerate', {
                url: '/exchangerate',
                templateUrl: '/modules/tourtariffs/client/views/exchangerate-tourtariff.client.view.html',
                controller: 'TourtariffsController',
                controllerAs: 'vm',
                data: {
                    pageTitle: 'Tourtariffs service'
                }
            })
            .state('tourtariffs.tariffperiod', {
                url: '/tariffperiod',
                templateUrl: '/modules/tourtariffs/client/views/tariffperiod-tourtariff.client.view.html',
                controller: 'TourtariffsController',
                controllerAs: 'vm',
                data: {
                    pageTitle: 'Tourtariffs service'
                }
            })
            .state('tourtariffs.sorry', {
                url: '/sorry',
                templateUrl: '/modules/tourtariffs/client/views/sorrypage-tourtariff.client.view.html',
                controller: 'TourtariffsController',
                controllerAs: 'vm',
                data: {
                    pageTitle: 'Tourtariffs service'
                }
            })
            .state('tourtariffs.supplier', {
                url: '/supplier',
                templateUrl: '/modules/tourtariffs/client/views/supplier-tourtariff.client.view.html',
                controller: 'TourtariffsController',
                controllerAs: 'vm',
                data: {
                    pageTitle: 'Tourtariffs service'
                }
            })
            .state('tourtariffs.priority', {
                url: '/priority',
                templateUrl: '/modules/tourtariffs/client/views/priority-tourtariff.client.view.html',
                controller: 'TourtariffsController',
                controllerAs: 'vm',
                data: {
                    pageTitle: 'Tourtariffs service'
                }
            })
            .state('tourtariffs.notify', {
                url: '/notify',
                templateUrl: '/modules/tourtariffs/client/views/notify-tourtariff.client.view.html',
                controller: 'TourtariffsController',
                controllerAs: 'vm',
                data: {
                    pageTitle: 'Tourtariffs service'
                }
            })
            .state('tourtariffs.users', {
                url: '/users',
                templateUrl: '/modules/tourtariffs/client/views/users-tourtariff.client.view.html',
                controller: 'TourtariffsController',
                controllerAs: 'vm',
                data: {
                    pageTitle: 'Tourtariffs service'
                }
            })
            .state('tourtariffs.webinfo', {
                url: '/webinfo',
                templateUrl: '/modules/tourtariffs/client/views/webinfo-tourtariff.client.view.html',
                controller: 'UsersController',
                controllerAs: 'vm',
                data: {
                    pageTitle: 'Tourtariffs service'
                }
            })
            .state('tourtariffs.client', {
                url: '/client/:idCode',
                templateUrl: '/modules/tourtariffs/client/views/client-tourtariff.client.view.html',
                controller: 'TourtariffsController',
                controllerAs: 'vm',
                data: {
                    pageTitle: 'VN Tour Tariff'
                }
            })
            .state('tourtariffs.detail', {
                url: '/detail/:tourtariffId/:codeID',
                templateUrl: '/modules/tourtariffs/client/views/detailclient-tourtariff.client.view.html',
                controller: 'TourtariffsController',
                controllerAs: 'vm',
                data: {
                    pageTitle: 'Tourtariffs service'
                }
            })
            .state('tourtariffs.edit', {
                url: '/:tourtariffId/edit',
                templateUrl: 'modules/tourtariffs/client/views/form-tourtariff.client.view.html',
                controller: 'TourtariffsController',
                controllerAs: 'vm',
                resolve: {
                    tourtariffResolve: getTourtariff
                },
                data: {
                    roles: ['user', 'admin'],
                    pageTitle: 'Edit Tourtariff {{ tourtariffResolve.name }}'
                }
            })
            .state('tourtariffs.view', {
                url: '/:tourtariffId',
                templateUrl: 'modules/tourtariffs/client/views/view-tourtariff.client.view.html',
                controller: 'TourtariffsController',
                controllerAs: 'vm',
                resolve: {
                    tourtariffResolve: getTourtariff
                },
                data: {
                    pageTitle: 'Tourtariff {{ tourtariffResolve.name }}'
                }
            });
    }

    getTourtariff.$inject = ['$stateParams', 'TourtariffsService'];

    function getTourtariff($stateParams, TourtariffsService) {
        return TourtariffsService.get({
            tourtariffId: $stateParams.tourtariffId
        }).$promise;
    }

    newTourtariff.$inject = ['TourtariffsService'];

    function newTourtariff(TourtariffsService) {
        return new TourtariffsService();
    }
}());
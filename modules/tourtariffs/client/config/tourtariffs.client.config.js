(function () {
  'use strict';

  angular
    .module('tourtariffs')
    .run(menuConfig);

  menuConfig.$inject = ['menuService'];

  function menuConfig(menuService) {
    // Set top bar menu items
    menuService.addMenuItem('topbar', {
      title: 'Tourtariffs',
      state: 'tourtariffs',
      type: 'dropdown',
      roles: ['*']
    });

    // Add the dropdown list item
    menuService.addSubMenuItem('topbar', 'tourtariffs', {
      title: 'List Tourtariffs',
      state: 'tourtariffs.list'
    });

    // Add the dropdown create item
    menuService.addSubMenuItem('topbar', 'tourtariffs', {
      title: 'Create Tourtariff',
      state: 'tourtariffs.create',
      roles: ['user']
    });
  }
}());

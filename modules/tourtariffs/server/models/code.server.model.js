'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
  Schema = mongoose.Schema;

/**
 * Tourtariff Schema
 */
var CodeSchema = new Schema({
  code: String,
  excursionName: String,
  tourCategory: String,
  location: String,
  fromDate : Date,
  toDate: Date,
  duration: Number,
  isActive: Boolean
});

mongoose.model('Code', CodeSchema);

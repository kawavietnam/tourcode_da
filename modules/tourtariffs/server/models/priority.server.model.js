'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

/**
 * Tourtariff Schema
 */
var PrioritySchema = new Schema({
    name: {
        type: String,
        default: '',
        required: 'Please fill GeoTree name',
        trim: true
    },
    code: String
});

mongoose.model('Priority', PrioritySchema);
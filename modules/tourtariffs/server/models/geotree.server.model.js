'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
  Schema = mongoose.Schema;

/**
 * Tourtariff Schema
 */
var GeoTreeSchema = new Schema({
  name: {
    type: String,
    default: '',
    required: 'Please fill GeoTree name',
    trim: true
  },
  zone: String
});

mongoose.model('GeoTree', GeoTreeSchema);
  
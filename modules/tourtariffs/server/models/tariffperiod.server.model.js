'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

/**
 * Tourtariff Schema
 */
var TariffPeriodSchema = new Schema({
    date: Date,
    name: String,
    no: Number
});

mongoose.model('TariffPeriod', TariffPeriodSchema);
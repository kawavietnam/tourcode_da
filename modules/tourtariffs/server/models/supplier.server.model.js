'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

/**
 * Tourtariff Schema
 */
var SupplierSchema = new Schema({
    name: {
        type: String,
        default: '',
        required: 'Please fill Supplier name',
        trim: true
    },
    address: String,
    phone: String,
    email: String
});

mongoose.model('Supplier', SupplierSchema);
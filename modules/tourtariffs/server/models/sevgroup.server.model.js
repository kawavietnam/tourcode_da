'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

/**
 * Tourtariff Schema
 */
var SevGroupSchema = new Schema({
    name: {
        type: String,
        default: '',
        required: 'Please fill name',
        trim: true
    },
    beginDate: Date,
    endDate: Date,
    location: String,
    supplier: String,
    supplierTourCode: String,
    types: String,
    address: String,
    tel: String,
    email: String,
    url: String,
    group: {
        price: Number,
        currency: String
    },
    priceband: {
        currency: String,
        list: [{
            name: String,
            min: Number,
            max: Number,
            price: Number
        }]
    },
    accumulated: {
        name: String,
        min: Number,
        max: Number,
        price: Number,
        currency: String
    }

});

mongoose.model('SevGroup', SevGroupSchema);
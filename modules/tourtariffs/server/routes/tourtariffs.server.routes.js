'use strict';

/**
 * Module dependencies
 */
var tourtariffsPolicy = require('../policies/tourtariffs.server.policy'),
    tourtariffs = require('../controllers/tourtariffs.server.controller');

module.exports = function(app) {
    // Tourtariffs Routes


    app.route('/api/tourtariffs/code')
        .post(tourtariffs.SaveCode)

    app.route('/api/tourtariffs/updateImage')
        .post(tourtariffs.updateImage)
    app.route('/api/tourtariffs/updateimagegroup')
        .put(tourtariffs.updateImageGroup)
    app.route('/api/tourtariffs/deleteimage')
        .post(tourtariffs.DeleteImage)
    app.route('/api/tourtariffs/GeTListBygroup')
        .post(tourtariffs.GeTListBygroup)

    app.route('/api/tourtariffs/loadimage')
        .get(tourtariffs.LoadListImage)

    app.route('/api/tourtariffs/LoadImageToContent')
        .get(tourtariffs.LoadImageToContent)

    // service
    app.route('/api/tourtariffs/GettiersByGroupCode')
        .post(tourtariffs.GettiersByGroupCode)

    //SevGroup
    app.route('/api/tourtariffs/LoadSevGroup').get(tourtariffs.LoadSevGroup)
    app.route('/api/tourtariffs/createSevGroup').post(tourtariffs.createSevGroup)
    app.route('/api/tourtariffs/updateSevGroup').put(tourtariffs.updateSevGroup)
    app.route('/api/tourtariffs/deleteSevGroup').post(tourtariffs.deleteSevGroup)

    app.route('/api/tourtariffs/LoadService').post(tourtariffs.LoadService)


    //Category
    app.route('/api/tourtariffs/Category')
        .post(tourtariffs.createCategory)
        .get(tourtariffs.loadCategory)
        .put(tourtariffs.updateCategory)


    app.route('/api/tourtariffs/updateDataSaveChangeDate')
        .get(tourtariffs.updateDataSaveChangeDate)

    app.route('/api/tourtariffs/deleteCategory')
        .post(tourtariffs.deleteCategory)

    //servicetype
    app.route('/api/tourtariffs/servicetype')
        .post(tourtariffs.createServiceType)
        .get(tourtariffs.loadServiceType)
        .put(tourtariffs.updateServiceType)

    app.route('/api/tourtariffs/deleteservicetype')
        .post(tourtariffs.deleteServiceType)

    //geotree
    app.route('/api/tourtariffs/geotree')
        .post(tourtariffs.createGeoTree)
        .get(tourtariffs.loadGeoTree)
        .put(tourtariffs.updateGeoTree)

    app.route('/api/tourtariffs/deletegeotree')
        .post(tourtariffs.deleteGeoTree)

    //exchangerate
    app.route('/api/tourtariffs/exchangerate')
        .post(tourtariffs.createExchangeRate)
        .get(tourtariffs.loadExchangeRate)
        .put(tourtariffs.updateExchangeRate)

    app.route('/api/tourtariffs/deleteexchangerate')
        .post(tourtariffs.deleteExchangeRate)

    //tariffperiod
    app.route('/api/tourtariffs/tariffperiod')
        .post(tourtariffs.createTariffPeriod)
        .get(tourtariffs.loadTariffPeriod)
        .put(tourtariffs.updateTariffPeriod)

    app.route('/api/tourtariffs/deletetariffperiod')
        .post(tourtariffs.deleteTariffPeriod)

    //Supplier
    app.route('/api/tourtariffs/supplier')
        .post(tourtariffs.createSupplier)
        .get(tourtariffs.loadSupplier)
        .put(tourtariffs.updateSupplier)

    app.route('/api/tourtariffs/deletesupplier')
        .post(tourtariffs.deleteSupplier)

    //Priority
    app.route('/api/tourtariffs/priority')
        .post(tourtariffs.createPriority)
        .get(tourtariffs.loadPriority)
        .put(tourtariffs.updatePriority)

    app.route('/api/tourtariffs/deletepriority')
        .post(tourtariffs.deletePriority)

    //notify
    app.route('/api/tourtariffs/notify')
        .post(tourtariffs.createNotify)
        .get(tourtariffs.loadNotify)
        .put(tourtariffs.updateNotify)

    app.route('/api/tourtariffs/loadnotifyclient')
        .get(tourtariffs.loadNotifyClient)

    app.route('/api/tourtariffs/deletenotify')
        .post(tourtariffs.deleteNotify)
        //User
    app.route('/api/tourtariffs/users')
        .post(tourtariffs.createUsers)
        .get(tourtariffs.loadUsers)
        .put(tourtariffs.updateUsers)

    app.route('/api/tourtariffs/updateprofileusers').put(tourtariffs.updateProfileUsers)
    app.route('/api/tourtariffs/updatepassusers').put(tourtariffs.updatePassUsers)
    app.route('/api/tourtariffs/updateimgprofile').post(tourtariffs.updateImgProfile)

    app.route('/api/tourtariffs/deleteusers')
        .post(tourtariffs.deleteUsers)
    app.route('/api/tourtariffs/login')
        .post(tourtariffs.loginUsers)

    //WebInfo
    app.route('/api/tourtariffs/webinfo')
        .post(tourtariffs.createWebInfo)
        .get(tourtariffs.loadWebInfo)
        .put(tourtariffs.updateWebInfo)

    app.route('/api/tourtariffs/updateimgwebbanner').post(tourtariffs.UpdateImgWebBanner)
    app.route('/api/tourtariffs/addimgslide').post(tourtariffs.AddImgSlide)
    app.route('/api/tourtariffs/deleteimgslide').post(tourtariffs.DeleteImgSlide)
    app.route('/api/tourtariffs/updateterm').post(tourtariffs.UpdateTern)
    app.route('/api/tourtariffs/updatecoverpage').post(tourtariffs.UpdateCoverPage)

    //big data
    app.route('/api/tourtariffs/datasave')
        .post(tourtariffs.createDataSave)
        .put(tourtariffs.updateDataSave)
    app.route('/api/tourtariffs/filerdatasave')
        .post(tourtariffs.FilterDataSave)

    app.route('/api/tourtariffs/loaddata')
        .get(tourtariffs.LoadDataSave)
        .post(tourtariffs.LoadDataSaveByMarkup)
    app.route('/api/tourtariffs/loaddatasaveexcel')
        .post(tourtariffs.LoadDataSaveExcel)
    app.route('/api/tourtariffs/loadalldata')
        .post(tourtariffs.LoadAllDataSave)
    app.route('/api/tourtariffs/checkexistcode')
        .post(tourtariffs.CheckExistCode)
    app.route('/api/tourtariffs/loadrevisedata')
        .post(tourtariffs.LoadReviseDataSave)

    app.route('/api/tourtariffs/deletetour')
        .post(tourtariffs.deleteTour)
    app.route('/api/tourtariffs/LoadDataSaveById')
        .post(tourtariffs.LoadDataSaveById)

    app.route('/api/tourtariffs/groupImage')
        .get(tourtariffs.LoadGroupImage)
        .post(tourtariffs.createGroupImage)
        .put(tourtariffs.updateGroupImage)
    app.route('/api/tourtariffs/deletegroupimage')
        .post(tourtariffs.deleteGroupImage)



    // Finish by binding the Tourtariff middleware
    app.param('tourtariffId', tourtariffs.tourtariffByID);
};